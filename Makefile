CXX		  := /opt/PHYTEC_BSPs/gcc-linaro-6.2.1-2016.11-x86_64_arm-linux-gnueabihf/bin/arm-linux-gnueabihf-g++
CXX_FLAGS := -Wall -Wextra -std=c++11 -ggdb -pthread -Wno-psabi -Wno-pragmas

BIN		:= bin
SRC		:= src
INCLUDE	:= include
LIB		:= lib

LIBRARIES	:=
EXECUTABLE	:= main


all: $(BIN)/$(EXECUTABLE)

run: clean all
	clear
	./$(BIN)/$(EXECUTABLE)

#$(BIN)/$(EXECUTABLE): $(SRC)/*.cpp $(SRC)/comm/*.cpp $(SRC)/controller/*.cpp $(SRC)/f4_module/*.cpp $(SRC)/motor_driver/*.cpp \
	$(SRC)/remote_control/*.cpp $(SRC)/sensor/*.cpp $(SRC)/tracking_module/*.cpp $(SRC)/ui_pc/*.cpp $(SRC)/json_lib/*.cpp $(SRC)/logger/*.cpp
$(BIN)/$(EXECUTABLE): $(SRC)/*.cpp $(SRC)/comm/*.cpp $(SRC)/sensor/*.cpp $(SRC)/logger/*.cpp $(SRC)/json_lib/*.cpp $(SRC)/motor_driver/*.cpp
	$(CXX) $(CXX_FLAGS) -I$(INCLUDE) -L$(LIB) $^ -o $@ $(LIBRARIES)

clean:
	-rm $(BIN)/*
