cmake_minimum_required(VERSION 2.8.12)

project (STC_1207_AM572x_CC)

include_directories(include)


file(GLOB SOURCES "src/*.cpp" "src/comm/*.cpp" "src/sensor/*.cpp" "src/logger/*.cpp" "src/json_lib/*.cpp" "src/motor_driver/*.cpp" "src/remote_control/*.cpp" "src/controller/*.cpp")

set (CMAKE_CXX_FLAGS "-Wall -Wextra -std=c++11 -ggdb -pthread -Wno-psabi -Wno-pragmas")

add_executable(STC_1207_AM572x_CC ${SOURCES})
