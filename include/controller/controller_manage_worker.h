/**
******************************************************************************
* @file           : controller_manage_worker.h
* @brief          : header file of Worker for Controller Management
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef CONTROLLER_MANAGE_WORKER_H
#define CONTROLLER_MANAGE_WORKER_H

/* Include ------------------------------------------------------------------*/
#include "controller/controller.h"
#include <stdint.h>


class ControllerManageWorker
{
private:
    
public:
    ControllerManageWorker();
    ~ControllerManageWorker();

    // Attributes
    Controller AZController;
    float torque_debug;

    uint32_t time_log;

    double a_get, b_get, c_get, d_get;
    double t_offset;
    double k = 0, T_1 = 0, L = 0;
    float PID_detect_I;
    float PID_detect_P;
    float PID_detect;

    double w_n = 0, w_n_1 = 0;

    float D_filter = 0;

    double z_position;
    
    double y_position;
    float gyro_angle;

    float T_D_EL;
    float t_offset_old;

    // test
    float delta_pre;

    // new
    Controller ELController;

    // old
	int16_t setTorqueEL;
	int16_t setTorqueAZ;    
    float set_point_EL, set_point_AZ;
    bool flag_open_log;
    // offset param
    float gyro_offset_param_01;
    float gyro_offset_param_02;

    float w_ZF;

    float torque_filter;


    // test offset2
    float angle_setpoint;
    float angle_r;
    float kp_angle_controller, ki_angle_controller;
    float a_angle_controller, k_angle_controller;
    float I_angle_controller;
    float T_angle_controller;
    float T_detect_offset_2;
    float T_velocity;


    // 
    float test_angle, test_rate, test_bias;
    float test_rate_angle;
    float gyro_vel_bias;
    bool stop_controller_flag;

    // Method
    void stopControllerAsSafety(void);
    // test auto detection
    void run(void);
    void logFile(void);
    void inputParam(void);

    // test controller
    float az_torque;
    void runOldController(void);
    void debugLogController(void);

    // log file
    void logFileController(void);
    void logFileController2(void);

    void inputParamController(void);

    // new
    void runNonlinearController(void);

    // old
    void runVelocityController(void);
	// Calculation
	void multiply3x3(float* a, float* b, float* c);
	void multiply3x3x3x1(float* a, float* b, float* c);
	void transpose3x3(float* a, float* b);
	void transpose3x3(float* a);
	void rotz(float* R, float val);
	void rotx(float* R, float val);
	void roty(float* R, float val);	    
};

extern ControllerManageWorker controllerManageWorker;

#endif
