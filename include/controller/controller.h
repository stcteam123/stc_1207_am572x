/**
******************************************************************************
* @file           : controller.h
* @brief          : header file of Controller
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef CONTROLLER_H
#define CONTROLLER_H

/* Include -----------------------------------------------------------------*/

#include <math.h>
#include <stdint.h>

class Controller01{
private:

public:
    Controller01(){
        this->c1 = 1;
        this->c2 = 1;
        this->eta = 0.5;
        this->x1 = 0;
        this->x2 = 0;
        this->I = 0;
        this->J = 0.05;
        this->dT = 0.001; //ms
        this->torque = 0;
        this->torque_unlimit = 0;
        this->torque_min = -500;
        this->torque_max = 500;
    }
    ~Controller01(){

    }

    // attribute
    float c1, c2, eta;
    float x1; // roll, pith, heading
    float x2; // angle velocity
    float I;
    float J; // momen quan tinh truc quay
    float dT; // chu ki dieu kien ms
    float torque;
    float torque_log;
    float t_off;
    float torque_unlimit;
    float torque_max;
    float torque_min;

    // method
    void caculController(void);

};


class Controller02{
private:

public:
    Controller02(){
        this->k1 = 0;
        this->k2 = 0;
        this->lamda = 0;
        this->x1 = 0;
        this->x1_d = 0;
        this->x2 = 0;
        this->J = 0.05;
        this->s = 0;
        this->sign_s = 0;
        this->tanh_s = 0;
        this->torque = 0;
        this->torque_min = -500;
        this->torque_max = 500;
        this->x1_pre = 0;
    }
    ~Controller02(){

    }

    float k1, k2, lamda;
    float x1_d;
    float x1; // roll, pith, heading
    float x1_pre;
    float x2; // angle velocity

    float s;
    int sign_s;
    double tanh_s;
    float J; // momen quan tinh truc quay
    float dT;
    float eta;

    float t_offset;

    float torque;
    float torque_max;
    float torque_min;

    // method
    void caculController(void);  
};


class Controller03{
private:

public:
    Controller03(){
        this->set_point = 0;
        this->x1_filter = 0;
        this->x1_1 = 0;
        this->e = 0;
        this->e1 = 0;
        this->e2 = 0;

        this->z_pos = 0;

        this->dt = 0.001;

        this->k = 2;
        this->ki = 0;
        this->kp = 0;
        this->y = 0;
        this->y1 = 0;
        this->a = 2;
        this->x1 = 0;
        this->x1_d = 0;
        this->J = 0.05;

        this->output = 0;

        this->torque = 0;
        this->torque_min = -800;
        this->torque_max = 800;
    }
    ~Controller03(){

    }

    float set_point;
    float x1_filter, x1_1;
    float e, e1, e2;
    double dt;

    float z_pos;

    float k, a, kp, ki;
    float J; // momen quan tinh truc quay

    float y, y1;

    float x1_d;
    float x1; // roll, pitch, heading

    float output;

    float torque;
    float torque_unlimit;
    float torque_max;
    float torque_min;

    // method
    void caculController(void);    
};


class Controller04{
private:

public:
    Controller04(){

    }
    ~Controller04(){

    }

    float k1, k2;
    float x1; // roll, pith, heading
    float x2; // angle velocity

    float c1;

    float s;
    double tanh_s;
    float J; // momen quan tinh truc quay
    float dT;

    float torque;
    float torque_max;
    float torque_min;

    // method
    void caculController(void);    
};


class OriginPIDController{
private:
    
public:
    OriginPIDController(){
        this->I = 0;
        this->D = 0;
        this->I_D = 0;
        this->y_limit_max = 1000;
        this->y_limit_min = -1000;

    }
    ~OriginPIDController(){

    }

    // attribute
    float set_point;
	float kp;
	float ki;
	float kd;
	float T;
	float e;
	float y_limit_max;
	float y_limit_min;
	float T_control;
    float N;
    float P;
    float I;
    float D;
    float I_D;

    // method
    void caculController(void);    
};


class ReCognitionController{
private:
    
public:
    ReCognitionController(){
        this->phi_1 = 0;
        this->phi_2 = 0;
        this->phi_p_phi = 0;
        this->w_k = 0;
        this->w_k_1 = 0;
        this->T_k = 0;
        this->w_gyro_k_1 = 0;
        this->w_gyro_k_2 = 0;
        this->T_k_1 = 0;
        this->w_imu_1 = 0;
        this->P_11 = 6.95 * pow(10, -6);
        this->P_12 = 1.08 * pow(10, -6);
        this->P_21 = 1.08 * pow(10, -6);
        this->P_22 = 2.18 * pow(10, -6);
        this->P = 0;
        this->lamda = 0.995;
        this->L_1 = 0;
        this->L_2 = 0;
        this->epsilon = 0;
        this->phi_1_y = 0;
        this->phi_2_y = 0;
        this->theta1_k = 0;
        this->theta1_k_1 = -0.99858;
        this->theta2_k = 0;
        this->theta2_k_1 = 0.00176;
        this->k = 0;
        this->T_1 = 0;
        this->L = 0;
    }
    ~ReCognitionController(){

    }
    // attribute
    double phi_1, phi_2, phi_p_phi;
    double w_k, w_k_1, T_k, w_gyro_k_1, w_gyro_k_2;
    int16_t T_k_1;
    double w_imu_1;
    double P_11;
    double P_12;
    double P_21;
    double P_22;
    double P, lamda;
    double L_1, L_2;
    double theta1_k, theta1_k_1, theta2_k, theta2_k_1;
    double epsilon;

    double phi_1_y, phi_2_y;

    double k, T_1, L;

    double T_detect_offset;

    // input
    float imu_angle_vel;
    float imu_angle_vel_pre;
    float gyro_angle_vel;
    float gyro_angle_vel_pre;
    float torque;

    // method
    void caculController(void);
};


class NumericalPIDController{
private:       
	float a0;
	float a1;
	float a2;
	float y;
	float y1;
	float x;
	float x1;
	float x2;    
	float y_limit_max;
	float y_limit_min;    
public:
    NumericalPIDController(){
        this->y_limit_max = 1000;
        this->y_limit_min = -1000;
        this->set_point = 0;
        this->kp = 0;
        this->ki = 0;
        this->kd = 0;
    }
    ~NumericalPIDController(){

    }
    // attribute
    float set_point;
	float kp;
	float ki;
	float kd;
	float T_control;

    // input
    float imu_angle_vel;
    float gyro_angle_vel;

    // method
    void caculController(void);
};

class CompenstateFriction{
private:  
    float T_f_limit_max;
public:
    CompenstateFriction(){
        this->T_f_limit_max = 50;
        this->K_f = 0;     
    }
    ~CompenstateFriction(){

    }
    // attribute
    float K_f;
    float T_compenstateFriction;
    // input
    float imu_angle_vel;
    float gyro_angle_vel;
    float delta_omega;
    float resolver_vel;
    float resolver_vel_pre;
    float resolver_pos;
    // method
    void caculController(void);

};

class IdFriction{
    private: 
    int T_f_max;
    float lamda;
    public:
    IdFriction(){
       this->T_f_max = 50;
       this->lamda = 0.995;
       this->y = 0;
       this->theta1_pre = 0; this->theta2_pre = 0; this->theta3_pre = 0;
       this->p11_pre = 1; this->p12_pre = 0; this->p13_pre = 0;
       this->p21_pre = 0; this->p22_pre = 1; this->p23_pre = 0;
       this->p31_pre = 0; this->p31_pre = 0; this->p33_pre = 1; 
    }
    ~IdFriction(){

    }
    // attribute for inputs
    float omega_imu, omega_gyro;
    float y_pre;
    int16_t Torque, T_pre;
    float loadMass;
    // attribute for Identification
    float phi1, phi2, phi3;
    float theta1, theta2, theta3;
    float theta1_pre, theta2_pre, theta3_pre;
    float p11, p12, p13, p21, p22, p23, p31, p32, p33;
    float p11_pre, p12_pre, p13_pre, p21_pre, p22_pre, p23_pre, p31_pre, p32_pre, p33_pre;
    float phiT_p_phi;
    float y;
    float L1, L2, L3;
    float epsi;
    float k, T1, k_f;
    float T_compenstate;
    // method
    void calculIdFriction(void);
};

class BiasFilter{
    private:
    const float Q_angle = 0.01;
	const float Q_bias = 0.0000032;
	const float R_measure = 0.3;
    const float dt = 0.001;
    public:
    // attribute
	float angle;
	float bias;
	float P_00;
	float P_01;
	float P_10;
	float P_11;
	float theta, theta_init;
	float newAngle, newRate, rate;
    // input
    float acc_Y_Imu, acc_X_Imu, gyro_X_filter;
	// method
    void calculBiasFilter(void);
};

class Identification{
    private:
    float lamda;
    public:
    Identification(){
        this->lamda = 0.995;
        this->theta1_pre = 0; this->theta2_pre = 0;
        this->p11_pre = 1; this->p12_pre = 0; this->p21_pre = 0; this->p22_pre = 0;
        this->y = 0; 
        this->Torque = 0;
    } 
    ~Identification() {}
    // atribute
    float y, y_pre;
    int16_t Torque, T_pre;
    float phi1, phi2;
    float epsi;
    float theta1, theta2, theta3;
    float theta1_pre, theta2_pre, theta3_pre;
    float L1, L2, L3;
    float p11, p12, p21, p22;
    float p11_pre, p12_pre, p21_pre, p22_pre;
    float phiT_p_phi;
    float k, T1;
    // input
    float omega_X_Gyro, omega_X_Imu, omega_X_Imu_pre, T_compenstate;
    // method
    void calculIdentification(void);
};


class Controller{
private:
    
public:
    Controller();
    ~Controller();

    // Attributes
    Controller01 controller01;
    Controller02 controller02;

    Controller03 controller03;

    Controller04 controller04;

    Controller03 pidPosParam;

    CompenstateFriction compensateFriction; 

    Identification identification;
    
    IdFriction idFriction;
    
    BiasFilter biasFilter;
    // controller
    OriginPIDController originPIDController;
    ReCognitionController recogController;
    NumericalPIDController numerPIDController;


    // Methods
    void caculController01(void);
    void caculController02(void);
    void caculController03(void);
    void caculController04(void);
    void caculPosController(void);

    void caculCompensateFriction(void);
    void calculIdFriction(void);
    void calculBiasFilter(void);
};



#endif
