/**
******************************************************************************
* @file           : sensor_manage_worker.h
* @brief          : header file of worker for sensor management
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef SENSOR_MANAGE_WORKER_H
#define SENSOR_MANAGE_WORKER_H

/* Inlcude ------------------------------------------------------------------*/
#include "logger/sys_logger.h"
#include "sensor/imu_Sfog.h"
#include "sensor/gyro_u2x00d.h"

class SensorManageWorker
{
private:
    // attributes
    std::shared_ptr<spdlog::logger>  logger;
    
public:
    SensorManageWorker();
    ~SensorManageWorker();

    // attributes
    SFogIMU sfogIMU;
    GyroInterface memGyro;

    // Methods
    void run(void);
    void logDebug(void);
    void logFile(void);
    void logFileIMU(void);
};


/* public Object ---------------------------------------------------------------*/
extern SensorManageWorker sensorManageWorker;

#endif
