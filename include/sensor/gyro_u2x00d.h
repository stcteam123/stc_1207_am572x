/**
******************************************************************************
* @file           : gyro_u2x00d.h
* @brief          : header file of Gyro device
******************************************************************************
******************************************************************************
*/

#ifndef GYRO_U2X00D_H
#define GYRO_U2X00D_H

#include <string>
#include <string.h>
#include "comm/uart_interface.h"
#include "logger/sys_logger.h"

// ma tran A
#define A1			0.999250593420717
#define A2			0.000749406579284
#define A3			0.000749406579284
#define A4			0.999250593420717
// ma tran B
#define B1			0.001499437804558
#define B2			0.000000562195442
#define B3			0.000000562195442
#define B4			0.001499437804558
// do loi Kalman
#define K1			0.499979167534686
#define K2			0.499979167534686


typedef struct GyroData_t
{
	long double  x_axis;
	long double  y_axis;
	long double  x_axis_filter;
	long double  y_axis_filter;
	bool flag_data;	
	bool gyro_state;
} GyroData_s;

class GyroInterface
{
public:
	GyroInterface(std::string uartName, uint32_t baudRate);
	GyroInterface();
	~GyroInterface();

	bool init(std::string port, uint32_t baudrate);
	int readGyro(void);
	GyroData_s getData(void);
	void checkGyroStatus(void);

private:
	// attributes
	std::shared_ptr <spdlog::logger> logger;
	UartInterface uartGyro;
	
	GyroData_s gyroData;
	uint16_t time_check_status;
};

#endif // !GYRO_U2X00D_H
