/**
******************************************************************************
* @file           : mcu_onboard_interface.h
* @brief          : header file of MCU On board Interface
* @author		  : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef MCU_ONBOARD_INTERFACE
#define MCU_ONBOARD_INTERFACE

/* Include ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h"
#include "comm/uart_interface.h"

class MCUData
{
public:
	bool encEL_status;
	double encEL_value;
	bool encAZ_status;
	double encAZ_value;
};

class MCUInterface
{
public:
	MCUInterface();
	~MCUInterface();
	
	void run(void);
	MCUData getData(void);

private:
	// logger
	std::shared_ptr<spdlog::logger> logger;
	UartInterface mcu_port;
	MCUData mcu_data;
	
	void recvData(void);
	
	
};


				  
#endif // !MCU_ONBOARD_INTERFACE

