/**
******************************************************************************
* @file           : XtrapulsPac.h
* @brief          : header file of XtrapulsPac Drives
* @author 		  : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef XTRAPULSPAC_H
#define XTRAPULSPAC_H

#include "comm/canbus_socket.h"
#include "sensor/gyro_u2x00d.h"
#include "logger/sys_logger.h"

#define EL_DRIVER			0x02
#define AZ_DRIVER			0x04

#define  PI  3.1415926535897

enum DeviceState{
	State_Disable, 
	State_ReadySwitchOn, 
	State_Operation, 
	State_Unknow
};

typedef struct ServoMotor_t
{
	uint16_t m_netId;
} ServoMotor_s;


enum DriverState{
	DRIVER_READY,
	DRIVER_NOT_READY
};


class DriverInfor
{
private:
	
public:
	int32_t resol_pos;
	int32_t resol_vel;
	bool flag_get_data;

	DriverState state;
};


class MotorDriver
{
public:
	MotorDriver();
	~MotorDriver();

	// Methods
	bool initComm(uint16_t id);
	bool initDriver(void);
		
	// Driver Init
	bool setOperational(void);
	bool setSwitchOn(void);
	bool setTorqueMode(void);
	bool setEnable(void);
	
	
	int readIGBTError(void); // read to check IGBT error
	int checkReady(void); // check ready
	int resetDriver(void); //  reset driver
	int mapTorqueRPDO2(void); // turn off ACK CAN-Bus
	int mapResolTDPO2(void);
	int mapVelocityTDPO3(void);
	int setResolTime(void); // 1ms
	int setVelocityTime(void); // 1ms
	int activeResol(void);
	int activeVelocity(void);
	
	int MotorUpdateNetState(void);
	
	// profile Torque Mode
	int setTargetTorque(int16_t value);
	int setInputModeTorque(uint32_t value);
	int setSlopeTorque(uint32_t value);
	int setProfileTypeTorque(int16_t value);
	int setOffsetTorque(int16_t value);
	
	
	// control - position
	int MotorSetProfileVelocity(uint32_t Speed);
	int SetTarPos(int32_t tarposition);
	int SetDevState(DeviceState  state);
	int StartMovPos(void);	

	// interpolate - position
	int SetRefInterpolPos(int32_t RefPosition);
	int EnInterpolPos(void);
	int ReqResolPos(void);
	int32_t recvData(void);
	DriverInfor getData(void);
	
	// Attributes
	CanSocket canOpen;
private:
	std::shared_ptr <spdlog::logger> logger;
	uint16_t driver_id;
	DriverInfor driverData;
	
};

#endif // !XTRAPULSPAC_H