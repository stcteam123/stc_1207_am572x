/**
******************************************************************************
* @file           : driver_manage_worker.h
* @brief          : Header file of worker for driver management
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef DRIVER_MANAGE_WORKER_H
#define DRIVER_MANAGE_WORKER_H

/* Include ------------------------------------------------------------------*/
#include "logger/sys_logger.h"
#include "motor_driver/XtrapulsPac.h"

class DriverManageWorker
{
private:
    // Attributes
    std::shared_ptr <spdlog::logger> logger;


    // Methods    
public:
    DriverManageWorker();
    ~DriverManageWorker();

    // Attributes
    MotorDriver ELDriver, AZDriver;


    // Methods
    void run(void);
};


/* Public Object --------------------------------------------------------------*/
extern DriverManageWorker driverManageWorker;

#endif