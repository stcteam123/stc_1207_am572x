/**
******************************************************************************
* @file           : remote_control.h
* @brief          : header file of Joystick interface
* @author 		  : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef REMOTE_CONTROL_H
#define REMOTE_CONTROL_H

#include "comm/canbus_socket.h"
#include "logger/sys_logger.h"

#define JOYSTICK_ID				0x0180
#define KEYBOARD_ID				0x0100


class KeyBoardInfor{
private:
	
public:
	KeyBoardInfor();
	~KeyBoardInfor();

	enum state{
		ACTIVE = '1',
		INACTIVE = '0'
	};

	// Attributes
	state stb_on_off;
	state auto_tracking;
	state go_home;
	
	state fire_01;
	state fire_02;
	state fire_03;
	
	state arm;
	state offset_up;
	state offset_down;
	state offset_left;
	state offset_right;
	state focus_on_off;
	state zoom_in;
	state zoom_out;
	state switch_cam;
	state lrf;	
};



class JoystickInfor{
private:
	
public:
	JoystickInfor();
	~JoystickInfor();

	enum state{
		ACTIVE,
		INACTIVE
	};

	// Attributes
	int16_t x_axit;
	int16_t y_axit;
	state joy_state;
};

class RemoteControl{
public:
	RemoteControl();
	~RemoteControl();
	
	KeyBoardInfor keyboard;
	
	int recvData(void);
	JoystickInfor getJoyData(void);
	void checkJoyStatus(void);

private:
	// Attributes
	std::shared_ptr <spdlog::logger> logger;
	uint32_t myID;
	CanSocket myCanBus;
	JoystickInfor joystick;

	uint16_t time_check_status;
};

#endif // !REMOTE_CONTROL_H
