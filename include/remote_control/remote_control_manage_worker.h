/**
******************************************************************************
* @file           : remote_control_mange_worker.h
* @brief          : header file of worker for remote control management
* @author 		  : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef REMOTE_CONTROL_MANAGE_WORKER_H
#define REMOTE_CONTROL_MANAGE_WORKER_H

/* Include ---------------------------------------------------------------------*/
#include "remote_control/remote_control.h"


class RemoteControlManageWorker
{
private:
    
public:
    RemoteControlManageWorker();
    ~RemoteControlManageWorker();

    // attributes
    RemoteControl remoteControl;


    //methods
    void run(void);
};

/* Public Object -----------------------------------------------------------------*/
extern RemoteControlManageWorker remoteControlMangeWorker;


#endif
