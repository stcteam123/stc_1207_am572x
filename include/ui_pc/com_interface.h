/**
******************************************************************************
* @file           : fm_update.h
* @brief          : header file of update firmware over CanBus
******************************************************************************
******************************************************************************
*/

#ifndef FM_UPDATE_H
#define FM_UPDATE_H

#include <stdint.h>
#include "comm/tcp_socket.h"
#include "rapidjson/writer.h"
#include "controller/Torque_Controller.h"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h"

using namespace rapidjson;

#define APP_USER_NAME		"tuan"
#define APP_USER_PASS		"1234"

#define APP_ROOT_NAME		"root"
#define APP_ROOT_PASS		"admin"

#define AUTHEN_MSG			"Authen"

typedef enum
{
	DISCOONECTED = 0,
	CONNECTED
} ConnStatus; 

typedef enum
{
	APP_USER = 1,
	APP_ROOT = 2
} AppPermission;

typedef struct PCServiceTimeValue_t
{
	int test_servo_all;
	int test_joystick_all;
	
	int get_min_position_AZ;
	int get_max_position_AZ;
	int get_min_position_EL;
	int get_max_position_EL;
	int get_imu_status;
	
	int get_joystick_status;
	int get_joystick_value;
	
} PCServiceTimeValue_s;

class PCInterface
{
public:
	PCInterface();
	~PCInterface();
	
	void run(TorqueController *controller);
	int init(void);
	int sendData(std::string data);
	
private:
protected:
	Socket newSock;
	TcpConnection TcpServer;
	bool conn_status = DISCOONECTED;
	std::shared_ptr<spdlog::logger> logger;

};

class PCService
{
public:
	PCService(TcpConnection *p_client);
	~PCService();
	
	void run(TorqueController *p_controller);
	void runService(void);
	std::string recvPacket(void);
	void sendPacket(std::string json);
	
	bool login(std::string userName, std::string pass);
	void sendLoginAck(void);
	
	void sendMinPositionAZ(void);
	void sendMaxPositionAZ(void);
	void sendMinPositionEL(void);
	void sendMaxPositionEL(void);
	
	void sendJoystickStatus(void);
	void sendJoystickValue(void);
	
	void sendIMUStatus(void);

private:
	TcpConnection newClient;
	AppPermission permission;
	TorqueController *controller;
	PCServiceTimeValue_s timeValue;
	bool connecttion_status;
	std::shared_ptr<spdlog::logger> logger;
	
};

#endif // !FM_UPDATE_H
