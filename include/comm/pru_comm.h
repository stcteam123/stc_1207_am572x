/**
******************************************************************************
* @file           : pru_comm.h
* @brief          : header file of pru-icss communication
* @Author		  : Tuan-NT
******************************************************************************
******************************************************************************
*/
#ifndef PRU_COMM_H
#define PRU_COMM_H
#include <stdint.h>
#include <stdbool.h>
#include <string>

#define PRU_CHANEL_01			"/dev/rpmsg_pru30"

class PruComm
{
public:
	PruComm();
	~PruComm();
	
	bool init(std::string chanel);
	int readData(uint8_t *buff);
	bool sendData(uint8_t *buff, uint8_t len);

private:
	int pru_fd;
	
};


#endif // !PRU_COMM_H
