
/**
******************************************************************************
* @file           : tcp_socket.h
* @brief          : header file of tcp socket
******************************************************************************
******************************************************************************
*/

#ifndef SOCKET_H
#define SOCKET_H

/* Includes ------------------------------------------------------------------*/
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/tcp.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <string>
#include <iostream>


class TcpConnection
{
public:
	TcpConnection();
	~TcpConnection();
	int new_socket;
	int stt_conn;

	int connect(std::string host, uint16_t port);
	void close(void);
	int init(uint16_t port);
	TcpConnection accept(void);
	int send(std::string data);
	int send(char *buff, int len);
	int receive(char *buff, int len);
	int keepalive(void);
};

class Socket
{
public:
	Socket();
	~Socket();

	TcpConnection init(void);
	int close(void);

protected:
	int stt_conn;


};


#endif // !SOCKET_H
