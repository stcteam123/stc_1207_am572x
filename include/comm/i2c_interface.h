/**
******************************************************************************
* @file           : i2c_interface.h
* @brief          : header file of i2c interface
******************************************************************************
******************************************************************************
*/

#ifndef I2C_INTERFACE_H
#define I2C_INTERFACE_H

#include <stdint.h>
#include <iostream>
#include <string.h>

#define I2C0				"/dev/i2c-1"

class I2cInterface
{
public:
	I2cInterface();
	I2cInterface(std::string i2cName, uint8_t devAddr);
	~I2cInterface();

	int writeReg(uint8_t reg_addr, uint8_t value);
	int readReg(uint8_t reg_addr, int *value);

private:
	int i2c_fd;
	uint8_t dev_addr;
	
	int init(std::string i2c_name);
	int close(void);
};

#endif // !I2C_INTERFACE_H
