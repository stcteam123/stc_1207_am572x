/**
******************************************************************************
* @file           : uart_interface.h
* @brief          : header file of uart interface
******************************************************************************
******************************************************************************
*/

#ifndef UART_INTERFACE_H
#define UART_INTERFACE_H

#include <stdint.h>
#include <string>

#define UART0			"/dev/ttyUSB0"
#define UART1			"/dev/serial0"
#define UART5			"/dev/ttyS4"
#define UART3			"/dev/ttyS2"

#define UART4			"/dev/ttyS3"
#define UART6			"/dev/ttyS5"
#define UART7			"/dev/ttyS6"
#define UART8			"/dev/ttyS7"
#define UART10			"/dev/ttyS9"


class UartInterface
{
public:
	UartInterface();
	~UartInterface();
	
	int init(std::string uart_name, uint32_t uart_baudrate);
	int setBaudDivisor(int baudRate);
	int close(void);
	int send(uint8_t *buff, int len);
	int read(uint8_t *buff, int len);
	int read(void);
	int readln(uint8_t *buff, int max_len);
	void flushBuff(void);
	
	int getBuffAvailable(void);

private:
	int new_uart;
	std::string uartName;
	uint32_t baudrate;
	
};



#endif // !UART_INTERFACE_H
