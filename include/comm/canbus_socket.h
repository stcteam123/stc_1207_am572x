/**
******************************************************************************
* @file           : canbus_socket.h
* @brief          : header file of CanBus socket
******************************************************************************
******************************************************************************
*/

#ifndef CANBUS_SOCKET_H
#define CANBUS_SOCKET_H

#include <sys/socket.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <linux/can.h>
#include <linux/if.h>
#include <fcntl.h>
#include <iostream>
#include <string.h>
#include <unistd.h>
#include <stdint.h>


#define CAN1_INTERFACE				"can1"
#define CAN0_INTERFACE				"can0"


class CanSocket
{
public:
	CanSocket();
	~CanSocket();

	int init(std::string can_name);
	int send(uint16_t id, uint8_t *data, uint8_t len);
	int recv(uint8_t * buff, uint16_t *id);
	
	uint32_t error_num;

protected:
	int cansock;
private:

};

#endif // CANBUS_SOCKET_H

