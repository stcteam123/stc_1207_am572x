/**
******************************************************************************
* @file           : spidev.h
* @brief          : header file of spi
******************************************************************************
******************************************************************************
*/

#ifndef SPI_INTERFACE_H
#define SPI_INTERFACE_H

#include <stdint.h>
#include <iostream>


#define SPI0				"/dev/spidev0.0"
#define SPI1				"/dev/spidev0.1"

class SpiInterface
{
public:
	SpiInterface(std::string spiName);
	SpiInterface(std::string spiName, char spiMode, char spiBitNum, uint32_t spiSpeed);
	~SpiInterface();
	
	
	int send(char *buff, uint16_t len);
	int recv(char *buff);
	int sendRecv(uint8_t *tx_rx_buff, uint16_t len);

private:
	char spi_mode;
	char spi_bit_num;
	uint32_t spi_speed;
	int new_spi;
	
	int init(std::string name);
	int close(void);
protected:
	
	
};


#endif // !SPI_INTERFACE_H
