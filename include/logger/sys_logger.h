/**
******************************************************************************
* @file           : sys_logger.h
* @brief          : header file of system logger
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef SYS_LOGGER_H
#define SYS_LOGGER_H

/* Include ------------------------------------------------------------------*/
#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h"
#include <string>

namespace SysLogger{
    class Logger{
    private:
        std::string log_path;
        std::string sys_log_file;
        std::string gateway_log_file;
        
    public:
        Logger();
        ~Logger();

        void init(void);

        // logger
        std::shared_ptr<spdlog::logger>  sysLogger;
    };
}


/* object and variable Public ---------------------------------------------------------*/
extern SysLogger::Logger myLogger;

#endif
