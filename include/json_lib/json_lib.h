/**
******************************************************************************
* @file           : json_lib.h
* @brief          : header file of Json Lib
******************************************************************************
******************************************************************************
*/
#ifndef JSON_LIB_H
#define JSON_LIB_H
#include "rapidjson/document.h"

class JsonClass
{
public:
	JsonClass();
	~JsonClass();
	int parse(std::string json);
	std::string getStringValue(std::string json_key);
	int getIntValue(std::string json_key);

private:
	rapidjson::Document jsonObj;
};



#endif // !JSON_LIB_H
