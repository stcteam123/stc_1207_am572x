/**
******************************************************************************
* @file           : json_lib.cpp
* @brief          : source file of Json Lib
******************************************************************************
******************************************************************************
*/
#include "json_lib/json_lib.h"

JsonClass::JsonClass()
{
}

JsonClass::~JsonClass()
{
}

int JsonClass::parse(std::string json)
{
	rapidjson::ParseResult result = this->jsonObj.Parse(json.c_str());
	if (!result)
	{
		throw std::string("parse json: error");
	}
	return 1;
}

std::string JsonClass::getStringValue(std::string json_key)
{
	// check validate
	if (this->jsonObj.HasMember(json_key.c_str()) == true)
	{
		if (this->jsonObj[json_key.c_str()].IsString() == true)
		{
			return this->jsonObj[json_key.c_str()].GetString();
		}
		else
		{
			throw std::string("is not a string");
			return "";
		}
	}
	else
	{
		throw std::string("key not found");
		return "";
	}
}

int JsonClass::getIntValue(std::string json_key)
{
	// check validate
	if (this->jsonObj.HasMember(json_key.c_str()) == true)
	{
		if (this->jsonObj[json_key.c_str()].IsInt() == true)
		{
			return this->jsonObj[json_key.c_str()].GetInt();
		}
		else
		{
			throw std::string("is not a Integer");
			return -1;
		}
	}
	else
	{
		throw std::string("key not found");
		return -1;
	}
}