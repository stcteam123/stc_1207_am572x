/**
******************************************************************************
* @file           : sensor_manage_worker.cpp
* @brief          : source file of worker for sensor management
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/

/* Inlcude ------------------------------------------------------------------*/
#include "sensor/sensor_manage_worker.h"
#include "comm/uart_interface.h"
#include <thread>
#include <unistd.h>
#include <fstream>


/* object ---------------------------------------------------------------------------------------------------*/
SensorManageWorker sensorManageWorker;

SensorManageWorker::SensorManageWorker()
{
    this->logger = spdlog::basic_logger_mt("sensor_manage_worker_log", "/home/1207/logMonitor/sensor/sensor_manage_worker_log.txt");
}

SensorManageWorker::~SensorManageWorker()
{
}

void SensorManageWorker::run(void){
    bool sfog_imu_init = false;
    bool mem_gyro_init = false;

    myLogger.sysLogger->info("Starting Worker for sensor management....OK!");
    this->logger->info("Starting Worker for sensor management....OK!");
    while (1)
    {
        if(!sfog_imu_init){
            if(this->sfogIMU.init(UART5, 921600) == false){
                myLogger.sysLogger->error("Init port {0} for SfogIMU: False", UART0);
                this->logger->error("Init port {0} for SfogIMU: False", UART0);
            }
            else{
                myLogger.sysLogger->info("Init port {0} for SfogIMU: Success", UART0);
                this->logger->info("Init port {0} for SfogIMU: Success", UART0);
                sfog_imu_init = true;
            }
        }

        if(!mem_gyro_init){
            if(this->memGyro.init(UART6, 921600) == false){
                myLogger.sysLogger->error("Init port {0} for memGyro: False", UART6);
                this->logger->error("Init port {0} for memGyro: False", UART6);
            }
            else{
                myLogger.sysLogger->info("Init port {0} for memGyro: Success", UART6);
                this->logger->info("Init port {0} for memGyro: Success", UART6);
                mem_gyro_init = true;
            }
        }

        if(sfog_imu_init && mem_gyro_init){
            break;
        }
        sleep(1);
    }

    myLogger.sysLogger->info("Start SfogImuThread....OK!");
    this->logger->info("Start SfogImuThread....OK!");


    std::thread SfogImuThread(&SFogIMU::readData, &this->sfogIMU);
    std::thread logDebugThread(&SensorManageWorker::logDebug, this);
    std::thread logFileThread(&SensorManageWorker::logFile, this);
    // std::thread logFileThread(&SensorManageWorker::logFileIMU, this);

    myLogger.sysLogger->info("Start MemGyroThread....OK!");
    this->logger->info("Start MemGyroThread....OK!");
    std::thread MemGyroThread(&GyroInterface::readGyro, &this->memGyro);

    SfogImuThread.join();
    myLogger.sysLogger->info("Stopped Worker for sensor management....OK!");
    this->logger->info("Stopped Worker for sensor management....OK!");
    MemGyroThread.join();
    myLogger.sysLogger->info("Stopped Worker for sensor management....OK!");
    this->logger->info("Stopped Worker for sensor management....OK!");
}


void SensorManageWorker::logDebug(void){
    while (1){
        //ImuSfogData_s data;
        // GyroData_s data;
        // data = this->memGyro.getData();
        // this->logger->info("get Imu Data: x_axis = {0}, y_axis = {1}", data.x_axis, data.y_axis);
        ImuSfogData_s imuData;
        GyroData_s gyroData;
        gyroData = this->memGyro.getData();
        imuData = this->sfogIMU.getData();
        this->logger->info("get sensor Data: imu_x = {0}, imu_y = {1}, imu_z = {2}, gyro_x = {3}, gyro_y = {4}", imuData.ang_vel_x, imuData.acc_x, imuData.acc_z, gyroData.x_axis_filter, gyroData.y_axis_filter);
        usleep(500000);
    }
}



void SensorManageWorker::logFile(void){
    std::ofstream writeFile;

    writeFile.open("/home/1207/logFile/log_mem_25_02.txt");
    writeFile << "x_axis" << "\t" << "y_axis" << "\t" << "x_axis_filter" << "\t" << "y_axis_filter" << std::endl;

    while (1){
        //ImuSfogData_s data;
        GyroData_s data;
        data = this->memGyro.getData();
        writeFile << data.x_axis << "\t" << data.y_axis << "\t" << data.x_axis_filter << "\t" << data.y_axis_filter << std::endl;
        usleep(10000);
    }
    
}


void SensorManageWorker::logFileIMU(void){
    std::ofstream writeFile;

    writeFile.open("/home/1207/logFile/log_imu_12_03.txt");
    writeFile << "ang_vel_x" << "\t" << "ang_vel_y" << "\t"  << "ang_vel_z" << "\t" << "acc_x" << "\t" << "acc_y" << "\t" << "acc_z" << "\t" << "heading" << "\t" << "roll" << "\t" << "pitch" << "\t" << "angle" << "\t" << "rate" << std::endl;

    while (1){
        ImuSfogData_s data;
        data = this->sfogIMU.getData();
        writeFile << data.ang_vel_z << "\t" << data.acc_x << "\t" << data.acc_y << "\t" << data.acc_z << "\t" << data.heading_data << "\t" << data.roll_data << "\t" << data.pitch_data << "\t" << data.angle_z_cacul << "\t" << data.rate_z_cacul << std::endl;
        usleep(10000);
    }
    
}