/**
******************************************************************************
* @file           : gyro_u2x00d.cpp
* @brief          : source file of Gyro device
******************************************************************************
******************************************************************************
*/

#include "sensor/gyro_u2x00d.h"
#include <fstream>
#include <iostream>
#include <sys/time.h>
#include <unistd.h>
#include <thread>


GyroInterface::GyroInterface()
{
	this->logger = spdlog::basic_logger_mt("memGyro_Logger", "/home/1207/logMonitor/sensor/memGyro_Logger.txt");
	
	this->gyroData.x_axis_filter = 0;
	this->gyroData.y_axis_filter = 0;
	this->gyroData.flag_data = 0;
	
	this->gyroData.gyro_state = 0;
	this->time_check_status = 0;
}

GyroInterface::~GyroInterface()
{
}

bool GyroInterface::init(std::string port, uint32_t baudrate){
	// init port and baudrate for Gyro
	if (!this->uartGyro.init(port, baudrate))
	{
		this->logger->info("init port {0} false", port);
		return false;
	}
	this->logger->info("init port {0} success", port);
	return true;
}

int GyroInterface::readGyro(void)
{
	uint8_t i = 0;
	uint8_t buff[6] = { 1, 2, 3, 4, 5, 6 };
	uint8_t checksum = 0;
	//int check_sys = 0;	
	
	// Constant
	long double qw = 2 * (63.0 / 500000);
	long double qb = ((63 / 50.0)) / (36 * 36 * 3600.0);
	long double R = (0.03 * 0.03 * 500000) / 63.0;
	
	// filter x_axis
	// Initial
	long double  xe1 = 0;    // data out
	long double  xe2 = 0;
	long double  pe1 = 0;
	long double  pe2 = 0;
	long double  pe3 = 0;
	long double  pe4 = 0; 

	// Prediction
	long double xp1 = xe1;
	long double xp2 = xe2;
	long double pp1 = pe1 + qw;
	long double pp2 = pe2;
	long double pp3 = pe3;
	long double pp4 = pe4 + qb; 	
	
	// Estimation
	long double s;
	long double k1;
	long double k2;
	long double Z;	

	// y_axis
	// Initial
	long double  xe1_y_axis = 0;    // data out
	long double  xe2_y_axis = 0;
	long double  pe1_y_axis = 0;
	long double  pe2_y_axis = 0;
	long double  pe3_y_axis = 0;
	long double  pe4_y_axis = 0; 

	// Prediction
	long double xp1_y_axis = xe1;
	long double xp2_y_axis = xe2;
	long double pp1_y_axis = pe1 + qw;
	long double pp2_y_axis = pe2;
	long double pp3_y_axis = pe3;
	long double pp4_y_axis = pe4 + qb; 	
	
	// Estimation
	long double s_y_axis;
	long double k1_y_axis;
	long double k2_y_axis;
	long double Z_y_axis;		

	this->logger->info("Start read data Gyro");
	
	std::thread checkGyroThread(&GyroInterface::checkGyroStatus, this);
	while (true)
	{
		int get_buff_available_debug = 0;
		get_buff_available_debug = this->uartGyro.getBuffAvailable();
		if (get_buff_available_debug > 500)
		{
			this->logger->info("check buffer over 500 {0:d}: ", get_buff_available_debug);
			this->uartGyro.flushBuff();
		}
			
		while (get_buff_available_debug < 6)
		{
			usleep(1);
			get_buff_available_debug = this->uartGyro.getBuffAvailable();
		}
		
		for (; i < 6; i++)
		{
			do
			{
				buff[i] = uartGyro.read();		
			} while (buff[i] < 0);
		}
	
		checksum = buff[0] + buff[1] + buff[2] + buff[3] + buff[4];
		checksum = (~(checksum) + 1);
		if ((buff[0] == 0)&&(buff[1] == 0)&&(buff[2] == 0)&&(buff[3] == 0)&&(buff[4] == 0))
			continue;
		// check sum
		if (checksum == buff[5])
		{
			// get sync
			//check_sys = (buff[4] & 0xC0) >> 6;
			// check validate
			int data[2];
			int x_axis = 0;
			int y_axis = 0;

			if ((buff[4] & 0x10) == 0x10)
			{
				// get data
				data[0] = ((int)buff[0]) | ((int)buff[1] << 8) | ((int)(buff[2] & 0x03) << 16);
				if (data[0] > 131071)
				{
					data[0] -= 262143;
				}
				x_axis = data[0];							
			}
			else
			{
				this->logger->error("check validate: error");
			}
			if ((buff[4] & 0x20) == 0x20)
			{
				data[1] = ((int)(buff[2] & 0xFC) >> 2) | ((int)buff[3] << 6) | ((int)(buff[4] & 0x0F) << 14);
				if (data[1] > 131071)
				{
					data[1] -= 262143;
				}
				y_axis = data[1];
			}
			else
			{
				this->logger->error("check validate: error");
			}
			i = 0;
			// filter data[0];
			xp1 = xe1;
			xp2 = xe2;
			pp1 = pe1 + qw;
			pp2 = pe2;
			pp3 = pe3;
			pp4 = pe4 + qb; 	
			
			// Estimation
			s = pp1 + pp2 + pp3 + pp4 + R;
			k1 = (pp1 + pp2) / s;
			k2 = (pp3 + pp4) / s;
			Z = 0;				
			Z = ((long double)x_axis) / 771.0;
			xe1 = xp1 + (Z - xp1 - xp2)*k1;   // Z: raw data input
			xe2 = xp2 + (Z - xp1 - xp2)*k2;
			pe1 = (1 - k1) * pp1 - k1 * pp3;
			pe2 = (1 - k1) * pp2 - k1 * pp4;
			pe3 = -k2 * pp1 + (1 - k2) * pp3;
			pe4 = -k2 * pp2 + (1 - k2) * pp4;
				
//			if ((xe1 >= -0.015)&&(xe1 <= 0.015))
//			{
//				xe1 = 0;
//			}

			xp1_y_axis = xe1_y_axis;
			xp2_y_axis = xe2_y_axis;
			pp1_y_axis = pe1_y_axis + qw;
			pp2_y_axis = pe2_y_axis;
			pp3_y_axis = pe3_y_axis;
			pp4_y_axis = pe4_y_axis + qb; 	
			
			// Estimation
			s_y_axis = pp1_y_axis + pp2_y_axis + pp3_y_axis + pp4_y_axis + R;
			k1_y_axis = (pp1_y_axis + pp2_y_axis) / s_y_axis;
			k2_y_axis = (pp3_y_axis + pp4_y_axis) / s_y_axis;
			Z_y_axis = 0;				
			Z_y_axis = ((long double)y_axis) / 771.0;
			xe1_y_axis = xp1_y_axis + (Z_y_axis - xp1_y_axis - xp2_y_axis)*k1_y_axis;   // Z: raw data input
			xe2_y_axis = xp2_y_axis + (Z_y_axis - xp1_y_axis - xp2_y_axis)*k2_y_axis;
			pe1_y_axis = (1 - k1_y_axis) * pp1_y_axis - k1_y_axis * pp3_y_axis;
			pe2_y_axis = (1 - k1_y_axis) * pp2_y_axis - k1_y_axis * pp4_y_axis;
			pe3_y_axis = -k2_y_axis * pp1_y_axis + (1 - k2_y_axis) * pp3_y_axis;
			pe4_y_axis = -k2_y_axis * pp2_y_axis + (1 - k2_y_axis) * pp4_y_axis;

			//x_axis_sum += ((long int)data[0] + 7.2);
			this->gyroData.gyro_state = 1;
			this->time_check_status = 0;
			while (this->gyroData.flag_data)
			{
				usleep(1);
			}
			this->gyroData.flag_data = 1;
			this->gyroData.x_axis = ((long double)x_axis) / 771.0;
			this->gyroData.y_axis = ((long double)y_axis) / 771.0;			
			this->gyroData.x_axis_filter = xe1 - 0.001;
			this->gyroData.y_axis_filter = xe1_y_axis;
			this->gyroData.flag_data = 0;
			// std::cout << "debug" << this->gyroData.x_axis << "   " << this->gyroData.y_axis << std::endl;
			//gyroFile << this->gyroData.x_axis << "\t" << this->gyroData.x_axis_filter << std::endl;
//			x_axis_sum_filter += xe1;			
//			x_axis_sum += x_axis;
//			y_axis_sum += y_axis;
		}
		else
		{
			this->logger->error("check sum: error");
			for (i = 0; i < 5; i++)
			{
				buff[i] = buff[i + 1];
			}
		}
	}
}

GyroData_s GyroInterface::getData(void)
{
	GyroData_s data;
	while (this->gyroData.flag_data)
	{
		usleep(1);
	}
	this->gyroData.flag_data = 1;
	data = this->gyroData;
	this->gyroData.flag_data = 0;
	return data;
}

void GyroInterface::checkGyroStatus(void)
{
	while (true)
	{
		if (this->time_check_status > 50)
		{
			this->time_check_status = 0;
			this->gyroData.gyro_state = 0;
			this->logger->info("check gyro status: not found");
		}
		else
		{
			usleep(10000);
			this->time_check_status++;
		}
	}
}