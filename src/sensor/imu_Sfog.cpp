/**
******************************************************************************
* @file           : gyro_Sfog.cpp
* @brief          : source file of Gyro Spatial Fog device
******************************************************************************
******************************************************************************
*/

#include "sensor/imu_Sfog.h"
#include "iostream"
#include <fstream>
#include "sys/time.h"
#include "unistd.h"
#include "time.h"
#include <thread>
#include <sys/mman.h>
#include <math.h>


#define PI2  3.1415926535897

SFogIMU::SFogIMU()
{
	this->logger = spdlog::basic_logger_mt("SfogIMU_Logger", "/home/1207/logMonitor/sensor/SfogIMU_Logger.txt");
	this->imuSfogData.imu_state = 0;
}

SFogIMU::~SFogIMU()
{
}


bool SFogIMU::init(std::string port, uint32_t baudrate){
	// init uart_port for imu
	if (!this->uartPort.init(port, baudrate))
	{
		this->logger->info("Init port {0} : False", port);
		return false;
	}
	this->logger->info("Init port {0} : Success", port);
	return true;
}


bool SFogIMU::initPru(void)
{
	this->pru_state = false;
	while (true)
	{
		if (this->pruImu.init(PRU_CHANEL_01) == true)
		{
			this->pru_state = true;
			std::cout << "init PRU Chanel: success" << std::endl;
			return true;
		}
		usleep(200000);
	}
}

int SFogIMU::readData(void)
{
	
	uint8_t buff_read[5] = { 0 };
	uint8_t check_lrc = 0;
	uint16_t crc = 0xFFFF;
	uint8_t i = 0;
	GyroSfogPacket_s packet;	
		
	std::thread checkIMUThread(&SFogIMU::checkIMUStatus, this);
	this->logger->info("Start read data IMU");
	
//	this->initPru();
//	while (true)
//	{
//		this->getDataPRU();
//		usleep(300);
//		
//	}
//	checkIMUThread.join();
	
	while (true)
	{
		//usleep(10);
		
		if (i == 0)
		{
			int get_buff_available_debug2 = 0;
			
			while (get_buff_available_debug2 < 5)
			{
				usleep(5);
				get_buff_available_debug2 = this->uartPort.getBuffAvailable();
			}
			uartPort.read(buff_read, 5);
			
		}
		else
		{
			for (; i < 5; i++)
			{
				buff_read[i] = uartPort.read();
			}
		}

		// check Header LRC
		check_lrc = ((buff_read[1] + buff_read[2] +
			buff_read[3] + buff_read[4]) ^ 0xFF) + 1;
		
		if (buff_read[0] == check_lrc)
		{
			// LRC OK, read packet data
			packet.packet_id	= buff_read[1];
			packet.packet_len	= buff_read[2];
			packet.crc			= (uint16_t)buff_read[3] | ((uint16_t)buff_read[4] << 8);
//			for (uint8_t j = 0; j < packet.packet_len; j++)
//			{
//				packet.packet_data[j] = uartGyro.read();
//			}
			
			int get_buff_available_debug = 0;
			
			while (get_buff_available_debug < packet.packet_len)
			{
				usleep(1);
				get_buff_available_debug = this->uartPort.getBuffAvailable();
			}
			uartPort.read(packet.packet_data, packet.packet_len);

			i = 0;
			this->time_check = 0;
			this->imuSfogData.imu_state = 1;
			if ((packet.packet_id == DCM_ORIEN_PACKET_ID)&&(packet.packet_len == DCM_ORIEN_PACKET_LEN))
			{
				crc = 0xFFFF;
				for (uint8_t j = 0; j < packet.packet_len; j++)
				{
					crc = (uint16_t)((crc << 8) ^ crc16_table[(crc >> 8) ^ packet.packet_data[j]]);
				}
				// check CRC
				if(packet.crc == crc)
				{
					uint8_t byte[4] = { 0 };
					// CRC OK, get data
					for(uint8_t j = 0 ; j < 9 ; j++)
					{
						for (uint8_t k = 0; k < 4; k++)
						{
							byte[k] = packet.packet_data[4*j + k];
						}
						this->dataLocker.lock();
						imuSfogData.dcm_data[j] = *(float *)byte;
						this->dataLocker.unlock();
					}
				}
			}
			else if((packet.packet_id == EULER_ORIEN_PACKET_ID)&&(packet.packet_len == EULER_ORIEN_PACKET_LEN))
			{
				crc = 0xFFFF;
				for (uint8_t j = 0; j < packet.packet_len; j++)
				{
					crc = (uint16_t)((crc << 8) ^ crc16_table[(crc >> 8) ^ packet.packet_data[j]]);
				}
				// check CRC
				if(packet.crc == crc)
				{
					uint8_t byte[4] = { 0 };
					// CRC OK, get data
					for (uint8_t k = 0; k < 4; k++)
					{
						byte[k] = packet.packet_data[k];
					}
					this->dataLocker.lock();
					imuSfogData.roll_data = (*(float *)byte) * 180 / PI2;
					for (uint8_t k = 0; k < 4; k++)
					{
						byte[k] = packet.packet_data[k + 4];
					}
					imuSfogData.pitch_data = (*(float *)byte) * 180 / PI2;		
					for (uint8_t k = 0; k < 4; k++)
					{
						byte[k] = packet.packet_data[k + 8];
					}
					imuSfogData.heading_data = (*(float *)byte) * 180 / PI2;
					this->dataLocker.unlock();	
				}
				else
				{
					std::cout << "error CRC" << std::endl;
				}
			}
			else if((packet.packet_id == ANG_VEL_PACKET_ID)&&(packet.packet_len == ANG_VEL_PACKET_LEN))
			{
				
				crc = 0xFFFF;
				for (uint8_t j = 0; j < packet.packet_len; j++)
				{
					crc = (uint16_t)((crc << 8) ^ crc16_table[(crc >> 8) ^ packet.packet_data[j]]);
				}
				// check CRC
				if(packet.crc == crc)
				{				
					uint8_t byte[4] = { 0 };

					for (uint8_t k = 0; k < 4; k++)
					{
						byte[k] = packet.packet_data[k];
					}
					this->dataLocker.lock();
					imuSfogData.ang_vel_x = (*(float *)byte) * 180 / PI2;					
					for (uint8_t k = 0; k < 4; k++)
					{
						byte[k] = packet.packet_data[k + 4];
					}
					imuSfogData.ang_vel_y = (*(float *)byte) * 180 / PI2;
					for (uint8_t k = 0; k < 4; k++)
					{
						byte[k] = packet.packet_data[k + 8];
					}
					imuSfogData.ang_vel_z = (*(float *)byte) * 180 / PI2;
					this->dataLocker.unlock();

					int get_buff_available_check = 0;
					get_buff_available_check = this->uartPort.getBuffAvailable();
					if (get_buff_available_check > 1000)
					{
						this->logger->info("check buffer (>1000): {0:d}", get_buff_available_check);
						this->uartPort.flushBuff();
					}
				}
			}
			else if((packet.packet_id == ACC_PACKET_ID)&&(packet.packet_len == ACC_PACKET_LEN))
			{
				crc = 0xFFFF;
				for (uint8_t j = 0; j < packet.packet_len; j++)
				{
					crc = (uint16_t)((crc << 8) ^ crc16_table[(crc >> 8) ^ packet.packet_data[j]]);
				}
				// check CRC
				if (packet.crc == crc)
				{
					uint8_t byte[4] = { 0 };
					// CRC OK, get data
					for (uint8_t k = 0; k < 4; k++)
					{
						byte[k] = packet.packet_data[k];
					}
					
					this->dataLocker.lock();					
					imuSfogData.acc_x = (*(float *)byte);
					for (uint8_t k = 0; k < 4; k++)
					{
						byte[k] = packet.packet_data[k + 4];
					}
					imuSfogData.acc_y = (*(float *)byte);		
					for (uint8_t k = 0; k < 4; k++)
					{
						byte[k] = packet.packet_data[k + 8];
					}
					imuSfogData.acc_z = (*(float *)byte);
					this->dataLocker.unlock();	
				}
				else
				{
					//std::cout << "error CRC" << std::endl;
				}
			}
			else if ((packet.packet_id == RAW_SENSOR_PACKET_ID) && (packet.packet_len == RAW_SENSOR_PACKET_LEN))
			{
				crc = 0xFFFF;
				for (uint8_t j = 0; j < packet.packet_len; j++)
				{
					crc = (uint16_t)((crc << 8) ^ crc16_table[(crc >> 8) ^ packet.packet_data[j]]);
				}
				// check CRC
				if (packet.crc == crc)
				{
					uint8_t byte[4] = { 0 };
					this->dataLocker.lock();
					for (uint8_t k = 0; k < 4; k++)
					{
						byte[k] = packet.packet_data[k];
					}
					imuSfogData.acc_x = *(float *)byte;
					for (uint8_t k = 0; k < 4; k++)
					{
						byte[k] = packet.packet_data[k + 4];
					}
					imuSfogData.acc_y = *(float *)byte;
					for (uint8_t k = 0; k < 4; k++)
					{
						byte[k] = packet.packet_data[k + 8];
					}
					imuSfogData.acc_z = *(float *)byte;
					for (uint8_t k = 0; k < 4; k++)
					{
						byte[k] = packet.packet_data[k + 12];
					}
					imuSfogData.ang_vel_x = (*(float *)byte) * 180 / PI2;					
					for (uint8_t k = 0; k < 4; k++)
					{
						byte[k] = packet.packet_data[k + 16];
					}
					imuSfogData.ang_vel_y = (*(float *)byte) * 180 / PI2;
					for (uint8_t k = 0; k < 4; k++)
					{
						byte[k] = packet.packet_data[k + 20];
					}
					imuSfogData.ang_vel_z = (*(float *)byte) * 180 / PI2;

					// this->imuSfogData.angle_z_cacul = theta;
					// this->imuSfogData.angle_z_filter = alpha_filter;

					this->dataLocker.unlock();
					int get_buff_available = 0;
					get_buff_available = this->uartPort.getBuffAvailable();
					if (get_buff_available > 1000)
					{
						this->logger->info("check buffer (>1000): {0:d}", get_buff_available);
						this->uartPort.flushBuff();
					}
				}
			}
			else
			{
				continue;
			}
		}
		else
		{
			this->logger->info("check header: error");
			for (i = 0; i < 4; i++)
			{
				buff_read[i] = buff_read[i + 1];
			}	
		}    
	}
	return 0;
}

void SFogIMU::checkIMUStatus(void)
{
	this->time_check = 0;
	while (true)
	{
		this->time_check++;
		if (this->time_check > 500)
		{
			this->logger->info("check imu: not found");
			this->time_check = 0;
			this->imuSfogData.imu_state = 0;
		}
		usleep(1000);
	}
}

ImuSfogData_s SFogIMU::getData(void)
{
	ImuSfogData_s getData;
	this->dataLocker.lock();
	getData = this->imuSfogData;
	this->dataLocker.unlock();
	return getData;
}

void SFogIMU::getDataPRU(void)
{
	ImuSfogData_s getData;
	uint8_t buff_data[255] = { 0 };
	uint8_t buff_request[1] = { 0x53 };
	uint8_t len_data = 0;
	timeval start_time, end_time;
	
	
	if (this->pru_state == true)
	{
		// request to pru
		
		if (this->pruImu.sendData(buff_request, 1) == true)
		{
			
			len_data = this->pruImu.readData(buff_data);
			if (len_data > 0)
			{
				uint8_t byte_convert[4];
				for (uint8_t i = 0; i < 4; i++)
				{
					byte_convert[i] = buff_data[i];
				}
				getData.ang_vel_x = *(float*)byte_convert;
				
				for (uint8_t i = 0; i < 4; i++)
				{
					byte_convert[i] = buff_data[i+4];
				}
				getData.ang_vel_y = *(float*)byte_convert;
				
				for (uint8_t i = 0; i < 4; i++)
				{
					byte_convert[i] = buff_data[i+8];
				}
				getData.ang_vel_z = *(float*)byte_convert;
				
				for (uint8_t i = 0; i < 4; i++)
				{
					byte_convert[i] = buff_data[i+12];
				}
				getData.roll_data = *(float*)byte_convert;
				
				for (uint8_t i = 0; i < 4; i++)
				{
					byte_convert[i] = buff_data[i + 16];
				}
				getData.pitch_data = *(float*)byte_convert;
				
				for (uint8_t i = 0; i < 4; i++)
				{
					byte_convert[i] = buff_data[i + 20];
				}
				getData.heading_data = *(float*)byte_convert;				
				
				this->dataLocker.lock();
				this->imuSfogData = getData;
				this->dataLocker.unlock();		
				
				this->time_check = 0;
				this->imuSfogData.imu_state = 1;
				
				gettimeofday(&end_time, NULL);
				long check_time = 0;
				check_time = end_time.tv_usec - start_time.tv_usec;
				if (check_time > 1000)
				{
					//std::cout << "debug : " << check_time << std::endl;
				}
				gettimeofday(&start_time, NULL);
			}
		}
	}	

	//std::cout << "debug:  " << getData.ang_vel_x << "   " << getData.ang_vel_y << "   " << getData.ang_vel_z << "   " << getData.pitch_data << "   " << getData.roll_data << "  " << getData.heading_data << std::endl;

}

