/**
******************************************************************************
* @file           : tracking.cpp
* @brief          : source file of Tracking interface
******************************************************************************
******************************************************************************
*/

#include "tracking_module/tracking.h"
#include "thread"
#include "comm/uart_interface.h"
#include "sensor/imu_Sfog.h"
#include <sstream>

TrackingInterface::TrackingInterface()
{
	this->dataTracking.connect_status = 0;
	this->dataTrackingEth.connect_status = 0;
	if (!this->newUart.init(UART7, 115200))
	{
		std::cout << "init uart for tracking interface: eror" << std::endl;
	}
	//this->newCan.init(CAN0_INTERFACE);
}

TrackingInterface::~TrackingInterface()
{
}

void TrackingInterface::recvData(void)
{
	uint8_t buff[8] = { 0 };
	uint16_t dev_id = 0;
	bool check_value = 1;
	std::cout << "start receive tracking" << std::endl;
	std::thread checkTrackingThread(&TrackingInterface::checkTracking, this);
	while (true)
	{
		if (this->newCan.recv(buff, &dev_id))
		{
			// check ID
			if (dev_id == TRACKING_ID)
			{
				this->dataTracking.flag_check = 1;
				this->dataTracking.connect_status = 1;
				check_value = 1;
				for (uint8_t i = 0; i < 6; i++)
				{
					if (buff[i + 1] == '*')
					{
						check_value = 0;
					}
				}
				if (check_value)
				{
					while (this->flag_get_data)
					{
						usleep(1);
					}
					this->flag_get_data = 1;
					this->dataTracking.ex_axis = (((uint8_t)buff[1] - 48) * 100 + ((uint8_t)buff[2] - 48) * 10 + (uint8_t)buff[3] - 48);
					if (buff[0] == 45)
					{
						this->dataTracking.ex_axis = -this->dataTracking.ex_axis;
					}
					this->dataTracking.ey_axis = (((uint8_t)buff[5] - 48) * 100 + ((uint8_t)buff[6] - 48) * 10 + (uint8_t)buff[7] - 48);
					if (buff[4] == 45)
					{
						this->dataTracking.ey_axis = -this->dataTracking.ey_axis;
					}									
					this->flag_get_data = 0;
					//std::cout << "debug:  " << this->dataTracking.ex_axis << "    " << this->dataTracking.ey_axis << std::endl;
				}
				else
				{
					while (this->flag_get_data)
					{
						usleep(1);
					}
					this->flag_get_data = 1;
					this->dataTracking.ex_axis = 0;
					this->dataTracking.ey_axis = 0;
					this->flag_get_data = 0;
				}
			}
		}
		usleep(1);
	}
}

void TrackingInterface::recvEthernet(void)
{
	Socket newSocket;
	TcpConnection newServer;
	char buff[100] = { 0 };
	bool check_value = 1;
	
	newServer = newSocket.init();
	newServer.init(7001);
	std::cout << "start receive tracking with Ethernet" << std::endl;
	std::thread checkTrackingThread(&TrackingInterface::checkTracking, this);
	while (true)
	{
		this->newClient = newServer.accept();
		while (true)
		{
			if (this->newClient.receive(buff, 100) == -1)
			{
				break;
			}
			this->dataTrackingEth.flag_check = 1;
			this->dataTrackingEth.connect_status = 1;
			check_value = 1;
			for (uint8_t i = 0; i < 6; i++)
			{
				if (buff[i + 1] == '*')
				{
					check_value = 0;
				}
			}
			if (check_value)
			{
				while (this->flag_get_data_eth)
				{
					usleep(1);
				}
				this->flag_get_data_eth = 1;
				this->dataTrackingEth.ex_axis = (((uint8_t)buff[2] - 48) * 100 + ((uint8_t)buff[3] - 48) * 10 + (uint8_t)buff[4] - 48);
				if (buff[1] == 45)
				{
					this->dataTrackingEth.ex_axis = -this->dataTrackingEth.ex_axis;
				}
				this->dataTrackingEth.ey_axis = (((uint8_t)buff[6] - 48) * 100 + ((uint8_t)buff[7] - 48) * 10 + (uint8_t)buff[8] - 48);
				if (buff[5] == 45)
				{
					this->dataTrackingEth.ey_axis = -this->dataTrackingEth.ey_axis;
				}									
				this->flag_get_data_eth = 0;
				this->dataTrackingEth.connect_status = 1;
				std::cout << "debug eth:  " << this->dataTrackingEth.ex_axis << "    " << this->dataTrackingEth.ey_axis << std::endl;
				
			}
			else
			{
				this->dataTrackingEth.connect_status = 0;
				while (this->flag_get_data_eth)
				{
					usleep(1);
				}
				this->flag_get_data_eth = 1;
				this->dataTrackingEth.ex_axis = 0;
				this->dataTrackingEth.ey_axis = 0;
				this->flag_get_data_eth = 0;
			}
		}
	}
}

void TrackingInterface::recvUart(void)
{
	
	char buff[14] = { 0 };
	bool check_value = 1;

	std::cout << "start receive tracking with uart" << std::endl;
	std::thread checkTrackingThread(&TrackingInterface::checkTracking, this);
	while (true)
	{
		buff[0] = this->newUart.read();
		//std::cout << "debug uart:  " << buff[0] << std::endl;
		if ((char)buff[0] == 'S') // start_byte
		{
			int get_buff_available = 0;
			while (get_buff_available < 12)
			{
				get_buff_available = this->newUart.getBuffAvailable();
				usleep(10);
			}
			this->newUart.read((uint8_t *)(&buff[1]), 12);
			// calculator crc
			uint16_t crc_cacul = 0;
			for (uint8_t i = 0; i < 8; i++)
			{
				crc_cacul = (crc_cacul >> 8) ^ crc16_table_traking[(crc_cacul ^ buff[i + 1]) & 0x00ff];
			}
			crc_cacul = ~crc_cacul;
			// check crc
			std::string get_crc = "";
			int get_crc_value = 0;
			for (uint8_t i = 0; i < 4; i++)
			{
				get_crc += buff[9 + i];
			}
			
			std::stringstream str;
			str << get_crc;
			str >> std::hex >> get_crc_value;
			
			if (get_crc_value == crc_cacul)
			{
				this->dataTracking.flag_check = 1;
				this->dataTracking.connect_status = 1;
				check_value = 1;
				for (uint8_t i = 0; i < 8; i++)
				{
					if (buff[i + 1] == '*')
					{
						check_value = 0;
					}
				}
				if (check_value)
				{
					while (this->flag_get_data)
					{
						usleep(1);
					}
					this->flag_get_data = 1;
					this->dataTracking.ex_axis = (((uint8_t)buff[2] - 48) * 100 + ((uint8_t)buff[3] - 48) * 10 + (uint8_t)buff[4] - 48);
					if (buff[1] == 45)
					{
						this->dataTracking.ex_axis = -this->dataTracking.ex_axis;
					}
					this->dataTracking.ey_axis = (((uint8_t)buff[6] - 48) * 100 + ((uint8_t)buff[7] - 48) * 10 + (uint8_t)buff[8] - 48);
					if (buff[5] == 45)
					{
						this->dataTracking.ey_axis = -this->dataTracking.ey_axis;
					}									
					this->flag_get_data = 0;
					this->dataTracking.connect_status = 1;
					//std::cout << "debug uart:  " << this->dataTracking.ex_axis << "    " << this->dataTracking.ey_axis << "    " << get_buff_available << std::endl;
					std::cout << "tracking data:" << dataTracking.ex_axis << "  " << dataTracking.ey_axis << std::endl;
				
				}
				else
				{
					this->dataTracking.connect_status = 0;
					while (this->flag_get_data)
					{
						usleep(1);
					}
					this->flag_get_data = 1;
					this->dataTracking.ex_axis = 0;
					this->dataTracking.ey_axis = 0;
					this->flag_get_data = 0;
				}
			}
		}
	         
	}
}

bool TrackingInterface::sendUart(uint8_t *buff, uint16_t len){
	if (this->newUart.send(buff, len)){
		return true;
	}
	else{
		return false;
	}
}

void TrackingInterface::checkTracking(void)
{
	uint16_t timeout = 0;
	std::cout << "start check tracking" << std::endl;
	while (true)
	{
		if (this->dataTracking.flag_check)
		{
			timeout = 0;
			this->dataTracking.flag_check = 0;
		}
		timeout++;
		usleep(10000);
		if (timeout > 50)
		{
			timeout = 0;
			this->dataTracking.connect_status = 0;
			this->dataTracking.ex_axis = 0;
			this->dataTracking.ey_axis = 0;
			//break;
		}
		
	}
}

DataTracking_s TrackingInterface::getDataTracking(void)
{
	DataTracking_s getData;
	while (this->flag_get_data)
	{
		usleep(1);
	}
	this->flag_get_data = 1;
	getData = this->dataTracking;
	this->flag_get_data = 0;
	return getData;
}

DataTracking_s TrackingInterface::getDataTrackingEth(void)
{
	DataTracking_s getData;
	while (this->flag_get_data_eth)
	{
		usleep(1);
	}
	this->flag_get_data_eth = 1;
	getData = this->dataTrackingEth;
	this->flag_get_data_eth = 0;
	return getData;
}


void TrackingInterface::sendDataEthernet(std::string data)
{
	if (this->newClient.stt_conn == 1)
	{
		this->newClient.send(data);
	}
	else
	{
		std::cout << "send data over Ethernet: false" << std::endl;
	}
}