/**
******************************************************************************
* @file           : uart_interface.cpp
* @brief          : source file of uart interface
******************************************************************************
******************************************************************************
*/
#include "comm/uart_interface.h"
#include <iostream>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/serial.h>
#include <sys/select.h>


int UartInterface::init(std::string uart_name, uint32_t uart_baurate)
{	
	struct termios options;
	uint8_t set_baud_divisor = 0;
	
	this->uartName = uart_name;
	this->baudrate = uart_baurate;
	
	this->new_uart = open(this->uartName.c_str(), O_RDWR, O_NDELAY);
	if (this->new_uart < 0)
	{
		std::cout << "(UART) error on opening serial port" << std::endl;
		return 0;
	}
	else
	{
		fcntl(this->new_uart, F_SETFL, 0);

		tcgetattr(this->new_uart, &options);
		switch (this->baudrate)
		{
		case 115200:
			options.c_cflag = B115200 | CS8 | CLOCAL | CREAD;
			break;
		case 9600:
			options.c_cflag = B9600 | CS8 | CLOCAL | CREAD;
			break;
		case 38400:
			options.c_cflag = B38400 | CS8 | CLOCAL | CREAD;
			break;
		case 460800:
			options.c_cflag = B460800 | CS8 | CLOCAL | CREAD;
			break;
		case 921600:
			options.c_cflag = B921600 | CS8 | CLOCAL | CREAD;
			break;		
		default:
			set_baud_divisor = 1;
			options.c_cflag = B38400 | CS8 | CLOCAL | CREAD;
			break;
		}
		options.c_iflag = IGNPAR;
		options.c_oflag = 0;
		options.c_lflag = 0;
		options.c_cc[VTIME] = 1;
		options.c_cc[VMIN] = 1;
		//tcflush(new_uart, TCIFLUSH);
		
		tcsetattr(this->new_uart, TCSANOW, &options);
		
//		FD_ZERO(&set);
//		FD_SET(new_uart, &set);
//		timeout.tv_sec = 0;
//		timeout.tv_usec = 1000;
//		rv = select(new_uart + 1, &set, NULL, NULL, &timeout);
//		if (rv > 0)
//		{
//			std::cout << "select success" << std::endl;
//		}
		
		
		if (set_baud_divisor)
		{
			if (setBaudDivisor(this->baudrate))
			{
				std::cout << "set baud rate divisor: success" <<std::endl;
			}
		}
		tcflush(this->new_uart, TCIOFLUSH); // flush buffer
		return 1;
	}
}

void UartInterface::flushBuff(void)
{
	tcflush(this->new_uart, TCIOFLUSH);
}

int UartInterface::setBaudDivisor(int baudRate)
{
	// default baud was not found, so try to set a custom divisor
	struct serial_struct ss;
	if (ioctl(this->new_uart, TIOCGSERIAL, &ss) != 0) 
	{
		std::cout << "eror set baud rate uart divisor" << std::endl;
		return 0;
	}

	ss.flags = (ss.flags & ~ASYNC_SPD_MASK) | ASYNC_SPD_CUST;
	ss.custom_divisor = (ss.baud_base + (baudRate / 2)) / baudRate;
	ss.custom_divisor = 4;
	int closest_speed = ss.baud_base / ss.custom_divisor;

	if (closest_speed < baudRate * 98 / 100 || closest_speed > baudRate * 102 / 100) 
	{
		std::cout << "eror set baud rate uart divisor  " << ss.baud_base << std::endl;
		return 0;
	}

	if (ioctl(this->new_uart, TIOCSSERIAL, &ss) < 0) 
	{
		std::cout << "eror set baud rate uart divisor" << std::endl;
		return 0;
	}
	return 1;
}

int UartInterface::close(void)
{
	if (::close(this->new_uart) < 0)
	{
		std::cout << "(UART) erro close uart port" << std::endl;
		return 0;
	}
	return 1;
}

int UartInterface::send(uint8_t *buff, int len)
{
	if (write(this->new_uart, buff, len) < 0)
	{
		std::cout << "(UART) error on send data" << std::endl;
		return 0;
	}
	return 1;
}

int UartInterface::read(void)
{
	int buff_len = 0;
	int data = 0;
	while (true)
	{
		buff_len = ::read(this->new_uart, &data, 1);
		if (buff_len > 0)
		{
			return data;
		}
		//ioctl(new_uart, FIONREAD, &bytes);
		else if (buff_len <= 0)
		{
			std::cout << "(uart) error on receive data" << std::endl;
			return -1;
		}
	}
	return buff_len;
}

int UartInterface::read(uint8_t *buff, int len)
{
	int buff_len = 0;
	while (true)
	{
		buff_len =::read(this->new_uart, buff, len);
		if (buff_len > 0)
		{
			return 1;
		}
		//ioctl(new_uart, FIONREAD, &bytes);
		else if (buff_len <= 0)
		{
			std::cout << "(uart) error on receive data" << std::endl;
			return -1;
		}
	}
	return buff_len;
}

//int UartInterface::readln(uint8_t *buff, int max_len)
//{
//	int data = 0;
//	int len = 0;
//	while (true)
//	{
//		data = this->read();
//		if (data > 0)
//		{
//			*buff = data;
//			len++;
//			if ((*buff == 0x0A)&&(*(buff-1) == 0x0D))
//			{
//				return len-2;
//			}
//			if (len >= max_len)
//			{
//				return -1;
//			}
//			buff++;
//		}
//		else
//		{
//			return -1;
//		}
//	}
//}

int UartInterface::getBuffAvailable(void)
{
	int num = 0;
	ioctl(this->new_uart, FIONREAD, &num);
	return num;
}

UartInterface::UartInterface()
{
}

UartInterface::~UartInterface()
{
	this->close();
}