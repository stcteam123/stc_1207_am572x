/**
******************************************************************************
* @file           : spi_interface.cpp
* @brief          : source file of spi interface
******************************************************************************
******************************************************************************
*/

#include "comm/spi_interface.h"
#include <linux/spi/spidev.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>


int SpiInterface::init(std::string spi_name)
{
	new_spi = open(spi_name.c_str(), O_RDWR);
	if (new_spi < 0)
	{
		std::cout << "error open spi device" << std::endl;
		return 0;
	}
	else
	{
		std::cout << "open spi device: OK" << std::endl;
		if (ioctl(new_spi, SPI_IOC_WR_MODE, &spi_mode) < 0)
		{
			std::cout << "error set SPIMode (WR)" << std::endl;
			return 0;
		}
		if (ioctl(new_spi, SPI_IOC_RD_MODE, &spi_mode) < 0)
		{
			std::cout << "error set SPIMode (RD)" << std::endl;
			return 0;
		}
		if (ioctl(new_spi, SPI_IOC_WR_BITS_PER_WORD, &spi_bit_num) < 0)
		{
			std::cout << "error set spi bit number (WR)" << std::endl;
			return 0;
		}
		if (ioctl(new_spi, SPI_IOC_RD_BITS_PER_WORD, &spi_bit_num) < 0)
		{
			std::cout << "error set spi bit number (RD)" << std::endl;
			return 0;
		}
		if (ioctl(new_spi, SPI_IOC_WR_MAX_SPEED_HZ, &spi_speed) < 0)
		{
			std::cout << "error set SPI speed (WR)" << std::endl;
			return 0;
		}
		if (ioctl(new_spi, SPI_IOC_RD_MAX_SPEED_HZ, &spi_speed) < 0)
		{
			std::cout << "error set SPI speed (RD)" << std::endl;
			return 0;
		}
		
		std::cout << "SPI Init: OK" << std::endl;
		return 1;
	}
}

int SpiInterface::close(void)
{
	if (::close(new_spi) < 0)
	{
		std::cout << "(SPI) error on Close" << std::endl;
		return 0;
	}
	return 1;
}


int SpiInterface::sendRecv(uint8_t *txrx_buff, uint16_t len)
{
	struct spi_ioc_transfer spi_transfer[len];
	int result;
	
	for (int i = 0; i < len; i++)
	{
		memset(&spi_transfer[i], 0, sizeof(spi_transfer[i]));
		spi_transfer[i].tx_buf = (unsigned long)(txrx_buff + i);
		spi_transfer[i].rx_buf = (unsigned long)(txrx_buff + i);
		spi_transfer[i].len = sizeof(*(txrx_buff + i));
		spi_transfer[i].delay_usecs = 0;
		spi_transfer[i].speed_hz = spi_speed;
		spi_transfer[i].bits_per_word = spi_bit_num;
		spi_transfer[i].cs_change = 0;
	}
	
	result = ioctl(new_spi, SPI_IOC_MESSAGE(len), &spi_transfer);
	if (result < 0)
	{
		std::cout << "(SPI) error transmitting data: ioctl mode" << std::endl;
		return 0;
	}
	return 1;
}

SpiInterface::SpiInterface(std::string spiName)
{
	spi_mode = SPI_MODE_3;
	spi_bit_num = 16;
	spi_speed = 1000000;   // 1 MHz
	this->init(spiName);
}

SpiInterface::SpiInterface(std::string spiName, char spiMode, char spiBitNum, uint32_t spiSpeed)
{
	spi_mode = spiMode;
	spi_bit_num = spiBitNum;
	spi_speed = spiSpeed;
	this->init(spiName);
}

SpiInterface::~SpiInterface()
{
	this->close();
}