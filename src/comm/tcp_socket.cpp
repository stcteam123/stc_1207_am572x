/**
******************************************************************************
* @file           : socket.cpp
* @brief          : Source file of socket
******************************************************************************
******************************************************************************
*/

#include "comm/tcp_socket.h"


TcpConnection Socket::init(void)
{
	TcpConnection obj;
	int a = 1;
	obj.new_socket = socket(AF_INET, SOCK_STREAM, 0);
	setsockopt(obj.new_socket, SOL_SOCKET, SO_REUSEADDR, &a, sizeof(int));
	if (obj.new_socket < 0)
	{
		std::cout << "Creat new socket: Error" << std::endl;
	}
	else
	{
		std::cout << "Creat new socket: OK" << std::endl;
	}
	return obj;
}


int Socket::close()
{
	//close(new_socket);
	return 1;
}

TcpConnection::TcpConnection()
{
}

TcpConnection::~TcpConnection()
{
	//::close(this->new_socket);
}

int TcpConnection::connect(std::string host, uint16_t port)
{
	struct sockaddr_in serveraddr;
	int conn_result;

	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = inet_addr(host.c_str());
	serveraddr.sin_port = htons(port);

	for (uint8_t i = 0; i < 3; i++)
	{
		conn_result =::connect(new_socket, (struct sockaddr *)&serveraddr, sizeof(serveraddr));
		if (conn_result < 0)
		{
			std::cout << "Connect to server: Error" << std::endl;
			std::cout << "Try to connect..." << i << std::endl;
		}
		else
		{
			std::cout << "Connect to server: OK" << std::endl;
			stt_conn = 1;
			return 1;
		}
	}
	std::cout << "Cannot connect to server...Break!" << std::endl;
	stt_conn = 0;
	return 0;
}

void TcpConnection::close(void)
{
	::close(new_socket);
}

int TcpConnection::init(uint16_t port)
{
	struct sockaddr_in serveraddr;

	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = INADDR_ANY;
	serveraddr.sin_port = htons(port);
	if (bind(new_socket, (struct sockaddr *)&serveraddr, sizeof(serveraddr)) < 0)
	{
		std::cout << "error on Binding" << std::endl;
		return 0;
	}
	else
	{
		listen(new_socket, 5);
		std::cout << "waiting to client connect.." << std::endl;
		return 1;
	}
}

TcpConnection TcpConnection::accept(void)
{
	TcpConnection new_conn;
	socklen_t clientlen;
	struct sockaddr_in clientaddr;

	clientlen = sizeof(clientaddr);
	new_conn.new_socket =::accept(new_socket, (struct sockaddr *)&clientaddr, &clientlen);
	if (new_conn.new_socket < 0)
	{
		std::cout << "error on accept" << std::endl;
	}
	else
	{
		new_conn.stt_conn = 1;
		std::cout << "new client connected" << std::endl;
	}
	return new_conn;
}

int TcpConnection::send(std::string data)
{
	if (!stt_conn)
	{
		std::cout << "Cannot send data" << std::endl;
		return 0;
	}
	else
	{
		if (::send(new_socket, data.c_str(), data.length(), 0) < 0)
		{
			std::cout << "Send data: Error" << std::endl;
			stt_conn = 0;
			return 0;
		}
		else
		{
			return 1;
		}
	}
}

int TcpConnection::send(char *buff, int len)
{
	if (!stt_conn)
	{
		std::cout << "Cannot send data" << std::endl;
		return 0;
	}
	else
	{
		if (::send(new_socket, buff, len, 0) < 0)
		{
			std::cout << "Send data: Error" << std::endl;
			return 0;
		}
		else
		{
			std::cout << "Send data: OK" << std::endl;
			return 1;
		}
	}
}

int TcpConnection::receive(char *buff, int len)
{
	int result;
	while (true)
	{
		result = read(new_socket, buff, len);
		if (result < 0)
		{
			std::cout << "error on receive data" << std::endl;
			stt_conn = 0;
			return 0;
		}
		else if (result == 0)
		{
			std::cout << "connecttion closed." << std::endl;
			stt_conn = 0;
			return -1;
		}
		//std::cout << "Received data: " << buff << "len" << result << std::endl;
		return result;
	}
	return 1;
}


int TcpConnection::keepalive(void)
{
	int get_value;
	socklen_t get_value_len;

	get_value = 1;
	get_value_len = sizeof(get_value);
	if (setsockopt(new_socket, SOL_SOCKET, SO_KEEPALIVE, &get_value, get_value_len) < 0)
	{
		std::cout << "error on setsockopt" << std::endl;
	}
	else
	{
		std::cout << "SO_KEEPALIVE set on socket" << std::endl;
		get_value = 10;
		get_value_len = sizeof(get_value);
		if (setsockopt(new_socket, IPPROTO_TCP, TCP_KEEPIDLE, &get_value, get_value_len) < 0)
		{
			std::cout << "error on set keepalive time" << std::endl;
		}
		else
		{
			std::cout << "set keepalive time : ok" << std::endl;
		}

	}
	return 0;
}


Socket::Socket()
{
}

Socket::~Socket()
{
}

