/**
******************************************************************************
* @file           : pru_comm.cpp
* @brief          : source file of pru-icss communication
* @Author		  : Tuan-NT
******************************************************************************
******************************************************************************
*/
#include "comm/pru_comm.h"
#include <unistd.h>
#include <fcntl.h>

PruComm::PruComm()
{
}

PruComm::~PruComm()
{
}

/**
 * @brief	: init Pru Chanel
 * @Param	: Chanel
 * @Return	: true/false
*/
bool PruComm::init(std::string chanel)
{
	this->pru_fd = open(chanel.c_str(), O_RDWR);
	if (this->pru_fd < 0)
	{
		return false;
	}
	return true;
}


/**
 * @brief	: send Data
 * @Param	: pointer of buff
 * @Return	: true/false
*/
bool PruComm::sendData(uint8_t *buff, uint8_t len)
{
	int result = 0;
	result = write(this->pru_fd, buff, len);
	if (result > 0)
	{
		return true;
	}
	return false;
}


/**
 * @brief	: read Data
 * @Param	: pointer of buff
 * @Return	: true/false
*/
int PruComm::readData(uint8_t *buff)
{
	int len = 0;
	uint8_t buff_recv[1024] = { 0 };
	len = read(this->pru_fd, buff_recv, 1024);
	if (len > 0)
	{
		for (uint16_t i = 0; i < len; i++)
		{
			buff[i] = buff_recv[i];
		}
		return len;
	}
	return -1;
}