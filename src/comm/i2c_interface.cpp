/**
******************************************************************************
* @file           : i2c_interface.cpp
* @brief          : source file of i2c interface
******************************************************************************
******************************************************************************
*/

#include "comm/i2c_interface.h"
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/i2c-dev.h>
#include <linux/i2c.h>

int I2cInterface::init(std::string i2c_Name)
{
	i2c_fd = open(i2c_Name.c_str(), O_RDWR);
	if (i2c_fd < 0)
	{
		std::cout << "(I2C) error on open i2c port" << std::endl;
		return 0;
	}
	else
	{
		return 1;
	}
}



int I2cInterface::close(void)
{
	if (::close(i2c_fd) < 0)
	{
		std::cout << "(I2C) error on close i2c port" << std::endl;
		return 0;
	}
	return 1;
}


int I2cInterface::writeReg(uint8_t reg_addr, uint8_t value)
{
	uint8_t buff[2];
	struct i2c_rdwr_ioctl_data packet;
	struct i2c_msg message;
	
	memset(&packet, 0, sizeof(packet));
	memset(&message, 0, sizeof(message));
	
	buff[0] = reg_addr;
	buff[1] = value;
	message.addr = dev_addr;
	message.flags = 0;
	message.len = sizeof(buff);
	message.buf = buff;
	packet.msgs = &message;
	packet.nmsgs = 1;
	
	if (ioctl(i2c_fd, I2C_RDWR, &packet) < 0)
	{
		std::cout << "(I2C) error on write device register" << std::endl;
		return 0;
	}
	return 1;
}

int I2cInterface::readReg(uint8_t reg_addr, int *value)
{
	struct i2c_rdwr_ioctl_data packet;
	struct i2c_msg message[2];
	
	//memset(&packet, 0, sizeof(packet));
	//memset(&message, 0, sizeof(message));
	message[0].addr = dev_addr;
	message[0].flags = 0;
	message[0].len = sizeof(reg_addr);
	message[0].buf = &reg_addr;
	
	message[1].addr = dev_addr;
	message[1].flags =	I2C_M_RD;
	message[1].len = 1;
	message[1].buf = (uint8_t *)value;
	
	packet.msgs = message;
	packet.nmsgs = 2;
	
	if (ioctl(i2c_fd, I2C_RDWR, &packet) < 0)
	{
		std::cout << "(I2C) error on read device register" << std::endl;
		return 0;
	}
	return 1;
}

I2cInterface::I2cInterface()
{
	dev_addr = 0x0D;
	this->init(I2C0);
}

I2cInterface::I2cInterface(std::string i2cName, uint8_t devAddr)
{
	dev_addr = devAddr;
	this->init(i2cName);
}

I2cInterface::~I2cInterface()
{
	this->close();
}