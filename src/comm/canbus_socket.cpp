/**
******************************************************************************
* @file           : canbus_socket.h
* @brief          : header file of Canbus socket
******************************************************************************
******************************************************************************
*/

#include "comm/canbus_socket.h"

CanSocket::CanSocket()
{
	this->error_num = 0;
}

CanSocket::~CanSocket()
{
}

int CanSocket::init(std::string can_name)
{
	struct sockaddr_can addr;
	struct ifreq ifre;

	memset(&addr, 0, sizeof(addr));
	memset(&ifre, 0, sizeof(ifre));

	cansock = socket(PF_CAN, SOCK_RAW, CAN_RAW);
	if (cansock < 0)
	{
		std::cout << "error creat can socket" << std::endl;
		return 0;
	}
	else
	{
		strcpy(ifre.ifr_name, can_name.c_str());
		ioctl(cansock, SIOCGIFINDEX, &ifre);
		addr.can_family = AF_CAN;
		addr.can_ifindex = ifre.ifr_ifindex;

		if (bind(cansock, (struct sockaddr *)&addr, sizeof(addr)) < 0)
		{
			std::cout << "error bind can socket" << std::endl;
			return 0;
		}
		else
		{
			fcntl(cansock, F_SETFL, O_NONBLOCK);
			//std::cout << "init can socket: " << cansock << "OK" << std::endl;			
			return 1;
		}
	}
}

int CanSocket::send(uint16_t id, uint8_t *data, uint8_t len)
{
	struct can_frame frame;

	memset(&frame, 0, sizeof(frame));
	if (len > 8)
	{
		std::cout << "overload data" << std::endl;
		return 0;
	}
	// create frame
	frame.can_id = id;
	for (uint8_t i = 0; i < len; i++)
	{
		frame.data[i] = data[i];
	}
	frame.can_dlc = len;

	if (::send(cansock, &frame, sizeof(frame), 0) < 0)
	{
		this->error_num++;
		std::cout << "(CanSocket) error on send data" << std::endl;
		return 0;
	}
	else
	{
		//std::cout << "(CanSocket) send data: OK" << std::endl;
		return 1;
	}
}

int CanSocket::recv(uint8_t *buff, uint16_t *id)
{
	int result;
	struct can_frame frame;
	
	memset(&frame, 0, sizeof(frame));
	
	frame.can_dlc = 8;

	while (true)
	{
		result = read(cansock, &frame, sizeof(frame));
		if (result < 0)
		{
			//std::cout << "(CanSocket)error in receive data" << std::endl;
			return 0;
		}
		else
		{
			for (uint8_t i = 0; i < 8; i++)
			{
				buff[i] = frame.data[i];
			}
			*id = frame.can_id;
			//std::cout << "data received: " << (char *)frame.data << std::endl;
			return 1;
		}
	}
	return 0;
}