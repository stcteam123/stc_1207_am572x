/**
******************************************************************************
* @file           : fm_update.c
* @brief          : source file of update firmware over CanBus
******************************************************************************
******************************************************************************
*/

#include "ui_pc/com_interface.h"
#include "comm/canbus_socket.h"
#include <iostream>
#include <fstream>
#include <iostream>
#include <thread>
#include "json_lib/json_lib.h"



PCInterface::PCInterface()
{
	this->logger = spdlog::basic_logger_mt("pc_interface_logger", "/home/root/app/logMonitor/pc_logger_interface.txt");
	spdlog::flush_on(spdlog::level::info);
}

PCInterface::~PCInterface()
{
}

void PCInterface::run(TorqueController *controller)
{
	this->logger->info("PC Thread startting...");
	spdlog::info("PC Thread startting...");
	if (this->init())
	{
		while (true)
		{
			TcpConnection newClient = TcpServer.accept();
			PCService newService(&newClient);
			newService.run(controller);
		}
	}
	// connect to PC
}

int PCInterface::init(void)
{
	TcpServer = newSock.init();
	//TcpServer.keepalive();
	if (TcpServer.init(7001))
	{
		return 1;
	}
	else
	{
		return 0;
	}
}


PCService::PCService(TcpConnection *p_client)
{
	this->newClient = *p_client;
	this->permission = APP_USER;
	this->timeValue = { 0 };
	this->logger = spdlog::basic_logger_mt("pc_interface_logger", "/home/root/app/logMonitor/pc_logger_interface.txt");
}

PCService::~PCService()
{
}

void PCService::run(TorqueController *p_controller)
{
	JsonClass jsonObject;
	this->controller = p_controller;
	while (true)
	{
		std::string jsonPacket;
		jsonPacket = this->recvPacket();
		if (jsonPacket == "")
		{
			connecttion_status = 0;
			return;
		}
		this->logger->info(" json packet received: {0}", jsonPacket);
		spdlog::info("json packet received: {0}", jsonPacket);
		try
		{
			// decode json
			jsonObject.parse(jsonPacket);
			std::string getMsgType = jsonObject.getStringValue("MessageType");
			if (getMsgType == "AuthenAck")
			{
				std::string getUser = jsonObject.getStringValue("user");
				std::string getPass = jsonObject.getStringValue("pwd");
				if (!this->login(getUser, getPass))
				{
					spdlog::error("User Login: False");
					this->logger->error("User Login: Flase");
					this->connecttion_status = 0;
					this->sendLoginAck();
					this->newClient.close();
					break;
				}
				else
				{
					spdlog::error("User Login: success");
					this->logger->error("User Login: success");
					this->connecttion_status = 1;
					this->sendLoginAck();
					std::thread runServiceThread(&PCService::runService, this);
					while (true)
					{
						jsonPacket = this->recvPacket();
						if (jsonPacket == "")
						{
							this->connecttion_status = 0;
							runServiceThread.join();
							return;
						}
						this->logger->info(" json packet received: {0}", jsonPacket);
						spdlog::info("json packet received: {0}", jsonPacket);
						try
						{
							// decode json
							jsonObject.parse(jsonPacket);
							std::string getMsgType = jsonObject.getStringValue("MessageType");
					
							if (getMsgType == "StartTestSV")
							{
								std::string getValue = jsonObject.getStringValue("value");
								if (getValue == "0")
								{
									this->timeValue.test_servo_all = -1;
								}
								else
								{
									this->timeValue.test_servo_all = 500;
								}
							}
							else if (getMsgType == "StopTest")
							{
								this->timeValue.test_servo_all = 0;
								this->timeValue.test_joystick_all = 0;
							}
							else if (getMsgType == "GetMinPositionAZ")
							{
								std::string getValue = jsonObject.getStringValue("value");
								if (getValue == "0")
								{
									this->timeValue.get_min_position_AZ = 0;
								}
								else
								{
									this->timeValue.get_min_position_AZ = 500;
								}
							}
							else if (getMsgType == "GetMaxPositionAZ")
							{
								std::string getValue = jsonObject.getStringValue("value");
								if (getValue == "0")
								{
									this->timeValue.get_max_position_AZ = 0;
								}
								else
								{
									this->timeValue.get_max_position_AZ = 500;
								}
							}
							else if (getMsgType == "GetMinPositionEL")
							{
								std::string getValue = jsonObject.getStringValue("value");
								if (getValue == "0")
								{
									this->timeValue.get_min_position_EL = 0;
								}
								else
								{
									this->timeValue.get_min_position_EL = 500;
								}
							}
							else if (getMsgType == "GetMaxPositionEL")
							{
								std::string getValue = jsonObject.getStringValue("value");
								if (getValue == "0")
								{
									this->timeValue.get_max_position_EL = 0;
								}
								else
								{
									this->timeValue.get_max_position_EL = 500;
								}
							}
							else if (getMsgType == "GetIMUStatus")
							{
								std::string getValue = jsonObject.getStringValue("value");
								if (getValue == "0")
								{
									this->timeValue.get_imu_status = -1;
								}
								else
								{
									this->timeValue.get_imu_status = 500;
								}
							}
							else if (getMsgType == "StartTestJS")
							{
								std::string getValue = jsonObject.getStringValue("value");
								if (getValue == "0")
								{
									this->timeValue.test_joystick_all = -1;
								}
								else
								{
									this->timeValue.test_joystick_all = 500;
								}
							}
							else if (getMsgType == "GetJoystickStatus")
							{
								std::string getValue = jsonObject.getStringValue("value");
								if (getValue == "0")
								{
									this->timeValue.get_joystick_status = 0;
								}
								else
								{
									this->timeValue.get_joystick_status = 500;
								}
							}
							else if (getMsgType == "GetJoystickValue")
							{
								std::string getValue = jsonObject.getStringValue("value");
								if (getValue == "0")
								{
									this->timeValue.get_joystick_value = 0;
								}
								else
								{
									this->timeValue.get_joystick_value = 500;
								}
							}
		
						}
						catch (std ::string e)
						{
							std ::cout << "An exception occurred. Exception: " << e << '\n';
						}
					}
				}
			}
		}
		catch (std ::string e)
		{
			std ::cout << "An exception occurred. Exception: " << e << '\n';
		}
		catch (...)
		{
			std::cout << "exception: hard found" << std::endl;
		}

	}
}

void PCService::runService(void)
{
	while (connecttion_status)
	{
		// servo
		if ((this->timeValue.get_min_position_AZ >= 0)||(this->timeValue.test_servo_all > 0))
		{
			this->sendMinPositionAZ();
			if (this->timeValue.get_min_position_AZ == 0)
			{
				this->timeValue.get_min_position_AZ = -1;
			}
		}
		if ((this->timeValue.get_max_position_AZ >= 0) || (this->timeValue.test_servo_all > 0))
		{
			this->sendMaxPositionAZ();
			if (this->timeValue.get_max_position_AZ == 0)
			{
				this->timeValue.get_max_position_AZ = -1;
			}
		}
		if ((this->timeValue.get_min_position_EL >= 0) || (this->timeValue.test_servo_all > 0))
		{
			this->sendMinPositionEL();
			if (this->timeValue.get_min_position_EL == 0)
			{
				this->timeValue.get_min_position_EL = -1;
			}
		}
		if ((this->timeValue.get_max_position_EL >= 0) || (this->timeValue.test_servo_all > 0))
		{
			this->sendMaxPositionEL();
			if (this->timeValue.get_max_position_EL == 0)
			{
				this->timeValue.get_max_position_EL = -1;
			}
		}
		if ((this->timeValue.get_imu_status >= 0) || (this->timeValue.test_servo_all > 0))
		{
			this->sendIMUStatus();
			if (this->timeValue.get_imu_status == 0)
			{
				this->timeValue.get_imu_status = -1;
			}
		}
		
		// Joystick
		if ((this->timeValue.get_joystick_status >= 0) || (this->timeValue.test_joystick_all > 0))
		{
			this->sendJoystickStatus();
			if (this->timeValue.get_joystick_status == 0)
			{
				this->timeValue.get_joystick_status = -1;
			}
		}
		if ((this->timeValue.get_joystick_value >= 0) || (this->timeValue.test_joystick_all > 0))
		{
			this->sendJoystickValue();
			if (this->timeValue.get_joystick_value == 0)
			{
				this->timeValue.get_joystick_value = -1;
			}
		}
		sleep(1);
	}
}

std::string PCService::recvPacket(void)
{
	std::string data;
	char data_read[1024] = { 0 };
	if (this->newClient.receive(data_read, 1024) > 0)
	{
		data = data_read;
	}
	else
	{
		data = "";
	}
	return data;
}

void PCService::sendPacket(std::string json)
{
	this->newClient.send(json);
	this->newClient.send("*");
}

bool PCService::login(std::string userName, std::string pass)
{
	if ((userName == APP_USER_NAME)&&(pass == APP_USER_PASS))
	{
		this->permission = APP_USER;
		return 1;
	}
	else if ((userName == APP_ROOT_NAME)&&(pass == APP_ROOT_PASS))
	{
		this->permission = APP_ROOT;
		return 1;
	}
	return 0;
}


void PCService::sendLoginAck(void)
{
	Document newJsonPacket;
	StringBuffer json;
	Writer<StringBuffer> writer(json);
	std::string result_conn;
	// get Min Position AZ
	if (this->connecttion_status == 0)
	{
		result_conn = "false";
	}
	else
	{
		result_conn = "success";
	}
	
	writer.StartObject();
	writer.Key("MessageType");
	writer.String("AuthenAck");
	writer.Key("result");
	writer.String(result_conn.c_str());
	writer.EndObject();
	
	std::cout << json.GetString() << std::endl;
	this->sendPacket(json.GetString());
}

void PCService::sendMinPositionAZ(void)
{
	Document newJsonPacket;
	StringBuffer json;
	Writer<StringBuffer> writer(json);
	char data[20] = { 0 };
	
	// get Min Position AZ
	sprintf(data, "%.3f", this->controller->min_position_AZ);
	
	writer.StartObject();
	writer.Key("MessageType");
	writer.String("MinPositionAZ");
	writer.Key("value");
	writer.String(data);
	writer.EndObject();
	
	std::cout << json.GetString() << std::endl;
	this->sendPacket(json.GetString());
}

void PCService::sendMaxPositionAZ(void)
{
	Document newJsonPacket;
	StringBuffer json;
	Writer<StringBuffer> writer(json);
	char data[20] = { 0 };
	
	// get Min Position AZ
	sprintf(data, "%.3f", this->controller->max_position_AZ);
	
	writer.StartObject();
	writer.Key("MessageType");
	writer.String("MaxPositionAZ");
	writer.Key("value");
	writer.String(data);
	writer.EndObject();
	
	std::cout << json.GetString() << std::endl;
	this->sendPacket(json.GetString());	
}

void PCService::sendMinPositionEL(void)
{
	Document newJsonPacket;
	StringBuffer json;
	Writer<StringBuffer> writer(json);
	char data[20] = { 0 };
	
	// get Min Position AZ
	sprintf(data, "%.3f", this->controller->min_position_EL);
	
	writer.StartObject();
	writer.Key("MessageType");
	writer.String("MinPositionEL");
	writer.Key("value");
	writer.String(data);
	writer.EndObject();
	
	std::cout << json.GetString() << std::endl;
	this->sendPacket(json.GetString());
}

void PCService::sendMaxPositionEL(void)
{
	Document newJsonPacket;
	StringBuffer json;
	Writer<StringBuffer> writer(json);
	char data[20] = { 0 };
	
	// get Min Position AZ
	sprintf(data, "%.3f", this->controller->max_position_EL);
	
	writer.StartObject();
	writer.Key("MessageType");
	writer.String("MaxPositionEL");
	writer.Key("value");
	writer.String(data);
	writer.EndObject();
	
	std::cout << json.GetString() << std::endl;
	this->sendPacket(json.GetString());	
}


// Joystick
void PCService::sendJoystickStatus(void)
{
	Document newJsonPacket;
	StringBuffer json;
	Writer<StringBuffer> writer(json);
	std::string joystick_status;
	
	// get Joystick Status
	if (this->controller->myJoystick.getJoystickStatus() == 0)
	{
		joystick_status = "inactive";
	}
	else
	{
		joystick_status = "active";
	}
	writer.StartObject();
	writer.Key("MessageType");
	writer.String("JoystickStatus");
	writer.Key("value");
	writer.StartObject();
	writer.Key("joystick");
	writer.String(joystick_status.c_str());
	writer.Key("btnA");
	writer.String("inactive");
	writer.Key("btnB");
	writer.String("active");
	writer.Key("btnE");
	writer.String("inactive");
	writer.Key("btnF");
	writer.String("active");
	writer.Key("btnG");
	writer.String("inactive");
	writer.EndObject();
	writer.EndObject();
	
	std::cout << json.GetString() << std::endl;
	//this->sendPacket(json.GetString());	
}

void PCService::sendJoystickValue(void)
{
	Document newJsonPacket;
	StringBuffer json;
	Writer<StringBuffer> writer(json);
	char AZ_value[10] = { 0 };
	char EL_value[10] = { 0 };
	
	// get Joystick Value
	sprintf(AZ_value, "%d", this->controller->myJoystick.getXAxis());
	sprintf(EL_value, "%d", this->controller->myJoystick.getYAxis());
	writer.StartObject();
	writer.Key("MessageType");
	writer.String("JoystickValue");
	writer.Key("value");
	writer.StartObject();
	writer.Key("AZ");
	writer.String(AZ_value);
	writer.Key("EL");
	writer.String(EL_value);
	writer.EndObject();
	writer.EndObject();
	
	std::cout << json.GetString() << std::endl;
	this->sendPacket(json.GetString());	
}


// IMU
void PCService::sendIMUStatus(void)
{
	Document newJsonPacket;
	StringBuffer json;
	Writer<StringBuffer> writer(json);
	std::string imu_status;
	
	// get IMU Status
	if (controller->ImuSensor.getIMUStatus() == 0)
	{
		imu_status = "inactive";
	}
	else
	{
		imu_status = "active";
	}
	writer.StartObject();
	writer.Key("MessageType");
	writer.String("IMUStatus");
	writer.Key("value");
	writer.String(imu_status.c_str());
	writer.EndObject();
	
	std::cout << json.GetString() << std::endl;
	this->sendPacket(json.GetString());	
}