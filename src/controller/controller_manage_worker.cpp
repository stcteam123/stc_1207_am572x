/**
******************************************************************************
* @file           : controller_manage_worker.cpp
* @brief          : source file of Worker for Controller Management
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
/* Include ------------------------------------------------------------------*/
#include "controller/controller_manage_worker.h"
#include "sensor/sensor_manage_worker.h"
#include "motor_driver/driver_manage_worker.h"
#include "logger/sys_logger.h"
#include <sys/time.h>
#include <fstream>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

ControllerManageWorker controllerManageWorker;

void my_handler(int s){
    printf("Caught signal %d\n",s);

    controllerManageWorker.stopControllerAsSafety();
    exit(1); 

}

uint32_t getTimestamp(void){
	struct timeval tv;
	uint32_t get_time = 0;
	gettimeofday(&tv, NULL);
	
	get_time = tv.tv_sec;
	return get_time;
}


uint32_t getTimeMs(void){
	struct timeval tv;
	uint32_t get_time = 0;
	gettimeofday(&tv, NULL);
	
	get_time = tv.tv_usec;
	return get_time / 1000;
}


void ControllerManageWorker::inputParamController(void){
    while (1)
    {
        std::cin >> this->a_get;
    }
}

ControllerManageWorker::ControllerManageWorker(){
    this->a_get = 0.1;
    this->b_get = 0.1;
    this->c_get = 0;
    this->d_get = 0;

    this->time_log = 0;

    this->set_point_EL = 0;
    this->set_point_AZ = 0;

    this->flag_open_log = false;

    this->gyro_offset_param_01 = 5;
    this->gyro_offset_param_02 = 5;

    this->w_ZF = 0;
    this->gyro_angle = 0;
    this->delta_pre = 0;

    this->torque_filter = 0;
    this->PID_detect_I = 0;
    this->PID_detect_P = 0;

    // test offset2
    this->angle_setpoint = 0;
    this->angle_r = 0;
    this->T_detect_offset_2 = 0;
    this->I_angle_controller = 0;

    this->L = 0;
    this->y_position = 0;
    this->z_position = 0;

    this->test_angle = 0;
    this->test_bias = 0;
    this->test_rate = 0;
    this->test_rate_angle = 0;
    this->gyro_vel_bias = 0;
    this->stop_controller_flag = false;
}

ControllerManageWorker::~ControllerManageWorker()
{
}


void ControllerManageWorker::stopControllerAsSafety(void){
    std::cout << "stop velocity controller" << std::endl;
    uint8_t cnt = 0;
    this->stop_controller_flag = true;
    while (1){
        cnt++;
        if(cnt > 10){
            break;
        }
        this->setTorqueEL = 0;
        driverManageWorker.ELDriver.setTargetTorque(0);   
        usleep(100000);             
    }
}

void ControllerManageWorker::run(void){
    // std::thread DebugLogThread(&ControllerManageWorker::debugLog, this);
    //std::thread InputParamThread(&ControllerManageWorker::inputParam, this);
    
    while (1)
    {
        bool result = true;
        ImuSfogData_s imuData;
        imuData = sensorManageWorker.sfogIMU.getData();
        if(imuData.imu_state == false){
            result = false;
        }
        DriverInfor el_driver_infor;
        el_driver_infor = driverManageWorker.ELDriver.getData();
        if(el_driver_infor.state == DRIVER_NOT_READY){
            result = false;
        }
        if(result == true){
            myLogger.sysLogger->info("Check IMU and Driver: OK");
            break;
        }
        sleep(1);
    }
    
    
    std::thread DebugLogThread(&ControllerManageWorker::debugLogController, this);
    std::thread InputParamThread(&ControllerManageWorker::inputParamController, this);
    std::thread LogFileThread(&ControllerManageWorker::logFileController2, this);

    sleep(4);

    myLogger.sysLogger->info("Starting Controller for Test...");
    ImuSfogData_s imuData;
    GyroData_s gyroData;
    DriverInfor az_driver_infor;
    DriverInfor driverDataEL;

    float torque_value = 0;

    // this->inputParam();
    // std::thread LogFileThread(&ControllerManageWorker::logFile, this);
    imuData = sensorManageWorker.sfogIMU.getData();
    //float heading_init = imuData.heading_data;
    //this->AZController.param_03.set_point = imuData.heading_data;    
    //this->AZController.param_03.x1_1 = this->AZController.param_03.set_point;
    this->torque_debug = 0;

    //this->AZController.param_03.set_point = a_get;
    

    uint32_t time_test = 0;

    // float dt = 0;

    // float u_sin = 0, u_sin_1 = 0;
    // float du = 0;


    // double phi_1 = 0, phi_2 = 0, phi_p_phi = 0;
    // double w_k = 0, w_k_1 = 0, T_k = 0, w_gyro_k_1 = 0, w_gyro_k_2 = 0;
    // int16_t T_k_1 = 0;
    // double w_imu_1 = 0;
    // double P_11 = 6.95 * pow(10, -6);
    // double P_12 = 1.08 * pow(10, -6);
    // double P_21 = 1.08 * pow(10, -6);
    // double P_22 = 2.18 * pow(10, -6);
    // double P = 0, lamda = 0.995;
    // double L_1 = 0, L_2 = 0;

    //double theta1_k = 0, theta1_k_1 = -0.99858, theta2_k = 0, theta2_k_1 = 0.00176;
   // double epsilon = 0;

   // double phi_1_y = 0, phi_2_y = 0;
    //double P_11 = 0, P_12 = 0, P_22 = 0;

    uint32_t time_detection = 0;

    // float I_filter = 0, e_n_filter = 0, N_filter = 1;
    // float dt_test = 0.001;

    // float T_debug = 0;  

    long double gyro_x1_test = 0;

    uint32_t time_detect = 0;


	// param for angle cacul
	// const float Q_angle = 0.01;
	// const float Q_bias = 0.0000032;
	// const float R_measure = 0.3;
	// float angle = 0;
	// float bias = 0;
	// float P_00 = 0;
	// float P_01 = 0;
	// float P_10 = 0;
	// float P_11_test = 0;
	// float theta = 0;
	// float newAngle = 0, newRate = 0, rate = 0;
	// double dt = 0.001;	

	// float alpha_filter = 0;    

    float theta_init = 180 * atan2(imuData.acc_y, imuData.acc_x) / PI;

    struct sigaction sigIntHandler;

    sigIntHandler.sa_handler = my_handler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;

    sigaction(SIGINT, &sigIntHandler, NULL);    

    driverDataEL = driverManageWorker.ELDriver.getData();
    int32_t resol_pos_init = driverDataEL.resol_pos;

    while (1)
    {
        // if((imuData.imu_state == false) || (az_driver_infor.state == DRIVER_NOT_READY)){
        //     myLogger.sysLogger->info("Stop Controller for Test...");
        //     break;
        // }

        imuData = sensorManageWorker.sfogIMU.getData();
        gyroData = sensorManageWorker.memGyro.getData();
        imuData.ang_vel_x = -imuData.ang_vel_x;
        driverDataEL = driverManageWorker.ELDriver.getData();

		if ((this->stop_controller_flag) || (imuData.imu_state != 1) || ((this->a_get == 0) && (this->b_get == 0))){
            this->stopControllerAsSafety();
            break;
		}

       // theta = 180 * atan2(imuData.acc_y, imuData.acc_x) / PI;

        // alpha_filter = 0.98 * (alpha_filter + imuSfogData.ang_vel_x * 0.001) + 0.02 * theta;

        // newAngle = theta - theta_init;
        // newRate = gyroData.x_axis_filter;

        // rate = newRate - bias;
        // angle += dt * rate;
        // P_00 += dt * (dt * P_11_test - P_01 - P_10 + Q_angle);
        // P_01 -= dt * P_11_test;
        // P_10 -= dt * P_11_test;
        // P_11_test += Q_bias * dt;

        // float S = P_00 + R_measure;
        // float K[2];
        // K[0] = P_00 / S;
        // K[1] = P_10 / S;
        
        // float y = newAngle - angle;

        // angle += K[0] * y;
        // bias += K[1] * y;

        // float P_00_temp = P_00;
        // float P_01_temp = P_01;

        // P_00 -= K[0] * P_00_temp;
        // P_01 -= K[0] * P_01_temp;
        // P_10 -= K[1] * P_00_temp;
        // P_11_test -= K[1] * P_01_temp;   

        // this->test_angle = angle;
        // this->test_rate = rate;
        // this->test_bias = bias;

        // this->gyro_vel_bias = gyroData.x_axis_filter + bias;
       // this->z_position += gyroData.x_axis_filter * 0.001;
      //  this->z_position += rate*0.001;
      //  this->y_position += gyroData.y_axis_filter * 0.001;
      //  this->test_rate_angle += rate * 0.001;

       // Bias Filter
        this->ELController.biasFilter.acc_X_Imu = imuData.acc_x;
        this->ELController.biasFilter.acc_Y_Imu = imuData.acc_y;
        this->ELController.biasFilter.gyro_X_filter = gyroData.x_axis_filter;
        this->ELController.biasFilter.calculBiasFilter();
          
        // Recognition Controller
        // this->ELController.recogController.imu_angle_vel = imuData.ang_vel_x;
        // this->ELController.recogController.gyro_angle_vel = gyroData.x_axis_filter;
        // this->ELController.recogController.torque = setTorqueEL;
        // this->ELController.recogController.caculController();
        
        // Identification
        this->ELController.identification.omega_X_Imu = imuData.ang_vel_x;
        this->ELController.identification.omega_X_Gyro = this->ELController.biasFilter.rate;
        this->ELController.identification.Torque = setTorqueEL;
        this->ELController.identification.calculIdentification();

        // Friction Identification controller
        this->ELController.idFriction.omega_gyro = this->ELController.biasFilter.rate;
        this->ELController.idFriction.omega_imu = imuData.ang_vel_x;
        this->ELController.idFriction.Torque = setTorqueEL;
        this->ELController.idFriction.loadMass = 50;
        this->ELController.idFriction.calculIdFriction();

        // numerical PID Controller
        //this->ELController.numerPIDController.gyro_angle_vel = gyroData.x_axis_filter;
        this->ELController.numerPIDController.gyro_angle_vel = this->ELController.biasFilter.rate;
        this->ELController.numerPIDController.kp = 8;
        this->ELController.numerPIDController.ki = 0.3;
        this->ELController.numerPIDController.caculController();    

        // compenstate friction
        //this->ELController.compensateFriction.K_f = this->a_get;
        //this->ELController.compensateFriction.gyro_angle_vel = gyroData.x_axis_filter;
        // this->ELController.compensateFriction.gyro_angle_vel = rate;
        // this->ELController.compensateFriction.imu_angle_vel = imuData.ang_vel_x;
       // this->ELController.compensateFriction.resolver_vel = driverDataEL.resol_vel/1000.0;
       // this->ELController.compensateFriction.resolver_pos = (driverDataEL.resol_pos - resol_pos_init)/1000.0;
       // this->ELController.compensateFriction.caculController();

        // Torque Control
       // this->setTorqueEL = -(this->ELController.numerPIDController.T_control + this->ELController.compensateFriction.T_compenstateFriction);
        this->setTorqueEL = -this->ELController.numerPIDController.T_control;
        // Send Toque
		if (driverManageWorker.ELDriver.setTargetTorque(this->setTorqueEL) == 0)
		{
            std::cout << "driverManageWorker.ELDriver.setTargetTorque - send error" << setTorqueEL << std::endl;
		}        

        usleep(1000);
    }
    DebugLogThread.join();
    
}



void ControllerManageWorker::logFileController2(void){
	std::ofstream writeFile;
    uint32_t index_log = 0;

    GyroData_s gyroLog;
    ImuSfogData_s imuLog;
    DriverInfor elDriverData;

    sleep(3);

    imuLog = sensorManageWorker.sfogIMU.getData();
    float heading_init = imuLog.heading_data;
    float roll_init = imuLog.roll_data;
    float pitch_init = imuLog.pitch_data;

    elDriverData = driverManageWorker.ELDriver.getData();

    float angle_z_filter_init = imuLog.angle_z_filter;
    int32_t resol_pos_init = elDriverData.resol_pos;

	while (true){
		flag_open_log = 1;
        uint64_t log_index = 0;
		while (true){            
            
			char index_file[10] = { 0 };
			char log_file_name[50] = { 0 };
			if (flag_open_log){
				flag_open_log = 0;
				index_log = getTimestamp();
				writeFile.close();
				memset(index_file, 0, 3);
				memset(log_file_name, 0, 20);
				sprintf(index_file, "%d", index_log);
                std::string cmd = "mkdir -p /home/1207/logFile/";
                system(cmd.c_str());
				strcat(log_file_name, "/home/1207/logFile/log_");
				strcat(log_file_name, index_file);
				strcat(log_file_name, ".txt");
				writeFile.open(log_file_name);

                // init header
                writeFile << "index"
                        //  << "\t" << "gyro_ang_x" << "\t" << "imuLog_vel_x" << "\t" << "gyro_vel_x_filter"
                          << "\t" << "angle_X" << "\t" << "omega_X_bais_filter" << "\t" << "bias"
                          << "\t" << "k" << "\t" << "T_1" << "k_f"
                          << "\t"<< "Torque_EL" << "\t"<< "T_PID"<< "\t"<< "T_compenstateFriction"
                          << "\t" << "elDriverData_resol_vel" << "\t" << "elDriverData_resol_pos" << "\t" << "note: Test nhan dang," << std::endl;
                       
			}
			imuLog = sensorManageWorker.sfogIMU.getData();
            elDriverData = driverManageWorker.ELDriver.getData();
            gyroLog = sensorManageWorker.memGyro.getData();

			log_index++;
            writeFile << log_index 
                    //  << "\t" << this->z_position << "\t" << imuLog.ang_vel_x << "\t" << gyroLog.x_axis_filter
                      << "\t" << this->ELController.biasFilter.angle << "\t" << this->ELController.biasFilter.rate << "\t" << this->ELController.biasFilter.bias
                  //    << "\t" << this->ELController.recogController.k << "\t" << this->ELController.recogController.T_1 
                      << "\t" << this->ELController.idFriction.k << "\t" << this->ELController.idFriction.T1 << "\t" << this->ELController.idFriction.k_f
                   //   << "\t" << this->setTorqueEL << "\t" << this->ELController.recogController.T_detect_offset << "\t" << this->ELController.numerPIDController.T_control << "\t"<<this->ELController.compensateFriction.T_compenstateFriction
                      << "\t" << this->setTorqueEL << "\t" << this->ELController.numerPIDController.T_control << "\t" << this->ELController.idFriction.T_compenstate
                      << "\t" << (elDriverData.resol_vel / 1000.0) << "\t" << ((elDriverData.resol_pos - resol_pos_init) / 1000.0) << std::endl;            
            
			usleep(1000);
		}
	}
}


void ControllerManageWorker::runOldController(void){
    std::thread DebugLogThread(&ControllerManageWorker::debugLogController, this);

    std::thread InputParamThread(&ControllerManageWorker::inputParamController, this);
    
    while (1)
    {
        
        bool result = true;
        ImuSfogData_s imuData;
        imuData = sensorManageWorker.sfogIMU.getData();
        if(imuData.imu_state == false){
            result = false;
        }
        DriverInfor el_driver_infor;
        el_driver_infor = driverManageWorker.ELDriver.getData();
        if(el_driver_infor.state == DRIVER_NOT_READY){
            result = false;
        }
        if(result == true){
            myLogger.sysLogger->info("Check IMU and Driver: OK");
            break;
        }
        sleep(1);
    }
    
    
    myLogger.sysLogger->info("Starting to old Controller ...");
    ImuSfogData_s imuData;
    DriverInfor az_driver_infor;
    
    std::thread LogFileThread(&ControllerManageWorker::logFileController, this);
    std::thread VelocityController(&ControllerManageWorker::runVelocityController, this);   

    VelocityController.join();
    LogFileThread.join();
    
}

void ControllerManageWorker::debugLogController(void){

    DriverInfor driverData;
    ImuSfogData_s imuData;
    GyroData_s gyroData;

    while (1)
    {
        imuData = sensorManageWorker.sfogIMU.getData();
        gyroData = sensorManageWorker.memGyro.getData();
        driverData = driverManageWorker.AZDriver.getData();

        myLogger.sysLogger->info("torque = {0}, gyroData.x_axis_filter = {1}, imuData.ang_vel_x = {2}", 
            std::to_string(this->setTorqueEL), std::to_string(gyroData.x_axis_filter), std::to_string(imuData.ang_vel_x));
        usleep(500000);
    }
    
}


void ControllerManageWorker::logFileController(void){
	std::ofstream writeFile;
    uint32_t index_log = 0;

    GyroData_s gyroLog;
    ImuSfogData_s imuLog;
    DriverInfor elDriverData;

    sleep(3);

    imuLog = sensorManageWorker.sfogIMU.getData();
    float heading_init = imuLog.heading_data;
    float roll_init = imuLog.roll_data;
    float pitch_init = imuLog.pitch_data;

    elDriverData = driverManageWorker.ELDriver.getData();

    float angle_z_filter_init = imuLog.angle_z_filter;
    int32_t resol_pos_init = elDriverData.resol_pos;

	while (true){
		flag_open_log = 1;
        uint64_t log_index = 0;
		while (true){            
            
			char index_file[10] = { 0 };
			char log_file_name[50] = { 0 };
			if (flag_open_log){
				flag_open_log = 0;
				index_log = getTimestamp();
				writeFile.close();
				memset(index_file, 0, 3);
				memset(log_file_name, 0, 20);
				sprintf(index_file, "%d", index_log);
				strcat(log_file_name, "/home/logFile/log_");
				strcat(log_file_name, index_file);
				strcat(log_file_name, ".txt");
				writeFile.open(log_file_name);

                writeFile << "index"
                          << "\t" << "imuLog_ang_z" << "\t" << "gyro_ang_z"
                          << "\t" << "imuLog_ang_vel_z" << "\t" << "gyroLog_y_axis_filter" << "\t" << "e_input" 
                          << "\t" << "Torque_EL_non_offset" << "\t" << "T_offset" << "\t" << "Torque_EL" << "\t" << "Torque_EL_Filter"
                          << "\t" << "elDriverData_resol_vel" << "\t" << "elDriverData_resol_pos" << "\t" << "note: TestPID loc Khau D - khong bu gyro," << std::endl;
                        
			}
			imuLog = sensorManageWorker.sfogIMU.getData();
            elDriverData = driverManageWorker.ELDriver.getData();
            gyroLog = sensorManageWorker.memGyro.getData();

			log_index++;
            this->gyro_angle += (gyroLog.y_axis_filter + (elDriverData.resol_vel / 1000.0)) * 0.001;
            writeFile << log_index 
                      << "\t" << this->z_position << "\t" << this->gyro_angle
                      << "\t" << imuLog.ang_vel_z << "\t" << gyroLog.y_axis_filter << "\t" << this->ELController.originPIDController.e
                      << "\t" << this->ELController.originPIDController.T_control << "\t" << this->t_offset_old << "\t" << this->setTorqueEL << "\t" << this->torque_filter
                      << "\t" << (elDriverData.resol_vel / 1000.0) << "\t" << ((elDriverData.resol_pos - resol_pos_init) / 1000.0) << std::endl;            
            
			usleep(1000);
		}
	}
}



void ControllerManageWorker::runVelocityController(void){
	// Disturbance observer parameters
	float w_est_el = 0, w_est_el_1 = 0;
	float i_est_el = 0, i_est_el_1 = 0;
	float w_est_az = 0, w_est_az_1 = 0;
	float i_est_az = 0, i_est_az_1 = 0;
	
	// Tham số bù trọng lực, imbelance
	float gravity[3] = { 0, 0, 9.81 };
	
	// Ma trận quay từ Earth sang IMU
	float R_E2EL[9];
	float R_E2AZ[9];
	float R_AZ2E[9];
	float R_y[9];
	
	float ang_x = 0;
	float ang_y = 0;
	float ang_z = 0;
	// Tầm
	float alpha_el = 30;
	float length_el = 1; // length, alpha tọa độ cự của khối tam khâu tầm
	float r_el[3]; // tọa độ khối tâm EL;
	r_el[0] = length_el*sin(alpha_el * 180 / PI);
	r_el[1] = 0;
	r_el[2] = length_el*cos(alpha_el * 180 / PI);
	float g_el[3];
	float acc_el[0];
	float m_el = 0; // khối lượng EL
	float M_acc_el[3]; // nhiễu lực do sự không cân bằng và trọng lực tác dụng lên khâu EL
	float k0_el = 0;
	// Hướng
	float alpha_az = 0;
	float length_az = 0; // length, alpha tọa độ cự của khối tam khâu tầm
	float r_az[3]; // tọa độ khối tâm EL;
	r_az[0] = length_az*cos(alpha_az * 180 / PI);
	r_az[2] = 0;
	r_az[1] = length_az*sin(alpha_az * 180 / PI);
	float g_az[3];
	float acc_az[0];
	float m_az = 0; // khối lượng EL
	float M_acc_az[3]; // nhiễu lực do sự không cân bằng và trọng lực tác dụng lên khâu EL
	//******************************
	
	double	p1 = -5.564 * exp(-11),
			p2 = 1.675 * exp(-8), 
			p3 = -2.093 * exp(-6), 
			p4 = 0.0001404, 
			p5 = -0.005466, 
			p6 = 0.1251, 
			p7 = -1.634, 
			p8 = 13.43, 
			p9 = 150.4;
	double fx = 0;
	
	struct timeval start_time;
	struct timeval end_time;
	
	double Ie = 0;
	
	
	double kp = 0, ki = 0, kd = 0;
	// Pid PIDTestAZ(kp, ki, kd, 0.002, -1000, 1000);
	// LowPassFilter lpFilter(200, 0.0004);
	// tinh goc lech cua he
	float a, b, c;
	float a11, a13, a21, a23, a31, a33;
	float deviation_angle;
	
	ImuSfogData_s imuData;
    GyroData_s gyroData;
	DriverInfor driverDataEL;
	DriverInfor driverDataAZ;
	float resolver_pos_el_ini = 0;
	float resolver_pos_el = 0;
	float resolver_pos_az_ini = 0;
	float resolver_pos_az = 0;
	float q_az, q_el;
	
	float _q_el = 0;
	float _q_az = 0;
		
	float resolver_vel_EL = 0;
	float resolver_vel_AZ = 0;
	
	// Disturbance observer parameters
	float Ti = 0, Ti_el = 0, Ti_az = 0;
	
	// check can error
	uint8_t check_can1 = 0;
	uint8_t check_can2 = 0;
	long double gyro_x1_test = 0;
	
	// PID DSP
	double _Integral_EL = 0;
	float ki_debug = 1000;


    // test offset
    float e_test = 0;
    float resol_pos_k_1 = 0;
    float w_ad = 0, w_bn = 0, T_D_EL_1 = 0, T_D_EL_2 = 0;
    float A_d = 1.502, A_r = 1.445;
	
	this->set_point_EL = 0;
	this->set_point_AZ = 0;
	
	std::cout << " start velocity controller" << std::endl;
	
	// get Driver Data
	driverDataEL = driverManageWorker.ELDriver.getData();
	driverDataAZ = driverManageWorker.AZDriver.getData();
	
	resolver_pos_el_ini = -driverDataEL.resol_pos / 1000.0; // q1_zero_point
	resolver_pos_az_ini = -driverDataAZ.resol_pos / 1000.0; // q1_zero_point

    float input_EL_pre = 0;
    float input_EL = 0;
    bool check_t_max = true;
    bool is_check_t_max_first = true;

    bool reset_I = true;

	usleep(5000);
	while (true)
	{	
        usleep(1000);
		// get IMU Data
		imuData = sensorManageWorker.sfogIMU.getData();     
        gyroData = sensorManageWorker.memGyro.getData();   
		// check imu
		if ((this->stop_controller_flag) || (imuData.imu_state != 1) || ((this->a_get == 0) && (this->b_get == 0)))
		{
            this->stopControllerAsSafety();
			break;
		}
		
		// get Driver Data
		driverDataEL = driverManageWorker.ELDriver.getData();
		driverDataAZ = driverManageWorker.AZDriver.getData();
		
		
		resolver_vel_EL = driverDataEL.resol_vel / 1000.0;
		resolver_vel_AZ = driverDataAZ.resol_vel / 1000.0;	
		
		
		// // tính ma chận quay
		// ang_x = imuData.roll_data*PI / 180;
		// ang_y = imuData.pitch_data*PI / 180;
		// ang_z = imuData.heading_data*PI / 180;
		// R_E2EL[0] = cos(ang_z)*cos(ang_y);
		// R_E2EL[1] = cos(ang_z)*sin(ang_y)*sin(ang_x) - sin(ang_z)*cos(ang_x);
		// R_E2EL[2] = cos(ang_z)*sin(ang_y)*cos(ang_x) + sin(ang_z)*sin(ang_x);
		// R_E2EL[3] = sin(ang_z)*cos(ang_y);
		// R_E2EL[4] = sin(ang_z)*sin(ang_y)*sin(ang_x) + cos(ang_z)*cos(ang_x);
		// R_E2EL[5] = sin(ang_z)*sin(ang_y)*cos(ang_x) - cos(ang_z)*sin(ang_x);
		// R_E2EL[6] = -sin(ang_y);
		// R_E2EL[7] = cos(ang_y)*sin(ang_x);
		// R_E2EL[8] = cos(ang_y)*cos(ang_x);
		
		// this->rotz(R_y, -q_el * PI / 180);
		// this->multiply3x3(R_E2EL, R_y, R_E2AZ);
		// this->transpose3x3(R_E2AZ, R_AZ2E);
		// this->multiply3x3x3x1(R_AZ2E, gravity, g_az);
		
		// acc_el[0] = imuData.acc_x;
		// acc_el[1] = imuData.acc_y;
		// acc_el[2] = imuData.acc_z;
		
		// // only gravity, compensate for EL axis
		// M_acc_az[0] = m_az*(r_az[1]*g_az[2] - r_az[2]*g_az[1]);
		// M_acc_az[1] = m_az*(r_az[2]*g_az[0] - r_az[0]*g_az[2]);
		// M_acc_az[2] = m_az*(r_az[0]*g_az[1] - r_az[1]*g_az[0]); // need to be compensated
		
		// acc_el[0] = imuData.acc_x;
		// acc_el[1] = imuData.acc_y;
		// acc_el[2] = imuData.acc_z;
		
		// // include gravity and other acc disturbance, compensate for AZ axis
		// M_acc_el[0] = m_el*(r_el[1]*acc_el[2] - r_el[2]*acc_el[1]);
		// M_acc_el[1] = m_el*(r_el[2]*acc_el[0] - r_el[0]*acc_el[2]); // need to be compensated
		// M_acc_el[2] = m_el*(r_el[0]*acc_el[1] - r_el[1]*acc_el[0]);
/**====================================================================== EL Driver ============================================ **/
#pragma region EL Driver
		
		/*
		* period = 1000us
		* kp = 10
		* ki = 0.65
		* kd = 0
		* k0_el = 1.1
		* assume length_el = 1, we need to tune m_el = k*mass (mass: mass of EL axis)
		*/
			// HuyTai: Disturbance observer
		
		//k0_el = 0.0822;
		// k0_el = 1.1644;
		float_t F_dis_el = 0;
		// Ti_el = (PIDDriverEL.y_control / 1000.0) * 5.4 * 0.5;
		// i_est_el_1 = i_est_el;
		// w_est_el += (Ti_el + i_est_el_1) / 302;
		// i_est_el = (-resolver_vel_EL * PI / 180 - w_est_el) * k0_el; //
		
		
		// F_dis_el = (i_est_el / 5.4) * 0.5 * 1000;


        // test new w
        // w_ZF = (cos(imuData.heading_data) * sin(imuData.pitch_data) * cos(imuData.roll_data)) * imuData.ang_vel_x + (sin(imuData.heading_data) * sin(imuData.pitch_data) * cos(imuData.roll_data) - cos(imuData.heading_data) * sin(imuData.roll_data)) * imuData.ang_vel_y + cos(imuData.pitch_data) * cos(imuData.roll_data) * imuData.ang_vel_z;

        F_dis_el = 0;

        // numerial PID Controller
        this->ELController.numerPIDController.imu_angle_vel = imuData.ang_vel_z;
        this->ELController.numerPIDController.kp = 10;
        this->ELController.numerPIDController.ki = 0.4;
        this->ELController.numerPIDController.kd = 0;
        
        this->ELController.numerPIDController.caculController();
		
		// PIDDriverEL.y_control =  PIDDriverEL.y - F_dis_el; // - M_acc_el[1]*3.7;  // M/(100*0.5*5.4)*1000

        // origin PID Controller
        // this->ELController.originPIDController.e = - (imuData.ang_vel_z);
        // this->ELController.originPIDController.N = 0;
        // this->ELController.originPIDController.kd = 0;

        // this->ELController.originPIDController.caculController();


        this->gyro_offset_param_01 = 3;
        this->gyro_offset_param_02 = 10;
		
		this->t_offset_old = -(gyroData.y_axis_filter * this->gyro_offset_param_01 + (gyroData.y_axis_filter - gyro_x1_test) * this->gyro_offset_param_02);
		
		gyro_x1_test = gyroData.y_axis_filter;


        if(is_check_t_max_first){
            this->setTorqueEL = this->ELController.originPIDController.T_control + this->t_offset_old;
        }
        else{
            // In
            this->setTorqueEL = (this->ELController.originPIDController.T_control);
        }

        this->setTorqueEL  = this->ELController.numerPIDController.T_control;
		
		if (driverManageWorker.ELDriver.setTargetTorque(this->setTorqueEL) == 0)
		{
            std::cout << "driverManageWorker.ELDriver.setTargetTorque - send error" << setTorqueEL << std::endl;
		}
#pragma endregion
    }
}


// Multiply 2 matrix 3x3
void ControllerManageWorker::multiply3x3(float* a, float* b, float* c)
{
	c[0] = a[0]*b[0] + a[1]*b[3] + a[2]*b[6];
	c[1] = a[0]*b[1] + a[1]*b[4] + a[2]*b[7];
	c[2] = a[0]*b[2] + a[1]*b[5] + a[2]*b[8];
	c[3] = a[3]*b[0] + a[4]*b[3] + a[5]*b[6];
	c[4] = a[3]*b[1] + a[4]*b[4] + a[5]*b[7];
	c[5] = a[3]*b[2] + a[4]*b[5] + a[5]*b[8];
	c[6] = a[6]*b[0] + a[7]*b[3] + a[8]*b[6];
	c[7] = a[6]*b[1] + a[7]*b[4] + a[8]*b[7];
	c[8] = a[6]*b[2] + a[7]*b[5] + a[8]*b[8];
}
// 3x3 Matrix Multyply 3x1 Matrix
void ControllerManageWorker::multiply3x3x3x1(float* a, float* b, float* c)
{
	c[0] = a[0]*b[0] + a[1]*b[1] + a[2]*b[2];
	c[1] = a[3]*b[0] + a[4]*b[1] + a[5]*b[2];
	c[2] = a[6]*b[0] + a[7]*b[1] + a[8]*b[2];
}
// Matrix Transposition
void ControllerManageWorker::transpose3x3(float* a, float* b)
{
	b[0] = a[0];
	b[1] = a[3];
	b[2] = a[6];
	b[3] = a[1];
	b[4] = a[4];
	b[5] = a[7];
	b[6] = a[2];
	b[7] = a[5];
	b[8] = a[8];
}

void ControllerManageWorker::transpose3x3(float* a)
{
	float b[9];
	b[0] = a[0];
	b[1] = a[3];
	b[2] = a[6];
	b[3] = a[1];
	b[4] = a[4];
	b[5] = a[7];
	b[6] = a[2];
	b[7] = a[5];
	b[8] = a[8];
	
	a[0] = b[0];
	a[1] = b[1];
	a[2] = b[2];
	a[3] = b[3];
	a[4] = b[4];
	a[5] = b[5];
	a[6] = b[6];
	a[7] = b[7];
	a[8] = b[8];
}

// Matrix Rotation
void ControllerManageWorker::rotx(float* R, float val)
{
	R[0] = 1;
	R[1] = 0;
	R[2] = 0;
	R[3] = 0;
	R[4] = cos(val*PI / 180);
	R[5] = -sin(val*PI / 180);
	R[6] = 0;
	R[7] = sin(val*PI / 180);
	R[8] = cos(val*PI / 180);
}
void ControllerManageWorker::roty(float* R, float val)
{
	R[0] = cos(val*PI / 180);
	R[1] = 0;
	R[2] = sin(val*PI / 180);
	R[3] = 0;
	R[4] = 1;
	R[5] = 0;
	R[6] = -sin(val*PI / 180);
	R[7] = 0;
	R[8] = cos(val*PI / 180);
}
void ControllerManageWorker::rotz(float* R, float val)
{
	R[0] = cos(val*PI / 180);
	R[1] = -sin(val*PI / 180);
	R[2] = 0;
	R[3] = sin(val*PI / 180);
	R[4] = cos(val*PI / 180);
	R[5] = 0;
	R[6] = 0;
	R[7] = 0;
	R[8] = 1;
}


void ControllerManageWorker::runNonlinearController(void){
    std::thread DebugLogThread(&ControllerManageWorker::debugLogController, this);

    std::thread InputParamThread(&ControllerManageWorker::inputParamController, this);

    
    
    while (1)
    {
        
        bool result = true;
        ImuSfogData_s imuData;
        imuData = sensorManageWorker.sfogIMU.getData();
        if(imuData.imu_state == false){
            result = false;
        }
        DriverInfor el_driver_infor;
        el_driver_infor = driverManageWorker.ELDriver.getData();
        if(el_driver_infor.state == DRIVER_NOT_READY){
            result = false;
        }
        if(result == true){
            myLogger.sysLogger->info("Check IMU and Driver: OK");
            break;
        }
        sleep(1);
    }
    
    
    myLogger.sysLogger->info("Starting to old Controller ...");
    ImuSfogData_s imuData;
    DriverInfor el_driver_infor;

    // this->inputParamController();

    // while (1)
    // {
    //     // std::cout << "debug input param: " << this->a_get << " - " << this->b_get << " - " << this->c_get << std::endl;
    //     driverManageWorker.ELDriver.setTargetTorque(a_get);
    //     sleep(1);
    // }      
    
    std::thread LogFileThread(&ControllerManageWorker::logFileController, this);

    imuData = sensorManageWorker.sfogIMU.getData();
    this->torque_debug = 0;

    this->ELController.controller01.J = 0.5;
    this->ELController.controller01.dT = 0.001;
    this->ELController.controller01.eta = 0;
    this->ELController.controller01.torque_min = -1000;
    this->ELController.controller01.torque_max = 1000;

    // this->ELController.controller02.J = 0.5;
    // this->ELController.controller02.torque_min = -1000;
    // this->ELController.controller02.torque_max = 1000;
    // this->ELController.controller02.dT = 0.001;

    this->ELController.controller04.J = 0.5;
    this->ELController.controller04.dT = 0.001;
    this->ELController.controller04.torque_min = -1000;
    this->ELController.controller04.torque_max = 1000;    

    float angle_z_filter_init = imuData.angle_z_filter;

    while (1)
    {

        imuData = sensorManageWorker.sfogIMU.getData();
        // el_driver_infor = driverManageWorker.AZDriver.getData();


		if ((imuData.imu_state != 1) || (this->a_get == 0))
		{
			std::cout << "imu status: not found" << std::endl;
			std::cout << "stop velocity controller" << std::endl;

            this->setTorqueEL = 0;
            driverManageWorker.ELDriver.setTargetTorque(0);
            // driverManageWorker.AZDriver.setTargetTorque(0);
			break;
		}

        // controller

        // this->ELController.controller01.c1 = a_get;
        // this->ELController.controller01.c2 = b_get;
        // this->ELController.controller01.x2 = imuData.ang_vel_z;
        // this->ELController.controller01.x1 += this->ELController.controller01.x2 * this->ELController.controller01.dT;
        // this->ELController.controller01.eta = c_get;

        // this->ELController.controller02.k1 = a_get;
        // this->ELController.controller02.k2 = b_get;
        // this->ELController.controller02.lamda = c_get;
        // this->ELController.controller02.eta = d_get;
        // this->ELController.controller02.x2 = imuData.ang_vel_z;
        // // this->ELController.controller02.x1 += this->ELController.controller02.x2 * this->ELController.controller02.dT;
        // this->ELController.controller02.x1 = imuData.angle_z_filter - angle_z_filter_init;

        this->ELController.controller04.c1 = a_get;
        this->ELController.controller04.k1 = b_get;
        this->ELController.controller04.k2 = c_get;
        this->ELController.controller04.x2 = imuData.ang_vel_z;
        this->ELController.controller04.x1 += this->ELController.controller04.x2 * this->ELController.controller04.dT;

        this->ELController.controller04.caculController();

        setTorqueEL = this->ELController.controller04.torque;

        // this->az_torque = setTorqueEL;
                
		// if (driverManageWorker.ELDriver.setTargetTorque(setTorqueEL) == 0){
        //     std::cout << "driverManageWorker.ELDriver.setTargetTorque - send error" << setTorqueEL << std::endl;
		// }
        usleep(1000);
    }
    DebugLogThread.join();
    
}