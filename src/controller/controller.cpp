/**
******************************************************************************
* @file           : controller.cpp
* @brief          : source file of Controller
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
/* Include -----------------------------------------------------------------*/
#include "controller/controller.h"
#include <math.h>



Controller::Controller(){
}

Controller::~Controller(){
}

void Controller01::caculController(void){
    I += eta * (c1 * x1 + x2) * dT;

    // if(this->param.I > this->param.torque_max){
    //     this->param.I = this->param.torque_max;
    // }
    // else if(this->param.I < this->param.torque_min){
    //     this->param.I = this->param.torque_min;
    // }
    torque_log = -J * ((1 + c1 * c2) * x1 + (c1 + c2) * x2);
    t_off = -J * I;
    torque = -J * ((1 + c1 * c2) * x1 + (c1 + c2) * x2 + I);
    
    torque_unlimit = torque;

    if(torque > torque_max){
        torque = torque_max;
    }
    else if(torque < torque_min){
        torque = torque_min;
    }
}


void Controller02::caculController(void){
    s = x2 + lamda * x1;

    t_offset += -J * s * dT * eta;

    // if(s >= 0){
    //     sign_s = 1;
    // }
    // else if(s < 0){
    //     sign_s = -1;
    // }

    tanh_s = (exp(2*s) - 1.0) / ((exp(2*s) + 1.0));

    //tan_s = (exp(2*s) - 1.0) / ((exp(2*s) + 1.0));

    torque = t_offset - J * (lamda * x2 + k1 * s + k2 * tanh_s);

    if(torque > torque_max){
        torque = torque_max;
    }
    else if(torque < torque_min){
        torque = torque_min;
    }
}


void Controller03::caculController(void){

    //kp = k / (J * sqrt(a));
    //ki = (pow(k, 2)) / (pow(J, 2) * sqrt(pow(a, 3)));

    y += ki * (e) * dt;

    // if(y > 100){
    //     y = 100;
    // }
    // else if(y < -100){
    //     y = -100;
    // }

    torque = kp * (e) + y;

    // if(torque > torque_max){
    //     torque = torque_max;
    // }
    // else if(torque < torque_min){
    //     torque = torque_min;
    // }
}


void Controller04::caculController(void){
    s = x2 + c1 * x1;

    tanh_s = (exp(2*s) - 1.0) / ((exp(2*s) + 1.0));


    torque = -J * (c1 * x2 + x1 * s + k1 * s + k2 * tanh_s);

    if(torque > torque_max){
        torque = torque_max;
    }
    else if(torque < torque_min){
        torque = torque_min;
    }
}


void Controller::caculPosController(void){
    this->pidPosParam.kp = this->pidPosParam.k / (this->pidPosParam.J * sqrt(this->pidPosParam.a));
    this->pidPosParam.ki = (this->pidPosParam.k * this->pidPosParam.k) / (this->pidPosParam.J * this->pidPosParam.J * sqrt(this->pidPosParam.a * this->pidPosParam.a * this->pidPosParam.a));
    this->pidPosParam.y += this->pidPosParam.ki * (this->pidPosParam.e) * this->pidPosParam.dt;

    this->pidPosParam.output = this->pidPosParam.kp * (this->pidPosParam.e) + this->pidPosParam.y;    
}


void OriginPIDController::caculController(void){

    P = kp * e;
    I += ki * e;
    D = N * ( kd * e - I_D);
    I_D += D;

    T = P + I + D;

	if (T >= y_limit_max)
	{
		T = y_limit_max;
	}
	else if (T <= y_limit_min)
	{
		T = y_limit_min;
	} 
    T_control = T;
}


void ReCognitionController::caculController(void){
    T_k_1 = -torque;

    // w_k = driverDataEL.resol_vel / 1000.0;
    w_k = gyro_angle_vel - imu_angle_vel;

    phi_1 = -w_k_1;
    phi_2 = T_k_1;
    w_k_1 = w_k;     
    
    phi_p_phi = P_11 * phi_1 * phi_1 + P_22 * phi_2 * phi_2 + (P_21 + P_12) * phi_1 * phi_2;

    L_1 = (P_11 * phi_1 + P_12 * phi_2) / (lamda + phi_p_phi);

    L_2 = (P_21 * phi_1 + P_22 * phi_2) / (lamda + phi_p_phi);        

    P_11 = (P_11 - (P_11 * P_11 * phi_1 * phi_1 + (P_12 + P_21) * P_11 * phi_1 * phi_2 + P_12 * P_21 * phi_2 * phi_2) / (lamda + phi_p_phi)) / lamda;

    P_12 = (P_12 - (P_11 * P_12 * phi_1 * phi_1 + (P_12 * P_12 + P_11 * P_22) * phi_1 * phi_2 + P_22 * P_12 * phi_2 * phi_2) / (lamda + phi_p_phi)) / lamda;

    P_21 = (P_21 - (P_11 * P_21 * phi_1 * phi_1 + (P_11 * P_22 + P_21 * P_21) * phi_1 * phi_2 + P_21 * P_22 * phi_2 * phi_2) / (lamda + phi_p_phi)) / lamda;

    P_22 = (P_22 - (P_21 * P_12 * phi_1 * phi_1 + (P_12 + P_21) * P_22 * phi_1 * phi_2 + P_22 * P_22 * phi_2 * phi_2) / (lamda + phi_p_phi)) / lamda;


    epsilon = w_k - phi_1 * theta1_k_1 - phi_2 * theta2_k_1;

    theta1_k = theta1_k_1 + L_1 * epsilon;
    theta2_k = theta2_k_1 + L_2 * epsilon;

    // if((theta1_k < -0.9999) || (theta1_k > -0.99950012)){
    //     theta1_k = theta1_k_1;
    // }

    // if((theta2_k < -0.00005) || (theta2_k > -0.0015)){
    //     theta2_k = theta2_k_1;
    // }

    // this->theta1_k_min = this->a_get;
    // this->theta1_k_max = this->b_get;
    // this->theta2_k_min = this->a_get;
    // this->theta2_k_max = this->b_get;

    // if((theta1_k < this->theta1_k_min) || (theta1_k > this->theta1_k_max)){
    //     theta1_k = theta1_k_1;
    // }

    // if((theta2_k < 0.00016) || (theta2_k > 0.001)){
    //     theta2_k = theta2_k_1;
    // }

    theta1_k_1 = theta1_k;
    theta2_k_1 = theta2_k;

    k = theta2_k / (1 + theta1_k);

    T_1 = -0.001 / log(-theta1_k);

    if(((this->k >= 0.01) && (this->k <= 10)) && ((this->T_1 >= 0.05) && (this->T_1 <= 10))){
        // this->L = this->a_get;
        // this->T_detect_offset = - (((this->T_1 / this->k) * ((imu_angle_vel - gyro_angle_vel_pre) / 0.05)) + (imu_angle_vel / this->k));
        // this->T_detect_offset = - ( ( (this->T_1 * this->L / this->k) * ((gyroData.y_axis_filter - 2 * w_gyro_k_1 + w_gyro_k_2) / this->b_get) ) 
        //                             + (((this->T_1 + this->L) / this->k) * ((gyroData.y_axis_filter - w_gyro_k_1) / this->b_get)) 
        //                             + ((1 / this->k) * gyroData.y_axis_filter));
        // this->angle_r = this->angle_setpoint - this->z_position;
        // this->T_detect_offset_2 = -((this->T_1 / this->k) * gyroData.x_axis_filter + (this->angle_r / this->k));

        // this->PID_detect_I += (-imuData.ang_vel_z);
        // this->PID_detect_P = this->a_get * (-imuData.ang_vel_z);
        // this->PID_detect = PID_detect_P + this->PID_detect_I * ((1 + 2 * this->k * this->a_get) / (2 * this->T_1 * this->k));
        // this->setTorqueEL = this->T_detect_offset; 
        // if(this->setTorqueEL > 1000){
        //     this->setTorqueEL = 1000;
        // }
        // else if(this->setTorqueEL < -1000){
        //     this->setTorqueEL = -1000;
        // }
    }
    else{
        // T_detect_offset = 0;  
        // this->T_detect_offset_2 = 0;
        // this->angle_r = 0;  
        T_1 = 0.2;
        k = 0.45;  
        // T_detect_offset = - (((T_1 / k) * ((imuData.ang_vel_x - w_gyro_k_1) / 0.05)) + (imuData.ang_vel_x / this->k));
        // this->T_detect_offset = - ( ( (this->T_1 * this->L / this->k) * ((gyroData.y_axis_filter - 2 * w_gyro_k_1 + w_gyro_k_2) / this->b_get) ) 
                                    // + (((this->T_1 + this->L) / this->k) * ((gyroData.y_axis_filter - w_gyro_k_1) / this->b_get)) 
                                    // + ((1 / this->k) * gyroData.y_axis_filter));            
    }    
    // T_detect_offset = -(((T_1 / k) * ((gyro_angle_vel - gyro_angle_vel_pre) / 0.05)) + (gyro_angle_vel / k)); // Gyro for measure envir.
    T_detect_offset = - (((T_1 / k) * ((imu_angle_vel - imu_angle_vel_pre) / 0.05)) + (imu_angle_vel / k)); // IMU for measure envir.
    gyro_angle_vel_pre = gyro_angle_vel;
    imu_angle_vel_pre = imu_angle_vel;
}

void Identification::calculIdentification(void){

    T_pre = -Torque;
    y_pre = y;
    y = omega_X_Gyro - omega_X_Imu;

    phi1 = -y_pre;
    phi2 = Torque;

    epsi = y - phi1*theta1_pre - phi2*theta2_pre;

    phiT_p_phi = phi1*(p11_pre*phi1 + p21_pre*phi2) + phi2*(p12_pre*phi1 + p22_pre*phi2);

    L1 = (p11_pre*phi1 + p12_pre*phi2)/(lamda + phi1*(p11_pre*phi1 + p21_pre*phi2) + phi2*(p12_pre*phi1 + p22_pre*phi2));
    L2 = (p21_pre*phi1 + p22_pre*phi2)/(lamda + phi1*(p11_pre*phi1 + p21_pre*phi2) + phi2*(p12_pre*phi1 + p22_pre*phi2));
    
    theta1 = theta1_pre + L1*epsi;
    theta2 = theta2_pre + L2*epsi;

    theta1_pre = theta1;
    theta2_pre = theta2;

    p11 = p11_pre - (p11_pre*phi1*(p11_pre*phi1 + p12_pre*phi2) + p21_pre*phi2*(p11_pre*phi1 + p12_pre*phi2))/(lamda + phiT_p_phi);
    p12 = p12_pre - (p12_pre*phi1*(p11_pre*phi1 + p12_pre*phi2) + p22_pre*phi2*(p11_pre*phi1 + p12_pre*phi2))/(lamda + phiT_p_phi);
    p21 = p21_pre - (p11_pre*phi1*(p21_pre*phi1 + p22_pre*phi2) + p21_pre*phi2*(p21_pre*phi1 + p22_pre*phi2))/(lamda + phiT_p_phi);
    p22 = p22_pre - (p12_pre*phi1*(p21_pre*phi1 + p22_pre*phi2) + p22_pre*phi2*(p21_pre*phi1 + p22_pre*phi2))/(lamda + phiT_p_phi);
    p11_pre = p11;
    p12_pre = p12;
    p21_pre = p21;
    p22_pre = p22;

    k = theta2 / (1 + theta1);
    T1 = -0.001 / log(-theta1);

    T_compenstate = -((T1/k)*(omega_X_Imu - omega_X_Imu_pre)/0.05 + omega_X_Imu/k);
    omega_X_Imu_pre = omega_X_Imu;
}

void NumericalPIDController::caculController(void){
	a0 = kp + ki + kd;
	a1 = -kp - 2*kd;
	a2 = kd;	
	
	x2 = x1;
	x1 = x;
	x = set_point - gyro_angle_vel;
	y1 = y; 
	y = y1 + a0 * x + a1 * x1 + a2 * x2;
	if (y >= y_limit_max)
	{
		T_control = y_limit_max;
	}
	else if (y <= y_limit_min)
	{
		T_control = y_limit_min;
	}
    else{
        T_control = y;
    }
}

void CompenstateFriction::caculController(void){
    
   // delta_omega = imu_angle_vel - gyro_angle_vel;
   // T_compenstateFriction = -K_f*delta_omega;
   // T_compenstateFriction = 0.5*0.0859*resolver_vel*resolver_vel/resolver_pos;
    //  T_compenstateFriction = K_f*(resolver_vel - resolver_vel_pre)/0.001;
    T_compenstateFriction = K_f*60;
      resolver_vel_pre = resolver_vel;
    if(T_compenstateFriction >= T_f_limit_max)
    {
        T_compenstateFriction = T_f_limit_max;
    }
}

void IdFriction::calculIdFriction(void){

    // Compute input
    y_pre = y;
    T_pre = -Torque;
    y = omega_gyro - omega_imu;
    // Assign variable
    phi1 = -y_pre;
    phi2 = T_pre;
    phi3 = loadMass;
    // Compute identification
    epsi = y - phi1*theta1_pre - phi2*theta2_pre - phi3*theta3_pre;

    phiT_p_phi = phi1*(p11_pre*phi1 + p21_pre*phi2 + p31_pre*phi3) + phi2*(p12_pre*phi1 + p22_pre*phi2 + p32_pre*phi3) + phi3*(p13_pre*phi1 + p23_pre*phi2 + p33_pre*phi3);

    L1 = (p11_pre*phi1 + p12_pre*phi2 + p13_pre*phi3)/(lamda + phiT_p_phi);
    L2 = (p21_pre*phi1 + p22_pre*phi2 + p23_pre*phi3)/(lamda + phiT_p_phi);
    L3 = (p31_pre*phi1 + p32_pre*phi2 + p33_pre*phi3)/(lamda + phiT_p_phi);

    theta1 = theta1_pre + L1*epsi;
    theta2 = theta2_pre + L2*epsi;
    theta3 = theta3_pre + L3*epsi;

    theta1_pre = theta1;
    theta2_pre = theta2;
    theta3_pre = theta3;
    
    p11 = p11_pre - (p11_pre*phi1*(p11_pre*phi1 + p12_pre*phi2 + p13_pre*phi3) + p21_pre*phi2*(p11_pre*phi1 + p12_pre*phi2 + p13_pre*phi3) + p31_pre*phi3*(p11_pre*phi1 + p12_pre*phi2 + p13_pre*phi3))/(lamda + phiT_p_phi);
    p12 = p12_pre - (p12_pre*phi1*(p11_pre*phi1 + p12_pre*phi2 + p13_pre*phi3) + p22_pre*phi2*(p11_pre*phi1 + p12_pre*phi2 + p13_pre*phi3) + p32_pre*phi3*(p11_pre*phi1 + p12_pre*phi2 + p13_pre*phi3))/(lamda + phiT_p_phi);
    p13 = p13_pre - (p13_pre*phi1*(p11_pre*phi1 + p12_pre*phi2 + p13_pre*phi3) + p23_pre*phi2*(p11_pre*phi1 + p12_pre*phi2 + p13_pre*phi3) + p33_pre*phi3*(p11_pre*phi1 + p12_pre*phi2 + p13_pre*phi3))/(lamda + phiT_p_phi);
    p21 = p21_pre - (p11_pre*phi1*(p21_pre*phi1 + p22_pre*phi2 + p23_pre*phi3) + p21_pre*phi2*(p21_pre*phi1 + p22_pre*phi2 + p23_pre*phi3) + p31_pre*phi3*(p21_pre*phi1 + p22_pre*phi2 + p23_pre*phi3))/(lamda + phiT_p_phi);
    p22 = p22_pre - (p12_pre*phi1*(p21_pre*phi1 + p22_pre*phi2 + p23_pre*phi3) + p22_pre*phi2*(p21_pre*phi1 + p22_pre*phi2 + p23_pre*phi3) + p32_pre*phi3*(p21_pre*phi1 + p22_pre*phi2 + p23_pre*phi3))/(lamda + phiT_p_phi);
    p23 = p23_pre - (p13_pre*phi1*(p21_pre*phi1 + p22_pre*phi2 + p23_pre*phi3) + p23_pre*phi2*(p21_pre*phi1 + p22_pre*phi2 + p23_pre*phi3) + p33_pre*phi3*(p21_pre*phi1 + p22_pre*phi2 + p23_pre*phi3))/(lamda + phiT_p_phi);
    p31 = p31_pre - (p11_pre*phi1*(p31_pre*phi1 + p32_pre*phi2 + p33_pre*phi3) + p21_pre*phi2*(p31_pre*phi1 + p32_pre*phi2 + p33_pre*phi3) + p31_pre*phi3*(p31_pre*phi1 + p32_pre*phi2 + p33_pre*phi3))/(lamda + phiT_p_phi);
    p32 = p32_pre - (p12_pre*phi1*(p31_pre*phi1 + p32_pre*phi2 + p33_pre*phi3) + p22_pre*phi2*(p31_pre*phi1 + p32_pre*phi2 + p33_pre*phi3) + p32_pre*phi3*(p31_pre*phi1 + p32_pre*phi2 + p33_pre*phi3))/(lamda + phiT_p_phi);
    p33 = p33_pre - (p13_pre*phi1*(p31_pre*phi1 + p32_pre*phi2 + p33_pre*phi3) + p23_pre*phi2*(p31_pre*phi1 + p32_pre*phi2 + p33_pre*phi3) + p33_pre*phi3*(p31_pre*phi1 + p32_pre*phi2 + p33_pre*phi3))/(lamda + phiT_p_phi);
    
    p11_pre = p11;
    p12_pre = p12;
    p13_pre = p13;
    p21_pre = p21;
    p22_pre = p22;
    p23_pre = p23;
    p31_pre = p31;
    p32_pre = p32;
    p33_pre = p33;

    k = theta2 / (1 + theta1);
    T1 = -0.001 / log(-theta1);
    k_f = theta3/theta2;

    T_compenstate = k_f*phi3;
}

void BiasFilter::calculBiasFilter(void){

       theta = 180 * atan2(acc_Y_Imu, acc_X_Imu)/3.1415926535897;

        // alpha_filter = 0.98 * (alpha_filter + imuSfogData.ang_vel_x * 0.001) + 0.02 * theta;

        newAngle = theta - theta_init;
        newRate = gyro_X_filter;

        rate = newRate - bias;
        angle += dt * rate;             // predict
        P_00 += dt * (dt * P_11 - P_01 - P_10 + Q_angle);
        P_01 -= dt * P_11;
        P_10 -= dt * P_11;
        P_11 += Q_bias * dt;

        float S = P_00 + R_measure;
        float K[2];
        K[0] = P_00 / S;
        K[1] = P_10 / S;
        
        float y = newAngle - angle;

        angle += K[0] * y;              // update
         bias += K[1] * y;                    

        float P_00_pre = P_00;
        float P_01_pre = P_01;

        P_00 -= K[0] * P_00_pre;
        P_01 -= K[0] * P_01_pre;
        P_10 -= K[1] * P_00_pre;
        P_11 -= K[1] * P_01_pre;   

        // this->gyro_vel_bias = gyroData.x_axis_filter + bias;
}