/**
******************************************************************************
* @file           : sys_logger.cpp
* @brief          : source file of system logger
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
/* Include -----------------------------------------------------------------*/
#include "logger/sys_logger.h"

SysLogger::Logger myLogger;
using namespace SysLogger;

Logger::Logger(){
    std::string cmd = "mkdir -p /home/1207/logMonitor/";
    system(cmd.c_str());
    this->log_path = "/home/1207/logMonitor/";
    this->sys_log_file = "system_logger";
    this->init();
}

Logger::~Logger(){
}

void Logger::init(void){
    this->sysLogger = spdlog::basic_logger_mt(this->sys_log_file, this->log_path + this->sys_log_file + ".txt");
    spdlog::set_default_logger(this->sysLogger);
    spdlog::flush_on(spdlog::level::info);
}