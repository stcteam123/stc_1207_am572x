/**
******************************************************************************
* @file           : XtrapulsPac.cpp
* @brief          : source file of XtrapulsPac Drives
******************************************************************************
******************************************************************************
*/
#include "motor_driver/XtrapulsPac.h"
#include <fstream>
#include <sys/time.h>

MotorDriver::MotorDriver()
{
	this->driverData.resol_pos = 0;
	this->driverData.resol_vel = 0;
	this->driverData.flag_get_data = 0;
	this->driverData.state = DRIVER_NOT_READY;
}

MotorDriver::~MotorDriver()
{
}


bool MotorDriver::initComm(uint16_t id){
	this->driver_id = id;
	if(this->driver_id == EL_DRIVER){
		this->logger = spdlog::basic_logger_mt("Xtrapul_EL_driver_log", "/home/1207/logMonitor/motor_driver/Xtrapul_EL_driver_log.txt");
	}
	else if(this->driver_id == AZ_DRIVER){
		this->logger = spdlog::basic_logger_mt("Xtrapul_AZ_driver_log", "/home/1207/logMonitor/motor_driver/Xtrapul_AZ_driver_log.txt");
	}
	if (this->driver_id == EL_DRIVER)
	{
		if(!this->canOpen.init(CAN1_INTERFACE)){
			this->logger->error("init Canbus: False");
			return false;
		}
		//this->logger->info("Starting to init EL Driver...");
	}
	else if (this->driver_id == AZ_DRIVER)
	{
		if(!this->canOpen.init(CAN0_INTERFACE)){
			this->logger->error("init Canbus: False");
			return false;
		}
		//this->logger->info("Starting to init AZ Driver...");
	}	
}

bool MotorDriver::initDriver(void)
{
	// init Driver
	
	if (this->setOperational())
	{
		this->logger->info("set Operation: success");
	}
	else
	{
		this->logger->error("set Operation: False");
		return 0;
	}
	
	if (this->setSwitchOn())
	{
		this->logger->info("set Switch On: success");
	}
	else
	{
		this->logger->error("set Switch On: False");
		return 0;
	}
	
	if (this->setTorqueMode())
	{
		this->logger->info("set Torque Mode: success");
	}
	else
	{
		this->logger->error("set Torque Mode: False");
		return 0;
	}
	
	if (this->setEnable())
	{
		this->logger->info("set Enable: success");
	}
	else
	{
		this->logger->error("set Enable: False");
		return 0;
	}
	
	
	// check ready
	if (!this->checkReady())
	{
		this->logger->warn("Driver is not ready");
		this->logger->info("Restarting Driver.....");
		if (this->resetDriver())
		{
			this->logger->info("Reset Driver: Success");
		}
		else
		{
			this->logger->error("Reset Driver: False");
			return 0;
		}
	}
	else
	{
		if (this->readIGBTError())
		{
			this->logger->error("Driver: IGBT error");
			this->logger->info("Restarting Driver....");
			if (this->resetDriver())
			{
				this->logger->info("Reset Driver: Success");
			}
			else
			{
				this->logger->error("Reset Driver: False");
				return 0;
			}
		}
	}
	if (!this->mapTorqueRPDO2())
	{
		return 0;
	}
	if (!this->mapResolTDPO2())
	{
		return 0;
	}
	if (!this->mapVelocityTDPO3())
	{
		return 0;
	}
	// if (!this->setResolTime())
	// {
	// 	return 0;
	// }
	// if (!this->setVelocityTime())
	// {
	// 	return 0;
	// }
	if (!this->activeResol())
	{
		return 0;
	}
	if (!this->activeVelocity())
	{
		return 0;
	}	
	this->logger->info("init Motor Driver: success");
	this->driverData.state = DRIVER_READY;
	return 1;
}


// switch Pre-Operatinal to Operational
bool MotorDriver::setOperational(void)
{
	uint16_t wMsgID = 0;
	uint8_t buff_send[8] = { 0 };
	uint8_t buff_recv[8] = { 0 };
	uint16_t timeout_send = 0;
	uint32_t timeout_recv = 0;
	
	while (true)
	{
		// set Operational Mode
		buff_send[0] = 0x01;
		buff_send[1] = (uint8_t)this->driver_id;
		if (this->canOpen.send(wMsgID, buff_send, 2))
		{
			usleep(10000);
			// check Operational
			wMsgID = 0x600 + this->driver_id;
			buff_send[0] = 0x40; // read mode: 4 byte
			buff_send[1] = 0x01; // index: 0x2001
			buff_send[2] = 0x20;
			buff_send[3] = 0;
			if (this->canOpen.send(wMsgID, buff_send, 4))
			{
				// check read
				timeout_recv = 0;
				while (true)
				{
					if (canOpen.recv(buff_recv, &wMsgID))
					{
						if (wMsgID == (0x580 + this->driver_id))
						{
							if (buff_recv[4] == 0x05) // Operational
							{
								return 1;
							}
							else if(buff_recv[4] == 0x7F) // Pre-Operatinal
							{
								break;
							}
						}
					}
					usleep(1);
					timeout_recv++;
					if (timeout_recv > 10000)
					{
						break;
					}
				}
			}
		}
		timeout_send++;
		if (timeout_send > 20)
		{
			return 0;
		}
	}
	return 0;
}

// set Switch ON
bool MotorDriver::setSwitchOn(void)
{
	uint16_t wMsgID = 0;
	uint8_t buff_send[8] = { 0 };
	uint8_t buff_recv[8] = { 0 };
	uint16_t timeout_send = 0;
	uint32_t timeout_recv = 0;
	
	while (true)
	{
		wMsgID = 0x200 + this->driver_id;
		buff_send[0] = 0x06;
		buff_send[1] = 0;
		if (this->canOpen.send(wMsgID, buff_send, 2))
		{
			usleep(10000);
			// check ready to switch on
			wMsgID = 0x600 + this->driver_id;
			buff_send[0] = 0x40; // read mode: 4 byte
			buff_send[1] = 0x41; // index: 0x6041
			buff_send[2] = 0x60;
			buff_send[3] = 0; // sub-index: 0
			
			buff_send[4] = 0;
			buff_send[5] = 0;
			buff_send[6] = 0;
			buff_send[7] = 0;
			if (this->canOpen.send(wMsgID, buff_send, 8))
			{
				// check read
				timeout_recv = 0;
				while (true)
				{
					if (canOpen.recv(buff_recv, &wMsgID))
					{
						if (wMsgID == (0x580 + this->driver_id))
						{
							if (buff_recv[4] == 0x31) // switch on
							{
								return 1;
							}
						}
					}
					usleep(1);
					timeout_recv++;
					if (timeout_recv > 10000)
					{
						break;
					}
				}
			}
		}
		timeout_send++;
		if (timeout_send > 20)
		{
			return 0;
		}
	}
	
	return 0;
}

bool MotorDriver::setTorqueMode(void)
{
	uint16_t wMsgID = 0;
	uint8_t buff_send[8] = { 0 };
	uint8_t buff_recv[8] = { 0 };
	uint16_t timeout_send = 0;
	uint32_t timeout_recv = 0;
	
	while (true)
	{
		wMsgID = 0x600 + this->driver_id;
		buff_send[0] = 0x22;
		buff_send[1] = 0x60; // index: 0x6060
		buff_send[2] = 0x60;
		buff_send[3] = 0;
		buff_send[4] = 0x04; // Torque mode
		buff_send[5] = 0;
		buff_send[6] = 0;
		buff_send[7] = 0;
		if (this->canOpen.send(wMsgID, buff_send, 8))
		{
			usleep(10000);
			// check Torque mode
			wMsgID = 0x600 + this->driver_id;
			buff_send[0] = 0x40; // read mode: 4 byte
			buff_send[1] = 0x61; // index: 0x6061
			buff_send[2] = 0x60;
			buff_send[3] = 0; // sub-index: 0
			
			buff_send[4] = 0;
			buff_send[5] = 0;
			buff_send[6] = 0;
			buff_send[7] = 0;
			if (this->canOpen.send(wMsgID, buff_send, 8))
			{
				// check read
				timeout_recv = 0;
				while (true)
				{
					if (canOpen.recv(buff_recv, &wMsgID))
					{
						if (wMsgID == (0x580 + this->driver_id))
						{
							if ((buff_recv[1] == 0x61) && (buff_recv[2] == 0x60) && (buff_recv[3] == 0) && (buff_recv[4] == 0x04)) // Torque Mode
							{
								return 1;
							}
						}
					}
					usleep(1);
					timeout_recv++;
					if (timeout_recv > 10000)
					{
						break;
					}
				}
			}
		}
		timeout_send++;
		if (timeout_send > 20)
		{
			return 0;
		}
	}
	
	return 0;
}

// set Enale Driver
bool MotorDriver::setEnable(void)
{
	uint16_t wMsgID = 0;
	uint8_t buff_send[8] = { 0 };
	uint8_t buff_recv[8] = { 0 };
	uint16_t timeout_send = 0;
	uint32_t timeout_recv = 0;
	
	while (true)
	{
		wMsgID = 0x200 + this->driver_id;
		buff_send[0] = 0x0F;
		buff_send[1] = 0;
		if (this->canOpen.send(wMsgID, buff_send, 2))
		{
			usleep(10000);
			// check Torque mode
			wMsgID = 0x600 + this->driver_id;
			buff_send[0] = 0x40; // read mode: 4 byte
			buff_send[1] = 0x41; // index: 0x6041
			buff_send[2] = 0x60;
			buff_send[3] = 0; // sub-index: 0
			
			buff_send[4] = 0;
			buff_send[5] = 0;
			buff_send[6] = 0;
			buff_send[7] = 0;
			if (this->canOpen.send(wMsgID, buff_send, 8))
			{
				// check read
				timeout_recv = 0;
				while (true)
				{
					if (canOpen.recv(buff_recv, &wMsgID))
					{
						if (wMsgID == (0x580 + this->driver_id))
						{
							if ((buff_recv[1] == 0x41) && (buff_recv[2] == 0x60) && (buff_recv[3] == 0) && (buff_recv[4] == 0x37)) 
							{
								return 1;
							}
						}
					}
					usleep(1);
					timeout_recv++;
					if (timeout_recv > 10000)
					{
						break;
					}
				}
			}
		}
		timeout_send++;
		if (timeout_send > 20)
		{
			return 0;
		}
	}
	
	return 0;	
}


//read to check error: IGBT
int MotorDriver::readIGBTError(void)
{
	uint16_t wMsgID = 0;
	uint8_t buff_send[8] = { 0 };
	uint8_t buff_recv[8] = { 0 };
	wMsgID = 0x600 + this->driver_id;
	uint32_t timeout = 0;
	
	buff_send[0] = 0x40; /* Read mode: 4 Bytes */
	buff_send[1] = 0x22; // index
	buff_send[2] = 0x30;
	buff_send[3] = 0x01; // sub-index
	
	buff_send[4] = 0;
	buff_send[5] = 0;
	buff_send[6] = 0;
	buff_send[7] = 0;
	if (canOpen.send(wMsgID, buff_send, 8))
	{
		// check read
		timeout = 0;
		while (true)
		{
			if (canOpen.recv(buff_recv, &wMsgID))
			{
				if (wMsgID == (0x580 + this->driver_id))
				{
					if ((buff_recv[4] == 0x80)&&(buff_recv[5] == 0x00)&&(buff_recv[6] == 0x00)&&(buff_recv[7] == 0x08))
					{
						return 1;
					}
				}
			}
			usleep(1);
			timeout++;
			if (timeout > 10000)
			{
				return 0;
			}
		}
	}
	return 0;
}

// check ready
int MotorDriver::checkReady(void)
{
	uint16_t wMsgID = 0;
	uint8_t buff_send[8] = { 0 };
	uint8_t buff_recv[8] = { 0 };
	uint32_t timeout = 0;
	
	// read to check
	wMsgID = 0x600 + this->driver_id;
	buff_send[0] = 0x40; /* Read mode: 4 Bytes */
	buff_send[1] = 0x40; // index
	buff_send[2] = 0x60;
	buff_send[3] = 0; // sub-index
	
	buff_send[4] = 0;
	buff_send[5] = 0;
	buff_send[6] = 0;
	buff_send[7] = 0;
	if (this->canOpen.send(wMsgID, buff_send, 8))
	{
		timeout = 0;
		while (true)
		{
			if (this->canOpen.recv(buff_recv, &wMsgID))
			{
				if (wMsgID == (0x580 + this->driver_id))
				{
					if ((buff_recv[4] == 0x0F)&&(buff_recv[5] == 0x00)&&(buff_recv[6] == 0x00)&&(buff_recv[7] == 0x00))
					{
						return 1;
					}
				}
			}
			usleep(1);
			timeout++;
			if (timeout > 10000)
			{
				return 0;
			}
		}
	}
}

// reset driver
int MotorDriver::resetDriver(void)
{
	uint16_t wMsgID = 0;
	uint8_t buff_send[8] = { 0 };
	uint8_t buff_recv[8] = { 0 };
	uint32_t timeout = 0;
	
	wMsgID = 0x600 + this->driver_id;
	buff_send[0] = 0x22; /* Write mode: 4 Bytes */
	buff_send[1] = 0x40; // index
	buff_send[2] = 0x60;
	buff_send[3] = 0; // sub-index
	
	buff_send[4] = 0x80;
	buff_send[5] = 0;
	buff_send[6] = 0;
	buff_send[7] = 0;
	
	if (this->canOpen.send(wMsgID, buff_send, 8))
	{
		usleep(100000);
		wMsgID = 0x600 + this->driver_id;
		buff_send[0] = 0x22; /* Write mode: 4 Bytes */
		buff_send[1] = 0x40; // index
		buff_send[2] = 0x60;
		buff_send[3] = 0; // sub-index
	
		buff_send[4] = 0x06;
		buff_send[5] = 0;
		buff_send[6] = 0;
		buff_send[7] = 0;
		if (this->canOpen.send(wMsgID, buff_send, 8))
		{
			usleep(100000);
			wMsgID = 0x600 + this->driver_id;
			buff_send[0] = 0x22; /* Write mode: 4 Bytes */
			buff_send[1] = 0x40; // index
			buff_send[2] = 0x60;
			buff_send[3] = 0; // sub-index
	
			buff_send[4] = 0x0F;
			buff_send[5] = 0;
			buff_send[6] = 0;
			buff_send[7] = 0;
			if (this->canOpen.send(wMsgID, buff_send, 8))
			{
				usleep(1000);
				// read to check
				if (this->checkReady())
				{
					return 1;
				}
			}
		}
	}
	return 0;
}


int MotorDriver::mapTorqueRPDO2(void)
{
	uint16_t wMsgID = 0;
	uint8_t buff_send[8] = { 0 };
	uint8_t buff_recv[8] = { 0 };
	wMsgID = 0x600 + this->driver_id;
	uint32_t timeout = 0;
	
	buff_send[0] = 0x22; /* Write mode: 4 Bytes */
	buff_send[1] = 0x01; // index
	buff_send[2] = 0x16;
	buff_send[3] = 0x01; // sub-index
	
	buff_send[4] = 0x10;
	buff_send[5] = 0x00;
	buff_send[6] = 0x71;
	buff_send[7] = 0x60;
	if (canOpen.send(wMsgID, buff_send, 8))
	{
		// check ACK
		timeout = 0;
		while (true)
		{
			if (canOpen.recv(buff_recv, &wMsgID))
			{
				if (wMsgID == (0x580 + driver_id))
				{
					if ((buff_recv[0] == 0x60)&&(buff_recv[1] == 0x01)&&(buff_recv[2] == 0x16)&&(buff_recv[3] == 0x01))
					{
						// read to check
						wMsgID = 0x600 + this->driver_id;
	
						buff_send[0] = 0x40; /* read mode: 4 Bytes */
						buff_send[1] = 0x01; // index
						buff_send[2] = 0x16;
						buff_send[3] = 0x01; // sub-index
	
						buff_send[4] = 0;
						buff_send[5] = 0;
						buff_send[6] = 0;
						buff_send[7] = 0;
						usleep(1000);
						if (canOpen.send(wMsgID, buff_send, 8))
						{
							// check read
							timeout = 0;
							while (true)
							{
								if (canOpen.recv(buff_recv, &wMsgID))
								{
									if (wMsgID == (0x580 + driver_id))
									{
										if ((buff_recv[0] == 0x43)&&(buff_recv[1] == 0x01)&&(buff_recv[2] == 0x16)&&(buff_recv[3] == 0x01))
										{
											if ((buff_recv[4] == 0x10)&&(buff_recv[5] == 0x00)&&(buff_recv[6] == 0x71)&&(buff_recv[7] == 0x60))
											{
												return 1;
											}
										}
									}
								}
								usleep(1);
								timeout++;
								if (timeout > 1000)
								{
									return 0;
								}
							}
						}
					}
				}
			}
			usleep(1);
			timeout++;
			if (timeout > 1000)
			{
				return 0;
			}
		}
	}
	return 0;
}

int MotorDriver::mapResolTDPO2(void)
{
	uint16_t wMsgID = 0;
	uint8_t buff_send[8] = { 0 };
	uint8_t buff_recv[8] = { 0 };
	wMsgID = 0x600 + this->driver_id;
	uint32_t timeout = 0;
	
	buff_send[0] = 0x22; /* Write mode: 4 Bytes */
	buff_send[1] = 0x01; // index
	buff_send[2] = 0x1A;
	buff_send[3] = 0x01; // sub-index
	
	buff_send[4] = 0x20;
	buff_send[5] = 0x00;
	buff_send[6] = 0x09;
	buff_send[7] = 0x31;
	usleep(1000);
	if (canOpen.send(wMsgID, buff_send, 8))
	{
		// check ACK
		timeout = 0;
		while (true)
		{
			if (canOpen.recv(buff_recv, &wMsgID))
			{
				if (wMsgID == (0x580 + driver_id))
				{
					if ((buff_recv[0] == 0x60)&&(buff_recv[1] == 0x01)&&(buff_recv[2] == 0x1A)&&(buff_recv[3] == 0x01))
					{
						// read to check
						wMsgID = 0x600 + this->driver_id;
	
						buff_send[0] = 0x40; /* read mode: 4 Bytes */
						buff_send[1] = 0x01; // index
						buff_send[2] = 0x1A;
						buff_send[3] = 0x01; // sub-index
	
						buff_send[4] = 0;
						buff_send[5] = 0;
						buff_send[6] = 0;
						buff_send[7] = 0;
						usleep(1000);
						if (canOpen.send(wMsgID, buff_send, 8))
						{
							// check read
							timeout = 0;
							while (true)
							{
								if (canOpen.recv(buff_recv, &wMsgID))
								{
									if (wMsgID == (0x580 + driver_id))
									{
										if ((buff_recv[0] == 0x43)&&(buff_recv[1] == 0x01)&&(buff_recv[2] == 0x1A)&&(buff_recv[3] == 0x01))
										{
											if ((buff_recv[4] == 0x20)&&(buff_recv[5] == 0x00)&&(buff_recv[6] == 0x09)&&(buff_recv[7] == 0x31))
											{
												return 1;
											}
										}
									}
								}
								usleep(1);
								timeout++;
								if (timeout > 1000)
								{
									return 0;
								}
							}
						}
					}
				}
			}
			usleep(1);
			timeout++;
			if (timeout > 1000)
			{
				return 0;
			}
		}
	}
	return 0;
}

int MotorDriver::mapVelocityTDPO3(void)
{
	uint16_t wMsgID = 0;
	uint8_t buff_send[8] = { 0 };
	uint8_t buff_recv[8] = { 0 };
	wMsgID = 0x600 + this->driver_id;
	uint32_t timeout = 0;
	
	buff_send[0] = 0x22; /* Write mode: 4 Bytes */
	buff_send[1] = 0x02; // index
	buff_send[2] = 0x1A;
	buff_send[3] = 0x01; // sub-index
	
	buff_send[4] = 0x20;
	buff_send[5] = 0x00;
	buff_send[6] = 0x0A;
	buff_send[7] = 0x31;
	usleep(1000);
	if (canOpen.send(wMsgID, buff_send, 8))
	{
		// check ACK
		timeout = 0;
		while (true)
		{
			if (canOpen.recv(buff_recv, &wMsgID))
			{
				if (wMsgID == (0x580 + driver_id))
				{
					if ((buff_recv[0] == 0x60)&&(buff_recv[1] == 0x02)&&(buff_recv[2] == 0x1A)&&(buff_recv[3] == 0x01))
					{
						// read to check
						wMsgID = 0x600 + this->driver_id;
	
						buff_send[0] = 0x40; /* read mode: 4 Bytes */
						buff_send[1] = 0x02; // index
						buff_send[2] = 0x1A;
						buff_send[3] = 0x01; // sub-index
	
						buff_send[4] = 0;
						buff_send[5] = 0;
						buff_send[6] = 0;
						buff_send[7] = 0;
						usleep(1000);
						if (canOpen.send(wMsgID, buff_send, 8))
						{
							// check read
							timeout = 0;
							while (true)
							{
								if (canOpen.recv(buff_recv, &wMsgID))
								{
									if (wMsgID == (0x580 + driver_id))
									{
										if ((buff_recv[0] == 0x43)&&(buff_recv[1] == 0x02)&&(buff_recv[2] == 0x1A)&&(buff_recv[3] == 0x01))
										{
											if ((buff_recv[4] == 0x20)&&(buff_recv[5] == 0x00)&&(buff_recv[6] == 0x0A)&&(buff_recv[7] == 0x31))
											{
												return 1;
											}
										}
									}
								}
								usleep(1);
								timeout++;
								if (timeout > 1000)
								{
									return 0;
								}
							}
						}
					}
				}
			}
			usleep(1);
			timeout++;
			if (timeout > 1000)
			{
				return 0;
			}
		}
	}
	return 0;
}

int MotorDriver::setResolTime(void)
{
	uint16_t wMsgID = 0;
	uint8_t buff_send[8] = { 0 };
	uint8_t buff_recv[8] = { 0 };
	wMsgID = 0x600 + this->driver_id;
	uint32_t timeout = 0;
	
	buff_send[0] = 0x22; /* Write mode: 4 Bytes */
	buff_send[1] = 0x01; // index
	buff_send[2] = 0x18;
	buff_send[3] = 0x05; // sub-index
	
	buff_send[4] = 0x01;
	buff_send[5] = 0;
	buff_send[6] = 0;
	buff_send[7] = 0;
	usleep(1000);
	if (canOpen.send(wMsgID, buff_send, 8))
	{
		// check ACK
		timeout = 0;
		while (true)
		{
			if (canOpen.recv(buff_recv, &wMsgID))
			{
				if (wMsgID == (0x580 + driver_id))
				{
					if ((buff_recv[0] == 0x60)&&(buff_recv[1] == 0x01)&&(buff_recv[2] == 0x18)&&(buff_recv[3] == 0x05))
					{
						// read to check
						wMsgID = 0x600 + this->driver_id;
	
						buff_send[0] = 0x40; /* read mode: 4 Bytes */
						buff_send[1] = 0x01; // index
						buff_send[2] = 0x18;
						buff_send[3] = 0x05; // sub-index
	
						buff_send[4] = 0;
						buff_send[5] = 0;
						buff_send[6] = 0;
						buff_send[7] = 0;
						usleep(1000);
						if (canOpen.send(wMsgID, buff_send, 8))
						{
							// check read
							timeout = 0;
							while (true)
							{
								if (canOpen.recv(buff_recv, &wMsgID))
								{
									if (wMsgID == (0x580 + driver_id))
									{
										if ((buff_recv[1] == 0x01)&&(buff_recv[2] == 0x18)&&(buff_recv[3] == 0x05))
										{
											if ((buff_recv[4] == 0x01)&&(buff_recv[5] == 0)&&(buff_recv[6] == 0)&&(buff_recv[7] == 0))
											{
												this->logger->info("Set resol Time: success");
												return 1;
											}
										}
									}
								}
								usleep(1);
								timeout++;
								if (timeout > 1000)
								{
									this->logger->error("setResolTime: read to check - timeout");
									return 0;
								}
							}
						}
					}
				}
			}
			usleep(1);
			timeout++;
			if (timeout > 1000)
			{
				this->logger->error("setResolTime: check ACK - timeout");
				return 0;
			}
		}
	}
	return 0;
}

int MotorDriver::setVelocityTime(void)
{
	uint16_t wMsgID = 0;
	uint8_t buff_send[8] = { 0 };
	uint8_t buff_recv[8] = { 0 };
	wMsgID = 0x600 + this->driver_id;
	uint32_t timeout = 0;
	
	buff_send[0] = 0x22; /* Write mode: 4 Bytes */
	buff_send[1] = 0x02; // index
	buff_send[2] = 0x18;
	buff_send[3] = 0x05; // sub-index
	
	buff_send[4] = 0x01;
	buff_send[5] = 0;
	buff_send[6] = 0;
	buff_send[7] = 0;
	usleep(1000);
	if (canOpen.send(wMsgID, buff_send, 8))
	{
		// check ACK
		timeout = 0;
		while (true)
		{
			if (canOpen.recv(buff_recv, &wMsgID))
			{
				if (wMsgID == (0x580 + driver_id))
				{
					if ((buff_recv[0] == 0x60)&&(buff_recv[1] == 0x02)&&(buff_recv[2] == 0x18)&&(buff_recv[3] == 0x05))
					{
						// read to check
						wMsgID = 0x600 + this->driver_id;
	
						buff_send[0] = 0x40; /* read mode: 4 Bytes */
						buff_send[1] = 0x02; // index
						buff_send[2] = 0x18;
						buff_send[3] = 0x05; // sub-index
	
						buff_send[4] = 0;
						buff_send[5] = 0;
						buff_send[6] = 0;
						buff_send[7] = 0;
						usleep(1000);
						if (canOpen.send(wMsgID, buff_send, 8))
						{
							// check read
							timeout = 0;
							while (true)
							{
								if (canOpen.recv(buff_recv, &wMsgID))
								{
									if (wMsgID == (0x580 + driver_id))
									{
										if ((buff_recv[1] == 0x02)&&(buff_recv[2] == 0x18)&&(buff_recv[3] == 0x05))
										{
											if ((buff_recv[4] == 0x01)&&(buff_recv[5] == 0)&&(buff_recv[6] == 0)&&(buff_recv[7] == 0))
											{
												this->logger->info("set Velocity time: success");
												return 1;
											}
										}
									}
								}
								usleep(1);
								timeout++;
								if (timeout > 1000)
								{
									this->logger->error("setVelocityTime : read to check - timeout");
									return 0;
								}
							}
						}
					}
				}
			}
			usleep(1);
			timeout++;
			if (timeout > 1000)
			{
				this->logger->error("setVelocityTime : check ACK - timeout");
				return 0;
			}
		}
	}
	return 0;
}

int MotorDriver::activeResol(void)
{
	uint16_t wMsgID = 0;
	uint8_t buff_send[8] = { 0 };
	uint8_t buff_recv[8] = { 0 };
	wMsgID = 0x600 + this->driver_id;
	uint32_t timeout = 0;
	
	buff_send[0] = 0x22; /* Write mode: 4 Bytes */
	buff_send[1] = 0x01; // index
	buff_send[2] = 0x18;
	buff_send[3] = 0x02; // sub-index
	
	buff_send[4] = 0xFF;
	buff_send[5] = 0;
	buff_send[6] = 0;
	buff_send[7] = 0;
	usleep(1000);
	if (canOpen.send(wMsgID, buff_send, 8))
	{
		// check ACK
		timeout = 0;
		while (true)
		{
			if (canOpen.recv(buff_recv, &wMsgID))
			{
				if (wMsgID == (0x580 + driver_id))
				{
					if ((buff_recv[0] == 0x60)&&(buff_recv[1] == 0x01)&&(buff_recv[2] == 0x18)&&(buff_recv[3] == 0x02))
					{
						// read to check
						wMsgID = 0x600 + this->driver_id;
	
						buff_send[0] = 0x40; /* read mode: 4 Bytes */
						buff_send[1] = 0x01; // index
						buff_send[2] = 0x18;
						buff_send[3] = 0x02; // sub-index
	
						buff_send[4] = 0;
						buff_send[5] = 0;
						buff_send[6] = 0;
						buff_send[7] = 0;
						usleep(1000);
						if (canOpen.send(wMsgID, buff_send, 8))
						{
							// check read
							timeout = 0;
							while (true)
							{
								if (canOpen.recv(buff_recv, &wMsgID))
								{
									if (wMsgID == (0x580 + driver_id))
									{
										if ((buff_recv[1] == 0x01)&&(buff_recv[2] == 0x18)&&(buff_recv[3] == 0x02))
										{
											if ((buff_recv[4] == 0xFF)&&(buff_recv[5] == 0)&&(buff_recv[6] == 0)&&(buff_recv[7] == 0))
											{
												this->logger->info("Active Resol: Success");
												return 1;
											}
										}
									}
								}
								usleep(1);
								timeout++;
								if (timeout > 1000)
								{
									this->logger->error("activeResol : read to check - timeout");
									return 0;
								}
							}
						}
					}
				}
			}
			usleep(1);
			timeout++;
			if (timeout > 100000)
			{
				this->logger->error("activeResol : check ACK - timeout");
				return 0;
			}
		}
	}
	return 0;
}

int MotorDriver::activeVelocity(void)
{
	uint16_t wMsgID = 0;
	uint8_t buff_send[8] = { 0 };
	uint8_t buff_recv[8] = { 0 };
	wMsgID = 0x600 + this->driver_id;
	uint32_t timeout = 0;
	
	buff_send[0] = 0x22; /* Write mode: 4 Bytes */
	buff_send[1] = 0x02; // index
	buff_send[2] = 0x18;
	buff_send[3] = 0x02; // sub-index
	
	buff_send[4] = 0xFF;
	buff_send[5] = 0;
	buff_send[6] = 0;
	buff_send[7] = 0;
	usleep(1000);
	if (canOpen.send(wMsgID, buff_send, 8))
	{
		// check ACK
		timeout = 0;
		while (true)
		{
			if (canOpen.recv(buff_recv, &wMsgID))
			{
				if (wMsgID == (0x580 + driver_id))
				{
					if ((buff_recv[0] == 0x60)&&(buff_recv[1] == 0x02)&&(buff_recv[2] == 0x18)&&(buff_recv[3] == 0x02))
					{
						// read to check
						wMsgID = 0x600 + this->driver_id;
	
						buff_send[0] = 0x40; /* read mode: 4 Bytes */
						buff_send[1] = 0x02; // index
						buff_send[2] = 0x18;
						buff_send[3] = 0x02; // sub-index
	
						buff_send[4] = 0;
						buff_send[5] = 0;
						buff_send[6] = 0;
						buff_send[7] = 0;
						usleep(1000);
						if (canOpen.send(wMsgID, buff_send, 8))
						{
							// check read
							timeout = 0;
							while (true)
							{
								if (canOpen.recv(buff_recv, &wMsgID))
								{
									if (wMsgID == (0x580 + driver_id))
									{
										if ((buff_recv[1] == 0x02)&&(buff_recv[2] == 0x18)&&(buff_recv[3] == 0x02))
										{
											if ((buff_recv[4] == 0xFF)&&(buff_recv[5] == 0)&&(buff_recv[6] == 0)&&(buff_recv[7] == 0))
											{
												this->logger->info("active velocity: succe");
												return 1;
											}
										}
									}
								}
								usleep(1);
								timeout++;
								if (timeout > 1000)
								{
									this->logger->error("activeVelocity : read to check - timeout");
									return 0;
								}
							}
						}
					}
				}
			}
			usleep(1);
			timeout++;
			if (timeout > 100000)
			{
				this->logger->error("activeVelocity : check ACK - timeout");
				return 0;
			}
		}
	}
	return 0;
}


int MotorDriver::MotorUpdateNetState(void)
{
	uint16_t wMsgID = 0x00;
	return 1;
}


int MotorDriver::setTargetTorque(int16_t value)
{
	uint16_t wMsgID = 0x00;
	wMsgID = 0x300 + driver_id;
	
	uint8_t buff_send[8];

	memset(buff_send, 0, 8);

	buff_send[0] = value;
	buff_send[1] = value >> 8;
	buff_send[2] = 0;
	buff_send[3] = 0;

	if (canOpen.send(wMsgID, buff_send, 8))
	{
		return 1;
	}
	return 0;
	this->driverData.state = DRIVER_NOT_READY;
}

int MotorDriver::setInputModeTorque(uint32_t value)
{
	uint16_t wMsgID = 0x00;
	wMsgID = 0x600 + driver_id;
	
	uint8_t buff_send[8];	

	memset(buff_send, 0, 8);

	buff_send[0] = 0x22; /* Data with length <- 4 Bytes */
	buff_send[1] = 0x71; // index
	buff_send[2] = 0x30;
	buff_send[3] = 0x00;


	buff_send[4] = value;
	buff_send[5] = value >> 8;
	buff_send[6] = value >> 16;
	buff_send[7] = value >> 24;

	if (canOpen.send(wMsgID, buff_send, 8))
	{
		return 1;
	}
	return 0;	
}

int MotorDriver::setSlopeTorque(uint32_t value)
{
	uint16_t wMsgID = 0x00;
	wMsgID = 0x600 + driver_id;
	
	uint8_t buff_send[8];	

	memset(buff_send, 0, 8);

	buff_send[0] = 0x22; /* Data with length <- 4 Bytes */
	buff_send[1] = 0x87; // index
	buff_send[2] = 0x60;
	buff_send[3] = 0x00;


	buff_send[4] = value;
	buff_send[5] = value >> 8;
	buff_send[6] = value >> 16;
	buff_send[7] = value >> 24;

	if (canOpen.send(wMsgID, buff_send, 8))
	{
		return 1;
	}
	return 0;	
}

int MotorDriver::setProfileTypeTorque(int16_t value)
{
	uint16_t wMsgID = 0x00;
	wMsgID = 0x600 + driver_id;
	
	uint8_t buff_send[8];	

	memset(buff_send, 0, 8);

	buff_send[0] = 0x22; /* Data with length <- 4 Bytes */
	buff_send[1] = 0x88; // index
	buff_send[2] = 0x60;
	buff_send[3] = 0x00;


	buff_send[4] = value;
	buff_send[5] = value >> 8;
	buff_send[6] = 0;
	buff_send[7] = 0;

	if (canOpen.send(wMsgID, buff_send, 8))
	{
		return 1;
	}
	return 0;	
}

int MotorDriver::MotorSetProfileVelocity(uint32_t Speed) 
{
	uint16_t wMsgID = 0x00;
	wMsgID = 0x600 + driver_id;
	
	uint8_t buff_send[8];	

	memset(buff_send, 0, 8);

	buff_send[0] = 0x22; /* Data with length <- 4 Bytes */
	buff_send[1] = 0x81;
	buff_send[2] = 0x60;
	buff_send[3] = 0x00;


	buff_send[4] = Speed;
	buff_send[5] = Speed >> 8;
	buff_send[6] = Speed >> 16;
	buff_send[7] = Speed >> 24;

	if (canOpen.send(wMsgID, buff_send, 8))
	{
		return 1;
	}
	return 0;

}

int MotorDriver::SetTarPos(int32_t position)
{
	// set tartget position
	uint16_t wMsgID = 0x00;
	wMsgID = 0x600 + driver_id;
	
	uint8_t buff_send[8];
	
	memset(buff_send, 0, 8);
	buff_send[0] = 0x22; /* Data with length <- 4 Bytes */
	buff_send[1] = 0x7A;
	buff_send[2] = 0x60;
	buff_send[3] = 0x00;
	
	buff_send[4] = (position << 24) >> 24;
	buff_send[5] = (position << 16) >> 24;
	buff_send[6] = (position << 8) >> 24;
	buff_send[7] =  position >> 24;
	
	if (canOpen.send(wMsgID, buff_send, 8))
	{
		return 1;
	}
	return 0;
}

int MotorDriver::SetDevState(DeviceState state)
{
	uint16_t wMsgID = 0x00;
	uint8_t buff_send[2];
	
	memset(buff_send, 0, 2);
	wMsgID = 0x200 + driver_id;
	
	switch (state) 
	{
	case State_Disable:
		buff_send[0] = 0x00;    //0=Disable; 6=ready; F=Operation
		break;
	case State_ReadySwitchOn:
		buff_send[0] = 0x06;
		break;
	case State_Operation:
		buff_send[0] = 0x0F;
		break;
	default:
		return 0;
		break;
	}
	buff_send[1] = 0;
	if (canOpen.send(wMsgID, buff_send, 2))
	{
		return 1;
	}
	return 0;
}

int MotorDriver::StartMovPos(void)
{
	uint16_t wMsgID = 0x00;
	uint8_t buff_send[8];
	
	memset(buff_send, 0, 8);
	wMsgID = 0x200 + driver_id;
	
	//buff_send[0] = 0x7F; // tuong doi
	buff_send[0] = 0x3F; // tuyet doi
	buff_send[1] = 0x00;
	
	if (canOpen.send(wMsgID, buff_send, 2))
	{
		return 1;
	}
	return 0;
}



int MotorDriver::SetRefInterpolPos(int32_t RefPosition)
{
	uint16_t wMsgID = 0x00;
	uint8_t buff_send[8];
	
	memset(buff_send, 0, 8);
	wMsgID = 0x600 + driver_id;
	
	buff_send[0] = 0x22;
	buff_send[1] = 0xC1;
	buff_send[2] = 0x60;
	buff_send[3] = 0x01;
	
	buff_send[4] = (RefPosition << 24) >> 24;
	buff_send[5] = (RefPosition << 16) >> 24;
	buff_send[6] = (RefPosition << 8) >> 24;
	buff_send[7] = RefPosition >> 24;
	
	
	if (canOpen.send(wMsgID, buff_send, 8))
	{
		return 1;
	}
	
	return 0;
}

int MotorDriver::EnInterpolPos(void)
{
	uint16_t wMsgID = 0x00;
	uint8_t buff_send[8];
	
	memset(buff_send, 0, 8);
	wMsgID = 0x200 + driver_id;
	
	buff_send[0] = 0x1F; // 0x5F: change_set_immediately = 0, 0x7F = 1
	buff_send[1] = 0x00;
	
	if (canOpen.send(wMsgID, buff_send, 2))
	{
		return 1;
	}
	return 0;
}


int MotorDriver::ReqResolPos(void)
{
	uint16_t wMsgID = 0x00;
	uint8_t buff_send[8];
	uint8_t buff_recv[8];
	int32_t data_get = 0;
	uint32_t timeout = 0;
	
	while (true)
	{
		memset(buff_send, 0, 8);
		memset(buff_recv, 0, 8);
		wMsgID = 0x600 + driver_id;
	
		buff_send[0] = 0x40;
		buff_send[1] = 0x09;
		buff_send[2] = 0x31;
		buff_send[3] = 0x00;
		
	
		if (canOpen.send(wMsgID, buff_send, 8))
		{
			//return 1;
		}
		
		usleep(1000);
	}

	return 0;		
	
}

int32_t MotorDriver::recvData(void)
{
	uint16_t wMsgID = 0x00;
	uint8_t buff_recv[8];
		
	this->driverData.flag_get_data = 0;
	this->driverData.resol_pos = 0;
	this->driverData.resol_vel = 0;
	// read data
	while(true)
	{
		memset(buff_recv, 0, 8);
		if (canOpen.recv(buff_recv, &wMsgID))
		{
			if (wMsgID == (0x280 + driver_id))
			{
				wMsgID = 0;
				while (this->driverData.flag_get_data)
				{
					usleep(1);
				}
				this->driverData.flag_get_data = 1;
				this->driverData.resol_pos = (int32_t)buff_recv[0] | ((int32_t)buff_recv[1] << 8) | ((int32_t)buff_recv[2] << 16) | ((int32_t)buff_recv[3] << 24);
				this->driverData.flag_get_data = 0;
			}
			if (wMsgID == (0x380 + driver_id))
			{
				wMsgID = 0;
				while (this->driverData.flag_get_data)
				{
					usleep(1);
				}
				this->driverData.flag_get_data = 1;
				this->driverData.resol_vel = (int32_t)buff_recv[0] | ((int32_t)buff_recv[1] << 8) | ((int32_t)buff_recv[2] << 16) | ((int32_t)buff_recv[3] << 24);
				this->driverData.flag_get_data = 0;
			}
		}
		if(this->driverData.state == DRIVER_NOT_READY){
			break;
		}
		usleep(50);
	}

	return -1;	
}


DriverInfor MotorDriver::getData(void)
{
	DriverInfor data;
	while (this->driverData.flag_get_data)
	{
		usleep(1);
	}
	this->driverData.flag_get_data = 1;
	data = this->driverData;
	this->driverData.flag_get_data = 0;
	return data;
}