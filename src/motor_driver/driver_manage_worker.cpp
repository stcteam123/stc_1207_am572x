/**
******************************************************************************
* @file           : driver_manage_worker.cpp
* @brief          : source file of worker for driver management
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/

/* Include ------------------------------------------------------------------*/
#include "motor_driver/driver_manage_worker.h"
#include <thread>

/* Object -------------------------------------------------------------------------------------------------------------*/
DriverManageWorker driverManageWorker;


DriverManageWorker::DriverManageWorker(){
    this->logger = spdlog::basic_logger_mt("driver_manage_worker_log", "/home/1207/logMonitor/motor_driver/driver_manage_worker_log.txt");
    this->ELDriver.initComm(EL_DRIVER);
    this->AZDriver.initComm(AZ_DRIVER);
}

DriverManageWorker::~DriverManageWorker(){
    
}


void DriverManageWorker::run(void){
    this->logger->info("Starting Worker for Driver Management ...OK!");
    myLogger.sysLogger->info("Starting Worker for Driver Management ...OK!");
    while (1)
    {
        while (1){
            DriverInfor EL_infor, AZ_infor;
            EL_infor = this->ELDriver.getData();
            AZ_infor = this->AZDriver.getData();
            
            if(EL_infor.state == DRIVER_NOT_READY){
                this->logger->info("Init EL Driver...");
                myLogger.sysLogger->info("Init EL Driver...");
                if(this->ELDriver.initDriver()){
                    this->logger->info("Init EL Driver: Success");
                    myLogger.sysLogger->info("Init EL Driver: Success");
                }
                else{
                    this->logger->info("Init EL Driver: False");
                    myLogger.sysLogger->info("Init EL Driver: Flase");
                }
            }

            // if(AZ_infor.state == DRIVER_NOT_READY){
            //     this->logger->info("Init AZ Driver...");
            //     myLogger.sysLogger->info("Init AZ Driver...");
            //     if(this->AZDriver.initDriver()){
            //         this->logger->info("Init AZ Driver: Success");
            //         myLogger.sysLogger->info("Init AZ Driver: Success");
            //     }
            //     else{
            //         this->logger->info("Init AZ Driver: False");
            //         myLogger.sysLogger->info("Init AZ Driver: Flase");
            //     }
            // }
            AZ_infor.state = DRIVER_READY;
            if((EL_infor.state == DRIVER_READY) && (AZ_infor.state == DRIVER_READY)){
                break;
            }
            sleep(1);
        }

        this->logger->info("Starting ELDriverRecvThread ...OK!");
        myLogger.sysLogger->info("Starting ELDriverRecvThread ...OK!");
        // this->logger->info("Starting AZDriverRecvThread ...OK!");
        // myLogger.sysLogger->info("Starting AZDriverRecvThread ...OK!");        
        std::thread ELDriverRecvThread(&MotorDriver::recvData, &this->ELDriver);
        // std::thread AZDriverRecvThread(&MotorDriver::recvData, &this->AZDriver);

        ELDriverRecvThread.join();
        this->logger->info("Stopped ELDriverRecvThread ...OK!");
        myLogger.sysLogger->info("Stopped ELDriverRecvThread ...OK!");
        // AZDriverRecvThread.join();
        this->logger->info("Stopped AZDriverRecvThread ...OK!");
        myLogger.sysLogger->info("Stopped AZDriverRecvThread ...OK!");      
    }
}