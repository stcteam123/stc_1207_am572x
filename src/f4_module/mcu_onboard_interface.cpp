/**
******************************************************************************
* @file           : mcu_onboard_interface.cpp
* @brief          : source file of MCU On board Interface
* @author		  : Tuan_NT
******************************************************************************
******************************************************************************
*/
/* Include -----------------------------------------------------------------*/
#include "f4_module/mcu_onboard_interface.h"
#include <unistd.h>
#include <thread>
#include <iostream>


/* Method ------------------------------------------------------------------*/

MCUInterface::MCUInterface()
{
	this->logger = spdlog::basic_logger_mt("mcu_onboard_interface_logger", "/home/root/app/logMonitor/mcu_onboard_interface_logger.txt");
	this->mcu_data.encEL_status = false;
	this->mcu_data.encAZ_status = false;
}

MCUInterface::~MCUInterface()
{
}

void MCUInterface::run(void)
{
	spdlog::info("Start Thread: MCU Interface");
	this->logger->info("Start Thread: MCU Interface");
	std::thread recvDataMCUThread(&MCUInterface::recvData, this);
	while (true)
	{
		sleep(1);
	}
}

MCUData MCUInterface::getData(void)
{
	MCUData get_data;
	get_data = this->mcu_data;
	return get_data;
}



void MCUInterface::recvData(void)
{
	spdlog::info("Start Thread: Receiver MCU Data");
	this->logger->info("Start Thread: Receiver MCU Data");
	while (!this->mcu_port.init(UART8, 115200))
	{
		sleep(1);
		spdlog::info("open serial port for mcu interface: False");
		this->logger->info("open serial port for mcu interface: False");
	}
	spdlog::info("open serial port for mcu interface: success");
	this->logger->info("open serial port for mcu interface: success");
	
	char buff_recv[100];
	uint8_t len = 0;
	memset(buff_recv, NULL, 100);
	while (true)
	{
		if (this->mcu_port.getBuffAvailable() > 0)
		{
			uint8_t data;
			if (this->mcu_port.read(&data, 1) > 0)
			{
				if (data == 'S')
				{
					buff_recv[len] = data;
					len++;
					while (true)
					{
						if (this->mcu_port.getBuffAvailable() > 0)
						{
							if (this->mcu_port.read(&data, 1) > 0)
							{
								buff_recv[len] = data;
								len++;
								if (len >= 100)
								{
									len = 0;
								}
								if (data == 'E')
								{
									//std::cout << "get mcu data" << std::endl;
									while (true)
									{
										if (this->mcu_port.read(&data, 1) > 0)
										{
											buff_recv[len] = data;
											len++;
											break;
										}
										usleep(100);
									}
									if (buff_recv[0] == 'S')
									{
										// check crc
										uint8_t crc = 0xFF;
										for (uint8_t i = 0; i < len - 1; i++)
										{
											crc ^= buff_recv[i];
										}
										if (crc == buff_recv[len - 1])
										{
											std::string data_recv = buff_recv;
											size_t pos = 0;
											std::string data_split[10];
											uint8_t i = 0;
											while ((pos = data_recv.find(":")) != std::string::npos) {
												data_split[i] = data_recv.substr(0, pos);
												data_recv.erase(0, pos + 1);
												i++;
												if (i == 5) {
													break;
												}
											}
											//std::cout << "get mcu data: success" << data_split[2] << "   " << data_split[4] << std::endl;
											//spdlog::info("Get mcu data: success");
											this->logger->info("Get mcu data:success");
											this->mcu_data.encEL_status = std::stoi(data_split[1]);
											this->mcu_data.encEL_value = std::stof(data_split[2]);
											this->mcu_data.encAZ_status = std::stoi(data_split[3]);
											this->mcu_data.encAZ_value = std::stof(data_split[4]);
											//std::cout << "debug: AZ= " << data_split[4] << "  EL= " << data_split[2] << std::endl;
											//std::cout << "debug: AZ= " << this->mcu_data.encEL_value << "  EL= " << this->mcu_data.encAZ_value << std::endl;
										}
										else
										{
											//std::cout << "get mcu data: check crc false " << std::endl;
											spdlog::info("get mcu data: check crc false");
											this->logger->info("get mcu data: check crc false");
										}
									}

									len = 0;
									break;
								}
							}
						}
						usleep(1000);
					}
				}
			}
		}
		usleep(1000);
	}
}