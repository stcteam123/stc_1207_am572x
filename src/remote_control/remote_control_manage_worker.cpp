/**
******************************************************************************
* @file           : remote_control_mange_worker.cpp
* @brief          : Source file of worker for remote control management
* @author 		  : Tuan_NT
******************************************************************************
******************************************************************************
*/
/* Include -------------------------------------------------------------------------*/
#include "remote_control/remote_control_manage_worker.h"
#include "logger/sys_logger.h"
#include <thread>

/* Object --------------------------------------------------------------------------*/
RemoteControlManageWorker remoteControlMangeWorker;


RemoteControlManageWorker::RemoteControlManageWorker()
{
}

RemoteControlManageWorker::~RemoteControlManageWorker()
{
}

void RemoteControlManageWorker::run(void){
    myLogger.sysLogger->info("Starting RemoteControlManageWorker ....OK!");
    myLogger.sysLogger->info("Starting RemoteControlRecvThread ....OK!");
    std::thread RemoteControlRecvThread(&RemoteControl::recvData, &this->remoteControl);
    RemoteControlRecvThread.join();
    myLogger.sysLogger->warn("Stopped RemoteControlRecvThread!");
}
