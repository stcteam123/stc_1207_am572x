/**
******************************************************************************
* @file           : joystick.cpp
* @brief          : source file of Joystick interface
******************************************************************************
******************************************************************************
*/
#include "remote_control/remote_control.h"
#include <thread>


KeyBoardInfor::KeyBoardInfor()
{
	stb_on_off = INACTIVE;
	auto_tracking = INACTIVE;
	go_home = INACTIVE;
	
	fire_01 = INACTIVE;
	fire_02 = INACTIVE;
	fire_03 = INACTIVE;
	
	arm = INACTIVE;
	offset_up = INACTIVE;
	offset_down = INACTIVE;
	offset_left = INACTIVE;
	offset_right = INACTIVE;
	focus_on_off = INACTIVE;
	zoom_in = INACTIVE;
	zoom_out = INACTIVE;
	switch_cam = INACTIVE;
	lrf = INACTIVE;	
}

KeyBoardInfor::~KeyBoardInfor()
{
}


JoystickInfor::JoystickInfor()
{
	this->x_axit = 0;
	this->y_axit = 0;
	this->joy_state = INACTIVE;
}

JoystickInfor::~JoystickInfor()
{
}



RemoteControl::RemoteControl()
{
	this->myCanBus.init(CAN0_INTERFACE);
	this->myID = JOYSTICK_ID;
	this->time_check_status = 0;	
	this->logger = spdlog::basic_logger_mt("RemoteControl_Logger", "/home/1207/logMonitor/remote_control/RemoteControl_Logger.txt");
}

RemoteControl::~RemoteControl()
{
}

int RemoteControl::recvData(void)
{
	uint8_t buff_recv[8] = { 0 };
	uint16_t device_id = 0;
	this->logger->info("Starting RecvDataThread for RemotControl...OK!");
	std::thread checkJoystickThread(&RemoteControl::checkJoyStatus, this);
	while (true)
	{
		if (this->myCanBus.recv(buff_recv, &device_id))
		{
			if (device_id == JOYSTICK_ID)
			{
				this->time_check_status = 0;
				this->joystick.joy_state = JoystickInfor::ACTIVE;
				device_id = 0;
				this->joystick.x_axit = ((int16_t)buff_recv[2] << 8) | (int16_t)buff_recv[3];
				this->joystick.x_axit = ((int16_t)buff_recv[6] << 8) | (int16_t)buff_recv[7];
			}
			else if (device_id == KEYBOARD_ID)
			{
				if ((buff_recv[0] == 0) && (buff_recv[1] == 1) && (buff_recv[2] == 1) && (buff_recv[3] == 0))
				{
					uint32_t value = 0;
					value = ((uint32_t)buff_recv[4] << 24) | ((uint32_t)buff_recv[5] << 16) | ((uint32_t)buff_recv[6] << 8) | (uint32_t)buff_recv[7];
					this->logger->info("KEBOARD: {0}", std::to_string(value));
					switch (value)
					{
					case 1:
						{
							this->keyboard.fire_01 = KeyBoardInfor::INACTIVE;
							break;
						}
					case 2:
						{
							this->keyboard.fire_01 = KeyBoardInfor::ACTIVE;
							break;
						}
					case 4:
						{
							this->keyboard.fire_02 = KeyBoardInfor::INACTIVE;
							break;
						}
					case 8:
						{
							this->keyboard.fire_02 = KeyBoardInfor::ACTIVE;
							break;
						}
					case 16:
						{
							this->keyboard.fire_03 = KeyBoardInfor::INACTIVE;
							break;
						}
					case 32:
						{
							this->keyboard.fire_03 = KeyBoardInfor::ACTIVE;
							break;
						}
					case 64:
						{
							this->keyboard.arm = KeyBoardInfor::INACTIVE;
							break;
						}
					case 128:
						{
							this->keyboard.arm = KeyBoardInfor::ACTIVE;
							break;
						}
					case 256:
						{
							this->keyboard.offset_down = KeyBoardInfor::INACTIVE;
							break;
						}
					case 512:
						{
							this->keyboard.offset_down = KeyBoardInfor::ACTIVE;
							break;
						}
					case 1024:
						{
							this->keyboard.offset_right = KeyBoardInfor::INACTIVE;
							break;
						}
					case 2048:
						{
							this->keyboard.offset_right = KeyBoardInfor::ACTIVE;
							break;
						}
					case 4096:
						{
							this->keyboard.offset_left = KeyBoardInfor::INACTIVE;
							break;
						}
					case 8192:
						{
							this->keyboard.offset_left = KeyBoardInfor::ACTIVE;
							break;
						}
					case 16384:
						{
							this->keyboard.offset_up = KeyBoardInfor::INACTIVE;
							break;
						}
					case 32768:
						{
							this->keyboard.offset_up = KeyBoardInfor::ACTIVE;
							break;
						}
					case 65536:
						{
							this->keyboard.go_home = KeyBoardInfor::INACTIVE;
							break;
						}
					case 131072:
						{
							this->keyboard.go_home = KeyBoardInfor::ACTIVE;
							break;
						}
					case 262144:
						{
							this->keyboard.stb_on_off = KeyBoardInfor::INACTIVE;
							break;
						}
					case 524288: // STB_ON_OF
						{
							this->keyboard.stb_on_off = KeyBoardInfor::ACTIVE;
							break;
						}
					case 1048576:
						{
							this->keyboard.focus_on_off = KeyBoardInfor::INACTIVE;
							break;
						}
					case 2097152:
						{
							this->keyboard.focus_on_off = KeyBoardInfor::ACTIVE;
							break;
						}
					case 4194304:
						{
							this->keyboard.auto_tracking = KeyBoardInfor::INACTIVE;
							break;
						}
					case 8388608: // auto tracking
						{
							this->keyboard.auto_tracking = KeyBoardInfor::ACTIVE;
							break;
						}
					case 67108864:
						{
							this->keyboard.zoom_out = KeyBoardInfor::INACTIVE;
							break;
						}
					case 134217728:
						{
							this->keyboard.zoom_out = KeyBoardInfor::ACTIVE;
							break;
						}
					case 16777216:
						{
							this->keyboard.zoom_in = KeyBoardInfor::INACTIVE;
							break;
						}
					case 33554432:
						{
							this->keyboard.zoom_in = KeyBoardInfor::ACTIVE;
							break;
						}
					case 268435456:
						{
							this->keyboard.switch_cam = KeyBoardInfor::INACTIVE;
							break;
						}
					case 536870912:
						{
							this->keyboard.switch_cam = KeyBoardInfor::ACTIVE;
							break;
						}
					case 1073741824:
						{
							this->keyboard.lrf = KeyBoardInfor::INACTIVE;
							break;
						}
					case 2147483648:
						{
							this->keyboard.lrf = KeyBoardInfor::ACTIVE;
							break;
						}						
					default:
						break;
					}
				}
			}
		}
		usleep(100);
	}
	return 0;
}

JoystickInfor RemoteControl::getJoyData(void)
{
	JoystickInfor getData;
	getData = this->joystick;
	return getData;
}

void RemoteControl::checkJoyStatus(void)
{
	this->logger->info("Starting checkJoyStatusThread...OK!");
	this->time_check_status = 0;
	while (true)
	{
		this->time_check_status++;
		if (this->time_check_status > 10)
		{
			this->time_check_status = 0;
			this->joystick.joy_state = JoystickInfor::INACTIVE;
			this->logger->warn("Joystick not found");
		}
		usleep(100000);
	}
}