
/**
******************************************************************************
* @file           : main.cpp
* @brief          : source file of tcp client
******************************************************************************
******************************************************************************
*/

#include "main.h"
#include "sensor/sensor_manage_worker.h"
#include "motor_driver/driver_manage_worker.h"
#include "remote_control/remote_control_manage_worker.h"
#include "controller/controller_manage_worker.h"
#include <thread>
#include "comm/uart_interface.h"
#include <string>
#include <iostream>

float a_get, b_get, c_get;




int main(void)
{	
	std::cout<<"Start Program" <<std::endl;
	std::thread SenSorManageThread(&SensorManageWorker::run, &sensorManageWorker);  
	std::thread DriverManageThread(&DriverManageWorker::run, &driverManageWorker);

	// std::thread ControllerManageThread(&ControllerManageWorker::runNonlinearController, &controllerManageWorker);
	std::thread ControllerManageThread(&ControllerManageWorker::run, &controllerManageWorker);

	SenSorManageThread.join();
	DriverManageThread.join();
	ControllerManageThread.join();
		
	return 0;	
}


